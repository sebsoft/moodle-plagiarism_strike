<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Main "util" class for plagiarism_strike
 *
 * File         strikeremovedocument.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   INTERSIEC.com.pl / Strikeplagiarism.com
 * @author      Kamil Łuczak <kluczak@intersiec.com.pl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace plagiarism_strike;

defined('MOODLE_INTERNAL') || die();

use plagiarism_strike\configplagiarism;

require_once($CFG->dirroot . '/plagiarism/strike/lib.php');

/**
 * plagiarism_strike\removedocument
 *
 * @package     plagiarism_strike
 *
 * @copyright   INTERSIEC.com.pl / Strikeplagiarism.com
 * @author      Kamil Łuczak <kluczak@intersiec.com.pl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class strikeremovedocument {

    /**
     * Remove document from referecne database
     *
     * @param int $documentid document identifier
     * @return \stdClass
     */
    public static function remove_from_database($documentid) {
        static::require_api_files();
        $api = new \strikeplagiarism\api\document\removefromdatabase();
        $api->set_apitoken(configplagiarism::get('strike_key'));
        $api->set_apiurl(configplagiarism::get('strike_server'));
        $api->set_apisecure(false);
        $api->set_documentid($documentid);
        self::setup_api_debugging($api);

        $response = new \stdClass();

        try {
            $rs = $api->do_request();
            $info = $api->get_http_info();

            $response->statuscode = $info['http_code'];
            $response->errorresponse = 'Connection ok';
        } catch (\strikeplagiarism\api\exception\httpstatuserror $hex) {
            $response->details = "API HTTP Status exception: " . $hex->getCode() . ': ' . $hex->getMessage();
            $response->details2 = $hex->getTraceAsString();
            // API threw exception.
            $response->statuscode = $hex->getCode() . " (HTTP-" . $hex->get_httpstatuscode() . ")";
            $response->errorresponse = $hex->getMessage();
        } catch (\strikeplagiarism\api\exception $aex) {
            $response->details = "API exception: " . $aex->getCode() . ': ' . $aex->getMessage();
            $response->details2 = $aex->getTraceAsString();
            // API threw exception.
            $response->statuscode = $aex->getCode();
            $response->errorresponse = $aex->getMessage();
        } catch (\Exception $ex) {
            $response->details = "Unknown exception: " . $ex->getCode() . ': ' . $ex->getMessage();
            $response->details2 = $ex->getTraceAsString();
            // Some generic exception was thrown.
            $response->statuscode = $aex->getCode();
            $response->errorresponse = $aex->getMessage();
        }

        return $response;
    }

    /**
     * This is a utility method to include all external API files.
     */
    static public function require_api_files() {
        global $CFG;
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/debugger/idebugger.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/debugger/output.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/debugger/phperrorlog.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/debugger/file.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/debugger/email.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/exception.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/exception/apikeyexpired.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/exception/badrequest.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/exception/servererror.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/exception/parseerror.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/exception/httpstatuserror.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/document/add.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/document/addref.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/document/get.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/document/getreport.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/document/remove-from-database.php');
    }

    /**
     * Setup API debugging.
     *
     * @param \strikeplagiarism\api $api
     * @return void
     */
    protected static function setup_api_debugging(\strikeplagiarism\api $api) {
        $dodebug = (bool)configplagiarism::get('strike_enableapidebugging');
        if (!$dodebug) {
            return;
        }
        $api->set_debug($dodebug);
        if (configplagiarism::get('strike_debuggingmtrace')) {
            $api->add_debugger(new apidebuggermtrace());
        }
        if (configplagiarism::get('strike_debuggingerrorlog')) {
            $api->add_debugger(new \strikeplagiarism\api\debugger\phperrorlog());
        }
        if (configplagiarism::get('strike_debuggingemail')) {
            $emailaddress = configplagiarism::get('strike_debuggingemailaddress');
            if (!empty($emailaddress)) {
                $api->add_debugger(new \strikeplagiarism\api\debugger\email($emailaddress));
            }
        }
    }

}
