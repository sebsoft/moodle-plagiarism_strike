<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Setup/settings form for plagiarism_strike
 *
 * File         setup.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace plagiarism_strike\forms;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');
require_once($CFG->dirroot . '/plagiarism/lib.php');

/**
 * Setup/settings form for plagiarism_strike
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class setup extends \moodleform {

    /**
     * Define form.
     */
    public function definition() {
        $mform = & $this->_form;
        global $PAGE, $CFG;

        $PAGE->requires->js_call_amd('plagiarism_strike/apitest', 'init', array($CFG->wwwroot));

        // Strike Plagiarism Enable/Disable.
        $mform->addElement('html', get_string('strikeexplain', 'plagiarism_strike'));
        $mform->addElement('checkbox', 'strike_use', get_string('usestrike', 'plagiarism_strike'));

        // Strike Server URL.
        $mform->addElement('text', 'strike_server', get_string('strikeserver', 'plagiarism_strike'));
        $mform->addHelpButton('strike_server', 'strikeserver', 'plagiarism_strike');
        $mform->addRule('strike_server', null, 'required', null, 'client');
        $mform->setDefault('strike_server', 'https://lmsapi.plagiat.pl/');
        $mform->setType('strike_server', PARAM_URL);

        // Strike API Key.
        $mform->addElement('text', 'strike_key', get_string('strikekey', 'plagiarism_strike'));
        $mform->addHelpButton('strike_key', 'strikekey', 'plagiarism_strike');
        $mform->addRule('strike_key', null, 'required', null, 'client');
        $mform->setType('strike_key', PARAM_ALPHANUMEXT);

        $mform->addElement('button', 'strike_test_api', get_string('test_api_connection', 'plagiarism_strike'));
        $mform->addHelpButton('strike_test_api', 'test_api_connection', 'plagiarism_strike');

        $mform->addElement('static', 'strike_test_api_status',
                get_string('test_api_connection_status', 'plagiarism_strike'),
                '<div class="strike_api_status"></div>');
        // Strike Debugging.
        $mform->addElement('advcheckbox', 'strike_enableapidebugging', '',
                get_string('enableapidebugging', 'plagiarism_strike'));
        $mform->addHelpButton('strike_enableapidebugging', 'enableapidebugging', 'plagiarism_strike');
        $mform->setDefault('strike_enableapidebugging', 0);

        // Mtrace debugging.
        $mform->addElement('advcheckbox', 'strike_debuggingmtrace', '',
                get_string('apidebuggingmtrace', 'plagiarism_strike'));
        $mform->addHelpButton('strike_debuggingmtrace', 'apidebuggingmtrace', 'plagiarism_strike');
        $mform->setDefault('strike_debuggingmtrace', 0);
        $mform->disabledIf('strike_debuggingmtrace', 'strike_enableapidebugging');

        // Errorlog debugging.
        $mform->addElement('advcheckbox', 'strike_debuggingerrorlog', '',
                get_string('apidebuggingerrorlog', 'plagiarism_strike'));
        $mform->addHelpButton('strike_debuggingerrorlog', 'apidebuggingerrorlog', 'plagiarism_strike');
        $mform->setDefault('strike_debuggingerrorlog', 0);
        $mform->disabledIf('strike_debuggingerrorlog', 'strike_enableapidebugging');

        // Email debugging.
        $mform->addElement('advcheckbox', 'strike_debuggingemail', '',
                get_string('apidebuggingemail', 'plagiarism_strike'));
        $mform->addHelpButton('strike_debuggingemail', 'apidebuggingemail', 'plagiarism_strike');
        $mform->setDefault('strike_debuggingemail', 0);
        $mform->disabledIf('strike_debuggingemail', 'strike_enableapidebugging');

        $mform->addElement('text', 'strike_debuggingemailaddress', get_string('apidebuggingemailaddress', 'plagiarism_strike'));
        $mform->setType('strike_debuggingemailaddress', PARAM_EMAIL);
        $mform->disabledIf('strike_debuggingemailaddress', 'strike_enableapidebugging');
        $mform->disabledIf('strike_debuggingemailaddress', 'strike_debuggingemail');

        // Default language.
        $mform->addElement('text', 'strike_defaultlang', get_string('strike_defaultlang', 'plagiarism_strike'));
        $mform->addHelpButton('strike_defaultlang', 'strike_defaultlang', 'plagiarism_strike');
        $mform->addRule('strike_defaultlang', null, 'required', null, 'client');
        $mform->setDefault('strike_defaultlang', 'en-US');
        $mform->setType('strike_defaultlang', PARAM_TEXT);

        // Minimum wordcount.
        $mform->addElement('text', 'strike_wordcount', get_string('wordcount', 'plagiarism_strike'));
        $mform->addHelpButton('strike_wordcount', 'wordcount', 'plagiarism_strike');
        $mform->setType('strike_wordcount', PARAM_INT);
        $mform->addRule('strike_wordcount', null, 'required', null, 'client');
        $mform->setDefault('strike_wordcount', '50');

        // Use notifications?
        $mform->addElement('advcheckbox', 'strike_useadminnotifications', '',
                get_string('useadminnotifications', 'plagiarism_strike'));
        $mform->addHelpButton('strike_useadminnotifications', 'useadminnotifications', 'plagiarism_strike');
        $mform->setDefault('strike_useadminnotifications', 1);

        $mform->addElement('advcheckbox', 'strike_useteachernotifications', '',
                get_string('useteachernotifications', 'plagiarism_strike'));
        $mform->addHelpButton('strike_useteachernotifications', 'useteachernotifications', 'plagiarism_strike');
        $mform->setDefault('strike_useteachernotifications', 1);

        // List modules supporting plagiarism.
        $mods = \core_component::get_plugin_list('mod');
        foreach ($mods as $mod => $modname) {
            if (plugin_supports('mod', $mod, FEATURE_PLAGIARISM)) {
                $modstring = 'strike_enable_mod_' . $mod;
                $mform->addElement('checkbox', $modstring, get_string('strike_enableplugin', 'plagiarism_strike', $mod));
                if ($modname == 'assign') {
                    $mform->setDefault($modstring, 1);
                }
            }
        }

        $this->add_action_buttons(true);
    }

}