<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defaults form plagiarism_strike
 *
 * File         defaults.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace plagiarism_strike\forms;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');

/**
 * plagiarism_strike\forms\defaults
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class defaults extends \moodleform {

    /**
     * Define form.
     */
    protected function definition() {
        $mform = $this->_form;
        // Add description header.
        $mform->addElement('static', '_defaultsdesc', '', get_string('form:desc:defaults', 'plagiarism_strike'));
        // Add elements.
        strike_get_form_elements($mform, 0, true);
        // Set some "disabledIf" configs.
        strike_form_element_disable_rules($mform);
        // Add buttons.
        $this->add_action_buttons();
    }

    /**
     * Validate form input.
     *
     * @param array $data array of ("fieldname"=>value) of submitted data
     * @param array $files array of uploaded files "element_name"=>tmp_file_path
     * @return array of "element_name"=>"error_description" if there are errors,
     *         or an empty array if everything is OK (true allowed for backwards compatibility too).
     */
    public function validation($data, $files) {
        $rs = parent::validation($data, $files);

        if ((bool)$data['strike_allowallfile'] === false) {
            if (empty($data['strike_selectfiletypes]'])) {
                $rs['strike_selectfiletypes'] = get_string('err:strike_selectfiletypes-empty', 'plagiarism_strike');
            }
        }

        return $rs;
    }

}