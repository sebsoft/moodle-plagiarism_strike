<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Main "util" class for plagiarism_strike
 *
 * File         strikeplagiarism.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace plagiarism_strike;

defined('MOODLE_INTERNAL') || die();

use plagiarism_strike\config;
use plagiarism_strike\configplagiarism;
use plagiarism_strike\strikefile;
use plagiarism_strike\striketempfile;
use plagiarism_strike\strikereport;


require_once($CFG->dirroot . '/plagiarism/strike/lib.php');

/**
 * plagiarism_strike\strikeplagiarism
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class strikeplagiarism {

    /**
     * Is strike enabled for a specific course module?
     * @param int $cmid
     * @return bool
     */
    public static function use_strike($cmid) {
        global $DB;
        static $loaded = false;
        static $enabled = [];
        if (!$loaded) {
            $enabled = $DB->get_records_menu('plagiarism_strike_config',
                    array('cm' => $cmid, 'name' => 'strike_use_strike'), '', 'cm,value');
        }
        return (isset($enabled[$cmid]) ? (bool) $enabled[$cmid] : false);
    }

    /**
     * This is a utility method to include all external API files.
     */
    static public function require_api_files() {
        global $CFG;
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/debugger/idebugger.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/debugger/output.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/debugger/phperrorlog.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/debugger/file.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/debugger/email.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/exception.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/exception/apikeyexpired.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/exception/badrequest.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/exception/servererror.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/exception/parseerror.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/document/add.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/document/addref.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/document/get.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/document/getreport.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/account/settings.php');
    }

    /**
     * Get plagiarism file record
     *
     * @param int $cmid course module id
     * @param int $userid user id
     * @param \stored_file|string $file
     * @param int $relateduserid relateduserid if passed
     * @return strikefile
     */
    public static function get_plagiarism_file($cmid, $userid, $file, $relateduserid = null) {
        global $DB;

        $authorparams = array('id' => empty($relateduserid) ? $userid : $relateduserid);
        $author = $DB->get_record('user', $authorparams);
        $authorname = fullname($author);

        if (is_string($file)) { // This is a local file path.
            $filepath = $file;
            $filename = basename($file);
            $filehash = sha1_file($file);
            $title = basename($file);
            $isstoredfile = 0;
            $author = $authorname;
            $md5sum = md5_file($file);
        } else {
            $filepath = (!empty($file->filepath)) ? $file->filepath : $file->get_filepath();
            $filehash = (!empty($file->contenthash)) ? $file->contenthash : $file->get_contenthash();
            $filename = (!empty($file->filename)) ? $file->filename : $file->get_filename();
            $title = $file->get_filename();
            $isstoredfile = 1;
            $author = $authorname;
            $md5sum = md5($file->get_content());
        }

        // Now update or insert record into strike_files.
        $sql = "SELECT * FROM {".strikefile::table()."}
                WHERE cm = :cm AND contenthash = :contenthash AND isstoredfile = :isstoredfile
                AND ((userid = :userid AND COALESCE(relateduserid,0)=0)";
        $conditions = array('cm' => $cmid, 'contenthash' => $filehash, 'isstoredfile' => $isstoredfile, 'userid' => $userid);
        if (!empty($relateduserid)) {
            $sql .= ' OR (userid = :userid2 AND relateduserid = :relateduserid)';
            // Add these to the conditions, since it represents the STUDENT (submission is done on behalf).
            $conditions['userid2'] = $userid;
            $conditions['relateduserid'] = $relateduserid;
        }
        $sql .= ")";
        $plagiarismfile = strikefile::get_one($conditions);
        if (!empty($plagiarismfile->id)) {
            return $plagiarismfile;
        } else {
            // CM config.
            $config = new config($cmid);

            $plagiarismfile->relateduserid = $relateduserid;
            $plagiarismfile->contenthash = $filehash;
            $plagiarismfile->filename = $filename;
            $plagiarismfile->filepath = $filepath;
            $plagiarismfile->statuscode = PLAGIARISM_STRIKE_STATUS_PENDING;
            $plagiarismfile->attempt = 0;
            $plagiarismfile->timesubmitted = time();
            $plagiarismfile->md5sum = $md5sum;
            $plagiarismfile->title = $title;
            $plagiarismfile->author = $author;
            $plagiarismfile->coordinator = $author;
            if ($config->get('strike_docsendmode') == PLAGIARISM_STRIKE_SHOW_ALWAYS) {
                $plagiarismfile->automatedanalysis = 1;
                $plagiarismfile->doanalysis = 1;
            } else {
                $plagiarismfile->automatedanalysis = 0;
                $plagiarismfile->doanalysis = 0;
                $plagiarismfile->statuscode = PLAGIARISM_STRIKE_STATUS_AWAITCHOICE;
            }
            if (!$plagiarismfile->save()) {
                debugging("insert into strike_files failed");
            }
            return $plagiarismfile;
        }
    }

    /**
     * Queue a plagiarism file to STRIKE
     *
     * @param int $cmid course module id
     * @param int $userid user id
     * @param \stored_file|string $file
     * @param int $relateduserid relateduserid if passed
     * @return strikefile
     */
    public static function queue_file($cmid, $userid, $file, $relateduserid = null) {
        $plagiarismfile = static::get_plagiarism_file($cmid, $userid, $file, $relateduserid);

        // Check if $plagiarismfile actually needs to be submitted.
        $queuestatusses = array(
            PLAGIARISM_STRIKE_STATUS_PENDING,
            PLAGIARISM_STRIKE_STATUS_AWAITCHOICE
        );
        if (!in_array($plagiarismfile->statuscode, $queuestatusses)) {
            return '';
        }
        if (is_string($file)) {
            $filename = basename($file);
        } else {
            $filename = (!empty($file->filename)) ? $file->filename : $file->get_filename();
        }

        if ($plagiarismfile->filename !== $filename) {
            // This is a file that was previously submitted and not sent to STRIKE but the filename has changed so fix it.
            $plagiarismfile->filename = $filename;
            $plagiarismfile->save();
        }
        // Check to see if this is a valid file.
        $mimetype = static::check_file_type($filename);
        if (empty($mimetype)) {
            $plagiarismfile->statuscode = PLAGIARISM_STRIKE_STATUS_UNSUPPORTED;
            $plagiarismfile->save();
            return '';
        }

        $config = new config($cmid);
        // Check to see if configured to only send certain file-types and if this file matches.
        $allowallfiles = $config->get('strike_allowallfile', true);
        if ($allowallfiles !== null && $allowallfiles == 0) {
            $allowedtypes = explode(',', $config->get('strike_selectfiletypes'));

            $pathinfo = pathinfo($filename);
            if (!empty($pathinfo['extension'])) {
                $ext = strtolower($pathinfo['extension']);
                if (!in_array($ext, $allowedtypes)) {
                    // This file is not allowed, delete it from the table.
                    $plagiarismfile->delete();
                    debugging("File submitted to cm:" . $cmid . " with an extension " . $ext .
                            " This assignment is configured to ignore this filetype, " .
                            "only files of type:" . $config->get('strike_selectfiletypes') . " are accepted");
                    return '';
                }
            } else {
                // No path found - this shouldn't happen but ignore this file.
                debugging("Could not obtain the extension for a file submitted to cm:" . $cmid . " with the filename " . $filename .
                        "only files of type:" . $config->get('strike_selectfiletypes') . " are accepted");
                $plagiarismfile->delete();
                return '';
            }
        }

        return $plagiarismfile;
    }

    /**
     * Send a file to STRIKE
     *
     * @param strikefile $file
     */
    public static function send_file(strikefile $file) {
        // Check whether or not this CM still exists. If not, delete record.
        $module = $file->get_course_module();
        if (empty($module)) {
            mtrace("STRIKE fileid: {$file->id} Course module id: {$file->cm} does not exist, deleting record");
            $file->delete();
            return;
        }
        mtrace("STRIKE fileid: {$file->id} process sending off");
        $fileobj = static::get_file_object($file);
        if (empty($fileobj)) {
            mtrace("STRIKE fileid: {$file->id} File not found, this may have been replaced by a newer file - deleting record");
            if (debugging()) {
                mtrace(static::pretty_print($file));
            }
            $file->delete();
            return;
        }
        if ($file->statuscode == PLAGIARISM_STRIKE_STATUS_INVALIDRESPONSE) {
            // Check if we can handle this attempt.
            if (!static::check_attempt_timeout($file)) {
                mtrace("STRIKE fileid: {$file->id} File, Attempt: {$file->attempt}, failed, queued for resubmission after wait.");
                return;
            }
        }
        if ($module->name == "assign") {
            // Check for group assignment and adjust userid if required.
            // This prevents subsequent group submissions from flagging a previous submission as a match.
            $file = plagiarism_strike_check_group($file);
        }
        self::send_to_strike($file, $fileobj);
    }

    /**
     * Get a stored file object for the given plagiarismfile.
     *
     * @param strikefile $plagiarismfile
     * @return \stored_file
     */
    static public function get_file_object(strikefile $plagiarismfile) {
        global $CFG;
        require_once($CFG->dirroot . '/plagiarism/strike/lib.php');
        if (strpos($plagiarismfile->filepath, $CFG->tempdir) !== false) {
            return strike_get_temp_file_object($plagiarismfile);
        } else {
            $cm = get_coursemodule_from_id('', $plagiarismfile->cm);
            if (debugging()) {
                mtrace("STRIKE fileid {$plagiarismfile->id} {$cm->modname} found");
            }
            $file = null;
            switch ($cm->modname) {
                case 'assign':
                    $file = strike_get_assign_file_object($plagiarismfile);
                    break;
                case 'workshop':
                    $file = strike_get_workshop_file_object($plagiarismfile);
                    break;
                case 'forum':
                    $file = strike_get_forum_file_object($plagiarismfile);
                    break;
                default:
                    break;
            }
            return $file;
        }
    }

    /**
     * Test strike
     */
    static protected function test_strike() {
        // No-op.
        return;
    }

    /**
     * Send a file to STRIKE
     *
     * @param strikefile $plagiarismfile
     * @param \stored_file $file
     * @return boolean
     */
    static protected function send_to_strike(strikefile $plagiarismfile, $file) {
        global $DB;

        if (debugging()) {
            static::pretty_print($plagiarismfile);
            static::pretty_print($file);
        }

        $filename = (!empty($file->filename)) ? $file->filename : $file->get_filename();
        $mimetype = static::check_file_type($filename);
        if (empty($mimetype)) {// Sanity check on filetype - this should already have been checked.
            if (debugging()) {
                echo "STRIKE fileid {$plagiarismfile->id} - unsupported file format";
            }
            $plagiarismfile->statuscode = PLAGIARISM_STRIKE_STATUS_UNSUPPORTED;
            $plagiarismfile->save();
            return true;
        }
        mtrace("STRIKE fileid:" . $plagiarismfile->id . ' sending to STRIKE');
        if (!empty($plagiarismfile->relateduserid)) {
            $useremail = $DB->get_field('user', 'email', array('id' => $plagiarismfile->relateduserid));
        } else {
            $useremail = $DB->get_field('user', 'email', array('id' => $plagiarismfile->userid));
        }

        // Get url of api.
        $filecontents = (!empty($file->filepath)) ? file_get_contents($file->filepath) : $file->get_content();
        $filename = (!empty($file->filepath)) ? basename($file->filepath) : $file->get_filename();
        $ext = pathinfo($filename, PATHINFO_EXTENSION);

        static::require_api_files();
        $api = new \strikeplagiarism\api\document\add();
        $api->set_apitoken(configplagiarism::get('strike_key'));
        $api->set_apiurl(configplagiarism::get('strike_server'));
        $api->set_apisecure(false);
        self::setup_api_debugging($api);
        $api->set_documentaction('check'); // Default.
        $api->set_documentauthor($plagiarismfile->author);
        $api->set_documentcallback(static::get_callback_url());
        $api->set_documentcoordinator($plagiarismfile->coordinator);
        $api->set_documentlanguage(configplagiarism::get('strike_defaultlang'));
        $api->set_documenttitle($plagiarismfile->title);
        $tmpfile = new striketempfile($filecontents, $ext);
        $api->set_documentfile($tmpfile->get_filepath());

        try {
            $rs = $api->do_request();
            // Now store GUID.
            $plagiarismfile->guid = $rs['id'];
            $plagiarismfile->statuscode = $rs['status'];
            $plagiarismfile->errorresponse = $rs['message'];
            $plagiarismfile->attempt++;
            $plagiarismfile->save();
        } catch (\strikeplagiarism\api\exception\httpstatuserror $hex) {
            mtrace("API HTTP Status exception: " . $hex->getCode() . ': ' . $hex->getMessage());
            mtrace($hex->getTraceAsString());
            // API threw exception.
            $plagiarismfile->statuscode = $hex->getCode() . " (HTTP-" . $hex->get_httpstatuscode() . ")";
            $plagiarismfile->errorresponse = $hex->getMessage();
            $plagiarismfile->attempt++;
            $plagiarismfile->save();
            // Send errormessage.
            notification::send_error($plagiarismfile, $hex);
        } catch (\strikeplagiarism\api\exception $aex) {
            mtrace("API exception: " . $aex->getCode() . ': ' . $aex->getMessage());
            mtrace($aex->getTraceAsString());
            // API threw exception.
            $plagiarismfile->statuscode = $aex->getCode();
            $plagiarismfile->errorresponse = $aex->getMessage();
            $plagiarismfile->attempt++;
            $plagiarismfile->save();
            // Send errormessage.
            notification::send_error($plagiarismfile, $aex);
        } catch (\Exception $ex) {
            mtrace("Unknown exception: " . $ex->getCode() . ': ' . $ex->getMessage());
            mtrace($ex->getTraceAsString());
            // Some generic exception was thrown.
            $plagiarismfile->statuscode = $aex->getCode();
            $plagiarismfile->errorresponse = $aex->getMessage();
            $plagiarismfile->attempt++;
            $plagiarismfile->save();
            // Send errormessage.
            notification::send_error($plagiarismfile, $aex);
        }
    }

    /**
     * Send off all applicable files to STRIKE.
     */
    public static function send_files() {
        global $DB;
        // Is strike enabled?
        if ((bool) configplagiarism::get('strike_use') === false) {
            return;
        }
        // Select all sendable.
        $allowedstatusses = array(
            PLAGIARISM_STRIKE_STATUS_PENDING,
            PLAGIARISM_STRIKE_STATUS_INVALIDRESPONSE,
            PLAGIARISM_STRIKE_STATUS_ERRSERVER,
            PLAGIARISM_STRIKE_STATUS_ERRAPIKEY,
            PLAGIARISM_STRIKE_STATUS_ERRBADREQUEST,
            PLAGIARISM_STRIKE_STATUS_ERROR,
        );
        list($insql, $params) = $DB->get_in_or_equal($allowedstatusses);
        $select = "statuscode {$insql} AND attempt <= ? AND doanalysis = 1";
        $params[] = PLAGIARISM_STRIKE_MAXATTEMPTS;
        $files = strikefile::get_select($select, $params);
        foreach ($files as $file) {
            self::send_file($file);
        }
    }

    /**
     * Check if the filetype is legit for use with STRIKE.
     *
     * @param string $filename
     * @param bool $checkdb If true, also check database for extra known file types.
     * @return string mimetype or NULL
     */
    public static function check_file_type($filename, $checkdb = true) {
        $pathinfo = pathinfo($filename);

        if (empty($pathinfo['extension'])) {
            return null;
        }
        $ext = strtolower($pathinfo['extension']);
        $filetypes = static::default_allowed_file_types();

        if (!empty($filetypes[$ext])) {
            return $filetypes[$ext];
        }
        // Check for updated allowed filetypes.
        if ($checkdb) {
            return configplagiarism::get('ext_' . $ext);
        } else {
            return null;
        }
    }

    /**
     * Used to obtain allowed file types
     *
     * @param bool $checkdb If true, also check database for extra known file types.
     */
    static public function default_allowed_file_types($checkdb = false) {
        global $DB;
        $filetypes = array(
            'txt' => 'text/plain',
            'doc' => 'application/msword',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'odt' => 'application/vnd.oasis.opendocument.text',
            'pdf' => 'application/pdf',
            'rtf' => 'application/rtf',
        );

        if ($checkdb) {
            // Get all filetypes from db as well.
            $sql = 'SELECT name, value FROM {config_plugins} WHERE plugin = :plugin AND ' . $DB->sql_like('name', ':name');
            $types = $DB->get_records_sql($sql, array('name' => 'doc_ext_%', 'plugin' => 'plagiarism_strike'));
            foreach ($types as $type) {
                $ext = strtolower(str_replace('doc_ext_', '', $type->name));
                $filetypes[$ext] = $type->value;
            }
        }

        return $filetypes;
    }

    /**
     * Get all applicable reports from STRIKE.
     */
    public static function get_reports() {
        // This is a no-op method for now, but here because the calling task needs the method to exist.
        // TODO: implement some intelligent reporting method (should not be needed since adhoc tasks take care of the reports)?
        global $DB;
        $params = array('reportready' => 0, 'statuscode' => 'new', 'doanalysis' => 1);
        if ($reportsneeded = $DB->get_records('plagiarism_strike_files', $params)) {
            foreach ($reportsneeded as $val) {
                mtrace("Get report for: {$val->guid}");
                self::sync_report_for_guid($val->guid);
            }
        }
    }

    /**
     * Reset a strikefile so it can be resubmitted
     *
     * @param int|strikefile $strikefile
     * @return boolean
     */
    public static function reset_file($strikefile) {
        global $DB;
        if (is_numeric($strikefile)) {
            $strikefile = strikefile::get_by_id($strikefile);
        }
        // If submitted or done, don't process.
        if ($strikefile->statuscode == PLAGIARISM_STRIKE_STATUS_ACCEPTED || (bool)$strikefile->reportready) {
            return true;
        }
        // CM MUST exist.
        if (!$DB->record_exists('course_modules', array('id' => $strikefile->cm))) {
            mtrace("STRIKE fileid {$strikefile->id}: Course module {$strikefile->cm} does not exist, deleting record");
            $strikefile->delete();
            return true;
        }
        // Get fileobject.
        $fo = static::get_file_object($strikefile);
        if (!empty($fo)) {
            // File exists, let's try sending this again.
            $strikefile->statuscode = PLAGIARISM_STRIKE_STATUS_PENDING;
            $strikefile->errorresponse = '';
            $strikefile->attempt = 0;
            $strikefile->timesubmitted = time();
            $strikefile->save();

            $plagiarismfile = static::queue_file($strikefile->cm, $strikefile->userid, $fo);
            if (!empty($plagiarismfile)) {
                // Send off.
                static::send_to_strike($plagiarismfile, $fo);
            }
            return true;
        }
        return false;
    }

    /**
     * Check whether or not a failed submission to STRIKE can/should be resubmitted
     * @param strikefile $plagiarismfile
     * @return boolean
     */
    public static function check_attempt_timeout(strikefile $plagiarismfile) {
        // Maximum number of times to try and obtain the status of a submission.
        // Make sure the max attempt value in $statusdelay is higher than this.
        $maxstatusattempts = PLAGIARISM_STRIKE_MAXATTEMPTS;

        // Time to wait between checks array(number of attempts-1, time delay in minutes)..
        $statusdelay = array(2 => 5, // Up to attempt 3 check every 5 minutes.
                             3 => 15, // Up to attempt 4 check every 15 minutes.
                             6 => 30,
                            11 => 120,
                            20 => 240,
                            100 => 480);

        // The first time a file is submitted we don't need to wait at all.
        if (empty($plagiarismfile->attempt) && $plagiarismfile->statuscode == PLAGIARISM_STRIKE_STATUS_PENDING) {
            return true;
        }

        // Check if we have exceeded the max attempts.
        if ($plagiarismfile->attempt > $maxstatusattempts) {
            $plagiarismfile->statuscode = PLAGIARISM_STRIKE_STATUS_TIMEOUT;
            $plagiarismfile->save();
            return true; // Return true to cancel the event.
        }

        $now = time();

        // Now calculate wait time.
        $i = 0;
        $wait = 0;
        while ($i < $plagiarismfile->attempt) {
            // Find what multiple we need to use for this attempt.
            foreach ($statusdelay as $att => $delay) {
                if ($att >= $i) {
                    $wait = $wait + $delay;
                    break;
                }
            }
            $i++;
        }
        $wait = (int)$wait * 60;
        $timetocheck = (int)($plagiarismfile->timesubmitted + $wait);
        // Calculate when this should be checked next.

        if ($timetocheck < $now) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Synchronize data from STRIKE for a submitted file and fetch report.
     *
     * @param string $guid
     */
    public static function sync_report_for_guid($guid) {
        // Require all API files.
        static::require_api_files();
        // Get record.
        $strikefile = \plagiarism_strike\strikefile::get_by_guid($guid, false);
        if (empty($strikefile->id)) {
            // File no longer exists.
            return;
        }
        $config = new config($strikefile->cm);
        // Get the report.
        $api = new \strikeplagiarism\api\document\get();
        $api->set_apitoken(\plagiarism_strike\configplagiarism::get('strike_key'));
        $api->set_apiurl(\plagiarism_strike\configplagiarism::get('strike_server'));
        $api->set_apisecure(false);
        self::setup_api_debugging($api);
        $api->set_documentid($strikefile->guid);
        $api->set_documentchecksum($strikefile->md5sum);

        try {
            $rs = $api->do_request();
            // On success...
            $strikefile->reportready = $rs['report-ready'];
            $strikefile->indexed = $rs['indexed'];
            $strikefile->factor1 = $rs['factor1'];
            $strikefile->factor2 = $rs['factor2'];
            $strikefile->factor3 = $rs['factor3'];
            $strikefile->factor4 = $rs['factor4'];
            $strikefile->factor5 = $rs['factor5'];
            $strikefile->foreignalphabetalert = $rs['foreign-alphabet-alert'];
            // Set status to complete.
            $strikefile->statuscode = PLAGIARISM_STRIKE_STATUS_COMPLETE;
            $strikefile->save();
        } catch (\strikeplagiarism\api\exception\httpstatuserror $hex) {
            mtrace("API HTTP Status exception: " . $hex->getCode() . ': ' . $hex->getMessage());
            mtrace($hex->getTraceAsString());
            // API threw exception.
            $strikefile->statuscode = $hex->getCode() . " (HTTP-" . $hex->get_httpstatuscode() . ")";
            $strikefile->errorresponse = $hex->getMessage();
            $strikefile->save();
            // Send errormessage.
            notification::send_error($strikefile, $hex);
        } catch (\strikeplagiarism\api\exception $e) {
            mtrace("API exception: " . $e->getCode() . ': ' . $e->getMessage());
            mtrace($e->getTraceAsString());
            $strikefile->statuscode = $e->getCode();
            $strikefile->errorresponse = $e->getMessage();
            $strikefile->save();
            // Send errormessage.
            notification::send_error($strikefile, $e);
        } catch (\Exception $e2) {
            mtrace("API exception: " . $e2->getCode() . ': ' . $e2->getMessage());
            mtrace($e2->getTraceAsString());
            $strikefile->statuscode = PLAGIARISM_STRIKE_STATUS_ERROR;
            $strikefile->errorresponse = $e2->getMessage();
            $strikefile->save();
            // Send errormessage.
            notification::send_error($strikefile, $e2);
        }

        // Add reference? Fallback for when it's failed during the STRIKE callback.
        if (!(bool)$strikefile->indexed &&
                $config->get('strike_addref_method') === PLAGIARISM_STRIKE_ADDREF_AUTO) {
            self::index_document($strikefile);
        }

        $api = new \strikeplagiarism\api\document\getreport();
        $api->set_apitoken(\plagiarism_strike\configplagiarism::get('strike_key'));
        $api->set_apiurl(\plagiarism_strike\configplagiarism::get('strike_server'));
        $api->set_apisecure(false);
        self::setup_api_debugging($api);
        $api->set_documentid($strikefile->guid);
        $api->set_documentchecksum($strikefile->md5sum);

        try {
            $rs = $api->do_request();
            // On success, the result is the HTML report file.
            $strikereport = \plagiarism_strike\strikereport::get_by_guid($guid, false);
            $strikereport->strikefileid = $strikefile->id;
            $strikereport->report = $rs;
            $strikereport->save();
            // And do we notify students per email?
            if ((bool)$config->get('strike_studentemail') === true) {
                static::send_student_email($strikefile);
            }
        } catch (\strikeplagiarism\api\exception $e) {
            mtrace("API exception: " . $e->getCode() . ': ' . $e->getMessage());
            mtrace($e->getTraceAsString());
            // Send errormessage.
            notification::send_error($strikefile, $e);
        } catch (\Exception $e2) {
            mtrace("API exception: " . $e2->getCode() . ': ' . $e2->getMessage());
            mtrace($e2->getTraceAsString());
            // Send errormessage.
            notification::send_error($strikefile, $e2);
        }
    }

    /**
     * Add document to index database at STRIKE
     *
     * @param strikefile $plagiarismfile
     */
    static public function index_document(strikefile $plagiarismfile) {
        if (debugging()) {
            static::pretty_print($plagiarismfile);
        }

        static::require_api_files();
        $api = new \strikeplagiarism\api\document\addref();
        $api->set_apitoken(configplagiarism::get('strike_key'));
        $api->set_apiurl(configplagiarism::get('strike_server'));
        $api->set_apisecure(false);
        self::setup_api_debugging($api);
        $api->set_documentid($plagiarismfile->guid);

        try {
            $rs = $api->do_request();
            // Now store status.
            $plagiarismfile->indexed = 1;
            $plagiarismfile->save();
        } catch (\strikeplagiarism\api\exception $aex) {
            mtrace("API exception: " . $aex->getCode() . ': ' . $aex->getMessage());
            mtrace($aex->getTraceAsString());
            // Send errormessage.
            notification::send_error($plagiarismfile, $aex);
        } catch (\Exception $ex) {
            mtrace("Unknown exception: " . $ex->getCode() . ': ' . $ex->getMessage());
            mtrace($aex->getTraceAsString());
            // Send errormessage.
            notification::send_error($plagiarismfile, $ex);
        }
    }

    /**
     * Send an email to a user notifying him/her of the STRIKE results
     *
     * @param strikefile $strikefile
     * @return boolean
     */
    public static function send_student_email($strikefile) {
        global $DB, $CFG;
        if (empty($strikefile->userid)) { // Sanity check.
            return false;
        }
        if (!empty($strikefile->relateduserid)) {
            $user = $DB->get_record('user', array('id' => $strikefile->relateduserid));
        } else {
            $user = $DB->get_record('user', array('id' => $strikefile->userid));
        }
        $site = get_site();

        $a = new \stdClass();
        $cm = get_coursemodule_from_id('', $strikefile->cm);
        $a->modulename = format_string($cm->name);
        $a->modulelink = $CFG->wwwroot . '/mod/' . $cm->modname . '/view.php?id=' . $cm->id;
        $a->coursename = format_string($DB->get_field('course', 'fullname', array('id' => $cm->course)));
        $a->firstname = $user->firstname;
        $a->lastname = $user->lastname;
        $a->admin = generate_email_signoff();

        $emailsubject = get_string('studentemailsubject', 'plagiarism_strike');
        $emailcontenthtml = get_string('studentemailcontent', 'plagiarism_strike', $a);
        $emailcontenttext = format_text_email($emailcontenthtml, FORMAT_HTML);

        email_to_user($user, $site->shortname, $emailsubject, $emailcontenttext, $emailcontenthtml);
    }

    /**
     * Get STRIKE callback url
     *
     * @return string
     */
    public static function get_callback_url() {
        global $CFG;
        static $url = null;
        if ($url === null) {
            $url = new \moodle_url($CFG->wwwroot . '/plagiarism/strike/strikecallback.php');
        }
        return $url->out(false);
    }

    /**
     * Get report url
     *
     * @param strikefile|int $strikefile STRIKE plagiarism file or file id
     * @return \moodle_url
     */
    public static function get_report_url($strikefile) {
        global $CFG;
        if (is_numeric($strikefile)) {
            $strikefile = strikefile::get_by_id($strikefile);
        }
        $params = array('pid' => $strikefile->id, 'cm' => $strikefile->cm, 'sesskey' => sesskey());
        $url = new \moodle_url($CFG->wwwroot . '/plagiarism/strike/strikereport.php', $params);
        return $url;
    }

    /**
     * Render an image
     * @param string $icon icon identifier (must be present in pix folder of this plugin)
     * @param string $altid alt text identifier (must be present in language files)
     * @param string|null $title
     * @return string
     */
    public static function get_image($icon, $altid, $title = null) {
        global $OUTPUT, $CFG;
        $alt = get_string($altid, 'plagiarism_strike');
        if ($title == null) {
            $title = $alt;
        }
        $attributes = array('title' => $title);
        if ($CFG->branch >= 33) {
            $icon = $OUTPUT->image_url($icon, 'plagiarism_strike');
        } else {
            $icon = $OUTPUT->pix_url($icon, 'plagiarism_strike');
        }
        return \html_writer::img($icon, $alt, $attributes);
    }

    /**
     * We are not allowed to use print_object so use a hand-rolled function to help with debugging.
     *
     * @param \stdClass|array $arr data
     * @return string
     */
    public static function pretty_print($arr) {
        if (is_object($arr)) {
            $arr = (array) $arr;
        }
        $retstr = '<table class="generaltable">';
        $retstr .= '<tr><th width=20%>Key</th><th width=80%>Value</th></tr>';
        if (is_array($arr)) {
            foreach ($arr as $key => $val) {
                if (is_object($val)) {
                    $val = (array) $val;
                }
                if (is_array($val)) {
                    $retstr .= '<tr><td>' . $key . '</td><td>' . static::pretty_print($val) . '</td></tr>';
                } else {
                    $retstr .= '<tr><td>' . $key . '</td><td>' . ($val == '' ? '""' : $val) . '</td></tr>';
                }
            }
        }
        $retstr .= '</table>';
        return $retstr;
    }

    /**
     * Setup API debugging.
     *
     * @param \strikeplagiarism\api $api
     * @return void
     */
    protected static function setup_api_debugging(\strikeplagiarism\api $api) {
        $dodebug = (bool)configplagiarism::get('strike_enableapidebugging');
        if (!$dodebug) {
            return;
        }
        $api->set_debug($dodebug);
        if (configplagiarism::get('strike_debuggingmtrace')) {
            $api->add_debugger(new apidebuggermtrace());
        }
        if (configplagiarism::get('strike_debuggingerrorlog')) {
            $api->add_debugger(new \strikeplagiarism\api\debugger\phperrorlog());
        }
        if (configplagiarism::get('strike_debuggingemail')) {
            $emailaddress = configplagiarism::get('strike_debuggingemailaddress');
            if (!empty($emailaddress)) {
                $api->add_debugger(new \strikeplagiarism\api\debugger\email($emailaddress));
            }
        }
    }

}
