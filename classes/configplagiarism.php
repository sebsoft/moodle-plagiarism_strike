<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plagiarism config class for plagiarism_strike
 *
 * File         configplagiarism.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace plagiarism_strike;

defined('MOODLE_INTERNAL') || die();

/**
 * plagiarism_strike\configplagiarism
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class configplagiarism {

    /**
     *
     * @var whether or not config has been loaded yet
     */
    static private $loaded = false;

    /**
     *
     * @var \stdClass the tool configuration
     */
    static private $config = null;

    /**
     * initialize the tool configuration
     */
    static private function init() {
        if (!self::$loaded) {
            self::$config = get_config('plagiarism');
        }
    }

    /**
     * get the configuration object
     *
     * @return \stdClass
     */
    public static function get_config() {
        self::init();
        return self::$config;
    }

    /**
     * get a configuration value
     *
     * @param string $name config name
     * @return mixed
     */
    public static function get($name) {
        self::init();
        if (isset(self::$config->$name)) {
            return self::$config->$name;
        }
        return null;
    }

    /**
     * set a configuration value
     *
     * @param string $name config name
     * @param mixed $value config value
     * @param bool $force force insert if this value does not exist
     * @return mixed
     */
    public static function set($name, $value, $force = true) {
        self::init();
        if (isset(self::$config->$name) || $force) {
            self::$config->$name = $value;
            set_config($name, $value, 'plagiarism');
        }
        return self::$config->$name;
    }

    /**
     * Invalidates the cached configuration and reloads from database
     */
    public static function invalidate() {
        self::$loaded = false;
        self::$config = null;
        self::init();
    }

    /**
     * Return configuration as object
     * @return \stdClass
     */
    public static function as_object() {
        self::init();
        return (object)self::$config;
    }

    /**
     * Return configuration as array
     * @return array
     */
    public static function as_array() {
        self::init();
        return (array)self::$config;
    }

}