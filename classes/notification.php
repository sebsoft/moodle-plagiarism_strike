<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Notification class
 *
 * File         notification.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace plagiarism_strike;

defined('MOODLE_INTERNAL') || die();

/**
 * plagiarism_strike\notification
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class notification {

    /**
     * Send error message.
     *
     * @param int $cmid
     * @param string $message
     */
    public static function send($cmid, $message) {
        global $DB, $CFG;
        // Check if we use notifications.
        if ((bool) configplagiarism::get('strike_use') === false) {
            return;
        }
        $config = new config($cmid);
        if ((bool) $config->get('strike_use_strike') === false) {
            return;
        }

        $site = get_site();
        $module = get_coursemodule_from_id('', $cmid);
        $instance = $DB->get_record($module->modname, array('id' => $module->instance));
        $contexturl = new \moodle_url($CFG->wwwroot . '/mod/' . $module->modname . '/view.php', array('id' => $cmid));
        $contexturlname = $instance->name;
        $admin = get_admin();

        if ((bool) $config->get('strike_useadminnotifications')) {
            // Send message to admin. 'Current' language always!
            $subject = get_string('notification:subject', 'plagiarism_strike', $site->fullname);
            // Send off.
            self::message_send($admin, $subject, $message, $contexturl, $contexturlname, $module->course);
        }
        if ((bool) $config->get('strike_useteachernotifications')) {
            // Send message to teacher(s).
            $teacherids = $config->get('strike_notifyteachers');
            if (!empty($teacherids)) {
                $idslist = array_map('intval', array_map('trim', explode(',', $teacherids)));
                $teachers = $DB->get_records_list('user', 'id', $idslist);
                foreach ($teachers as $teacher) {
                    // Skip if admin.
                    if ($teacher->id == $admin->id) {
                        continue;
                    }
                    $subject = get_string('notification:subject', 'plagiarism_strike', $teacher->lang);
                    self::message_send($teacher, $subject, $message, $contexturl, $contexturlname, $module->course);
                }
            }
        }
    }

    /**
     * Send error message.
     *
     * @param strikefile $strikefile
     * @param \Exception $exception
     */
    public static function send_error(strikefile $strikefile, \Exception $exception) {
        global $DB, $CFG;
        // Check if we use notifications.
        if ((bool) configplagiarism::get('strike_use') === false) {
            return;
        }
        $config = new config($strikefile->cm);
        if ((bool) $config->get('strike_use_strike') === false) {
            return;
        }

        $context = \context_module::instance($strikefile->cm);
        $coursecontext = $context->get_course_context();
        $course = get_course($coursecontext->instanceid);
        $site = get_site();
        $module = get_coursemodule_from_id('', $strikefile->cm);
        $instance = $DB->get_record($module->modname, array('id' => $module->instance));
        $contexturl = new \moodle_url($CFG->wwwroot . '/mod/' . $module->modname . '/view.php', array('id' => $strikefile->cm));
        $contexturlname = $instance->name;

        $a = new \stdClass();
        $a->instancename = $instance->name;
        $a->courseshortname = $course->shortname;
        $a->coursefullname = $course->fullname;
        $a->errormessage = $exception->getMessage();
        $a->signoff = generate_email_signoff();

        $a->strikefileguid = $strikefile->guid;
        $a->strikefileauthor = $strikefile->author;
        $a->strikefilecoordinator = $strikefile->coordinator;
        $a->strikefiletitle = $strikefile->title;
        $a->strikefilename = $strikefile->filename;
        $a->strikefiletimecreated = date('Y-m-d', $strikefile->timecreated);

        $a->contexturl = $contexturl->out(false);
        $a->contexturlname = $contexturlname;

        $admin = get_admin();

        if ((bool) $config->get('strike_useadminnotifications')) {
            $a->fullname = fullname($admin);
            // Send message to admin. 'Current' language always!
            $subject = get_string('notification:subject', 'plagiarism_strike', $site->fullname);
            $messagehtml = get_string('notification:errormessage', 'plagiarism_strike', $a);
            // Send off.
            self::message_send($admin, $subject, $messagehtml, $contexturl, $contexturlname, $course->id);
        }
        if ((bool) $config->get('strike_useteachernotifications')) {
            // Send message to teacher(s).
            $teacherids = $config->get('strike_notifyteachers');
            if (!empty($teacherids)) {
                $idslist = array_map('intval', array_map('trim', explode(',', $teacherids)));
                $teachers = $DB->get_records_list('user', 'id', $idslist);
                $sm = get_string_manager();
                foreach ($teachers as $teacher) {
                    // Skip if admin.
                    if ($teacher->id == $admin->id) {
                        continue;
                    }
                    $a->fullname = fullname($teacher);

                    $subject = $sm->get_string('notification:subject', 'plagiarism_strike', $a, $teacher->lang);
                    $message = $sm->get_string('notification:errormessage', 'plagiarism_strike', $a, $teacher->lang);
                    self::message_send($teacher, $subject, $message, $contexturl, $contexturlname, $course->id);
                }
            }
        }
    }

    /**
     * Dispatch message.
     *
     * @param \stdClass $userto
     * @param string $subject
     * @param string $messagehtml
     * @param \moodle_url $contexturl
     * @param string $contexturlname
     * @param int $courseid Course ID, needed for Moodle 3.2
     * @return int|bool the integer ID of the new message or false if there was a problem with submitted data
     */
    protected static function message_send($userto, $subject, $messagehtml, $contexturl, $contexturlname, $courseid) {
        global $CFG;
        $supportuser = \core_user::get_support_user();
        $noreplyuser = \core_user::get_noreply_user();
        $message = new \core\message\message();
        $message->component = 'plagiarism_strike';
        $message->name = 'strikenotification';
        $message->userfrom = $supportuser;
        $message->userto = $userto;
        $message->subject = $subject;
        $message->fullmessage = format_text_email($messagehtml, FORMAT_HTML);
        $message->fullmessageformat = FORMAT_PLAIN;
        $message->fullmessagehtml = $messagehtml;
        $message->smallmessage = $message->fullmessage;
        $message->notification = 1;
        $message->contexturl = $contexturl;
        $message->contexturlname = $contexturlname;
        $message->replyto = $noreplyuser->email;
        $message->attachment = '';
        $message->attachname = '';
        if ($CFG->branch >= 32) {
            // Added since Moodle 3.2.
            $message->courseid = $courseid;
            $message->modulename = 'moodle';
            $message->timecreated = time();
        }

        // Send off.
        return message_send($message);
    }

}