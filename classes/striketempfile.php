<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Temp file implementation for plagiarism_strike
 *
 * File         striketempfile.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace plagiarism_strike;

defined('MOODLE_INTERNAL') || die();

/**
 * plagiarism_strike\striketempfile
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class striketempfile {

    /**
     * Full path to created file
     * @var string
     */
    protected $filepath;

    /**
     * Get the filepath
     * @return string
     */
    public function get_filepath() {
        return $this->filepath;
    }

    /**
     * Create a new temp file from the given contents and extension
     *
     * @param string $filecontent
     * @param string $fileext
     */
    public function __construct($filecontent, $fileext) {
        global $CFG;
        $basedir = $CFG->tempdir . '/striketmp';
        if (!check_dir_exists($basedir, true, true)) {
            make_writable_directory($basedir);
        }
        $filename = "content-" . random_string(8) . '.' . ltrim($fileext, '.');
        $filepath = $basedir . '/' . $filename;
        if (debugging()) {
            echo "creating tmp file {$filepath}";
        }
        $fd = fopen($filepath, 'wb');
        fwrite($fd, $filecontent);
        fclose($fd);

        $this->filepath = $filepath;
    }

    /**
     * Destroy instance. Will delete temp file if it still exists.
     */
    public function __destruct() {
        // Unlink file.
        if (file_exists($this->filepath)) {
            if (debugging()) {
                echo "removing tmp file {$this->filepath}";
            }
            unlink($this->filepath);
        }
    }

}
