<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Observer class for plagiarism_strike
 *
 * File         observer.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace plagiarism_strike;

defined('MOODLE_INTERNAL') || die();

/**
 * Observer class for plagiarism_strike
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class observer {

    /**
     * Observer function to handle the assessable_uploaded event in mod_assign.
     * @param \assignsubmission_file\event\assessable_uploaded $event
     */
    public static function assignsubmission_file_uploaded(
    \assignsubmission_file\event\assessable_uploaded $event) {
        global $CFG;
        require_once($CFG->dirroot . '/plagiarism/strike/lib.php');
        $eventdata = $event->get_data();
        $eventdata['eventtype'] = 'assignsubmission_file_uploaded';
        $eventdata['other']['eventtype'] = 'file_uploaded';
        $eventdata['other']['modulename'] = 'assign';
        $strike = new \plagiarism_plugin_strike();
        $strike->event_handler($eventdata);
    }

    /**
     * Observer function to handle the assessable_uploaded event in mod_assign onlinetext.
     * @param \assignsubmission_onlinetext\event\assessable_uploaded $event
     */
    public static function assignsubmission_onlinetext_uploaded(
    \assignsubmission_onlinetext\event\assessable_uploaded $event) {
        global $CFG;
        require_once($CFG->dirroot . '/plagiarism/strike/lib.php');
        $eventdata = $event->get_data();
        $eventdata['eventtype'] = 'assignsubmission_onlinetext_uploaded';
        $eventdata['other']['eventtype'] = 'content_uploaded';
        $eventdata['other']['modulename'] = 'assign';
        $strike = new \plagiarism_plugin_strike();
        $strike->event_handler($eventdata);
    }

    /**
     * Observer function to handle the assessable_submitted event in mod_assign.
     * @param \mod_assign\event\assessable_submitted $event
     */
    public static function assignsubmission_submitted(
    \mod_assign\event\assessable_submitted $event) {
        global $CFG;
        require_once($CFG->dirroot . '/plagiarism/strike/lib.php');
        $eventdata = $event->get_data();
        $eventdata['eventtype'] = 'assignsubmission_submitted';
        $eventdata['other']['eventtype'] = 'assessable_submitted';
        $eventdata['other']['modulename'] = 'assign';
        $strike = new \plagiarism_plugin_strike();
        $strike->event_handler($eventdata);
    }

    /**
     * Observer function to handle the assessable_uploaded event in mod_forum.
     * @param \mod_forum\event\assessable_uploaded $event
     */
    public static function forum_file_uploaded(
    \mod_forum\event\assessable_uploaded $event) {
        global $CFG;
        require_once($CFG->dirroot . '/plagiarism/strike/lib.php');
        $eventdata = $event->get_data();
        $eventdata['eventtype'] = 'forum_file_uploaded';
        $eventdata['other']['eventtype'] = 'assessable_submitted';
        $eventdata['other']['modulename'] = 'forum';
        $strike = new \plagiarism_plugin_strike();
        $strike->event_handler($eventdata);
    }

    /**
     * Observer function to handle the assessable_uploaded event in mod_workshop.
     * @param \mod_workshop\event\assessable_uploaded $event
     */
    public static function workshop_file_uploaded(
    \mod_workshop\event\assessable_uploaded $event) {
        global $CFG;
        require_once($CFG->dirroot . '/plagiarism/strike/lib.php');
        $eventdata = $event->get_data();
        $eventdata['eventtype'] = 'workshop_file_uploaded';
        $eventdata['other']['eventtype'] = 'assessable_submitted';
        $eventdata['other']['modulename'] = 'workshop';
        $strike = new \plagiarism_plugin_strike();
        $strike->event_handler($eventdata);
    }

    /**
     * Handle the assignment assessable_submitted event.
     * @param \mod_coursework\event\assessable_uploaded $event
     */
    public static function coursework_submitted(
    \mod_coursework\event\assessable_uploaded $event) {
        global $CFG;
        require_once($CFG->dirroot . '/plagiarism/strike/lib.php');
        $eventdata = $event->get_data();
        $eventdata['eventtype'] = 'coursework_assessable_submitted';
        $eventdata['other']['eventtype'] = 'assessable_submitted';
        $eventdata['other']['modulename'] = 'coursework';
        $plugin = new \plagiarism_plugin_strike();
        $plugin->event_handler($eventdata);
    }

    /**
     * Handle the course_module_deleted event.
     * @param \core\event\course_module_deleted $event
     */
    public static function course_module_deleted(
    \core\event\course_module_deleted $event) {
        global $DB;
        $eventdata = $event->get_data();
        $DB->delete_records('plagiarism_strike_files', array('cm' => $eventdata['contextinstanceid']));
        $DB->delete_records('plagiarism_strike_config', array('cm' => $eventdata['contextinstanceid']));
    }

    /**
     * Handle the course_module_updated event.
     * @param \core\event\course_module_updated $event
     */
    public static function course_module_updated(
    \core\event\course_module_updated $event) {
        $eventdata = $event->get_data();
    }

    /**
     * Handle the course_module_deleted event.
     * @param \core\event\course_reset_ended $event
     */
    public static function course_reset(
    \core\event\course_reset_ended $event) {
        global $CFG;
        require_once($CFG->dirroot . '/plagiarism/strike/lib.php');
        $eventdata = $event->get_data();
        $plugin = new \plagiarism_plugin_strike();
        $plugin->course_reset($eventdata);
    }

}
