<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Main "util" class for plagiarism_strike
 *
 * File         strikeplagiarism.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace plagiarism_strike;

defined('MOODLE_INTERNAL') || die();

use plagiarism_strike\configplagiarism;

require_once($CFG->dirroot . '/plagiarism/strike/lib.php');

/**
 * plagiarism_strike\strikeplagiarism
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class strikeapitest {

    /**
     * Is strike working fine?
     *
     * @param string|null $key api key
     * @param string|null $server api server address
     * @return \stdClass
     */
    public static function test_connection($key = null, $server = null) {
        static::require_api_files();
        $api = new \strikeplagiarism\api\account\settings();
        if (!$key) {
            $key = configplagiarism::get('strike_key');
        }
        if (!$server) {
            $server = configplagiarism::get('strike_server');
        }
        $api->set_apitoken($key);
        $api->set_apiurl($server);
        $api->set_apisecure(false);
        self::setup_api_debugging($api);

        $response = new \stdClass();

        try {
            $rs = $api->do_request();
            $info = $api->get_http_info();
            // Now store GUID.
            $response->simFact2Limit = $rs['simFact2Limit'];
            $response->statuscode = $info['http_code'];
            $response->errorresponse = 'Connection ok';
        } catch (\strikeplagiarism\api\exception\httpstatuserror $hex) {
            $response->details = "API HTTP Status exception: " . $hex->getCode() . ': ' . $hex->getMessage();
            $response->details2 = $hex->getTraceAsString();
            // API threw exception.
            $response->statuscode = $hex->get_httpstatuscode();

            $httperrors = array(
                400 => "Bad Request",
                401 => "Unauthorized",
                402 => "Payment Required",
                403 => "Forbidden",
                404 => "Not Found",
                405 => "Method Not Allowed",
                406 => "Not Acceptable",
                407 => "Proxy Authentication Required",
                408 => "Request Timeout",
                409 => "Conflict",
                410 => "Gone",
                411 => "Length required",
                412 => "Precondition Failed",
                413 => "Request Entity Too Large",
                414 => "Request-URI Too Long",
                415 => "Unsupported Media Type",
                416 => "Requested Range Not Satisfiable",
                417 => "Expectation Failed",
                418 => "I’m a teapot",
                451 => "Unavailable For Legal Reasons",
                500 => "Internal Server Error",
                501 => "Not Implemented",
                502 => "Bad Gateway",
                503 => "Service Unavailable",
                504 => "Gateway Timeout",
                505 => "HTTP Version Not Supported",
                506 => "Variant Also Negotiates",
                507 => "Insufficient Storage (WebDAV)",
                508 => "Loop Detected (WebDAV)",
                509 => "Bandwidth Limit Exceeded",
                510 => "Not Extended",
                511 => "Network Authentication Required",
            );

            $response->errorresponse = 'HTTP Server Error: ' . $httperrors[$response->statuscode];
        } catch (\strikeplagiarism\api\exception $aex) {
            $response->details = "API exception: " . $aex->getCode() . ': ' . $aex->getMessage();
            $response->details2 = $aex->getTraceAsString();
            // API threw exception.
            $response->statuscode = $aex->getCode();
            $response->errorresponse = $aex->getMessage();
        } catch (\Exception $ex) {
            $response->details = "Unknown exception: " . $ex->getCode() . ': ' . $ex->getMessage();
            $response->details2 = $ex->getTraceAsString();
            // Some generic exception was thrown.
            $response->statuscode = $aex->getCode();
            $response->errorresponse = $aex->getMessage();
        }

        return $response;
    }

    /**
     * This is a utility method to include all external API files.
     */
    static public function require_api_files() {
        global $CFG;
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/debugger/idebugger.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/debugger/output.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/debugger/phperrorlog.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/debugger/file.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/debugger/email.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/exception.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/exception/apikeyexpired.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/exception/badrequest.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/exception/servererror.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/exception/parseerror.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/exception/httpstatuserror.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/document/add.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/document/addref.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/document/get.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/document/getreport.php');
        require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/api/account/settings.php');
    }

    /**
     * Setup API debugging.
     *
     * @param \strikeplagiarism\api $api
     * @return void
     */
    protected static function setup_api_debugging(\strikeplagiarism\api $api) {
        $dodebug = (bool) configplagiarism::get('strike_enableapidebugging');
        if (!$dodebug) {
            return;
        }
        $api->set_debug($dodebug);
        if (configplagiarism::get('strike_debuggingmtrace')) {
            $api->add_debugger(new apidebuggermtrace());
        }
        if (configplagiarism::get('strike_debuggingerrorlog')) {
            $api->add_debugger(new \strikeplagiarism\api\debugger\phperrorlog());
        }
        if (configplagiarism::get('strike_debuggingemail')) {
            $emailaddress = configplagiarism::get('strike_debuggingemailaddress');
            if (!empty($emailaddress)) {
                $api->add_debugger(new \strikeplagiarism\api\debugger\email($emailaddress));
            }
        }
    }

}
