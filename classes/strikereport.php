<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * ORM Report class for plagiarism_strike
 *
 * File         strikereport.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace plagiarism_strike;

defined('MOODLE_INTERNAL') || die();

/**
 * plagiarism_strike\strikereport
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class strikereport {

    /**
     * DB Storage table
     * @var string
     */
    private static $table = 'plagiarism_strike_reports';

    /**
     * table ID / record PKEY
     * @var int
     */
    public $id;
    /**
     * Strike file ID
     * @var int
     */
    public $strikefileid;
    /**
     * HTML report
     * @var string
     */
    public $report;
    /**
     * Document GUID
     * @var string
     */
    public $guid;
    /**
     * Time of record creation
     * @var int
     */
    public $timecreated;
    /**
     * Time of record modification
     * @var int
     */
    public $timemodified;

    /**
     * Create a new instance given the parameters to fill the object with
     *
     * @param array|\stdClass $args key-values
     * @return \static
     */
    static public function create($args) {
        $self = new static();
        $self->load_data($args);
        return $self;
    }

    /**
     * Load the parameters to this object
     *
     * @param array|\stdClass $data key-values
     * @return \static
     */
    protected function load_data($data) {
        foreach ($data as $key => $value) {
            $m = "set_{$key}";
            if (method_exists($this, $m)) {
                call_user_func(array($this, $m), $value);
            } else if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }

    /**
     * Get the storage table
     * @return string Database storage table
     */
    public static function table() {
        return static::$table;
    }

    /**
     * Get object by id
     * @param int $id
     * @return static
     */
    public static function get_by_id($id) {
        global $DB;
        $rs = $DB->get_record(static::table(), array('id' => $id));
        return static::create($rs);
    }

    /**
     * Get object by guid
     * @param string $guid
     * @param bool $mustexist
     * @return static
     */
    public static function get_by_guid($guid, $mustexist = true) {
        global $DB;
        $conditions = array('guid' => $guid);
        $rs = $DB->get_record(static::table(), $conditions, '*', $mustexist ? MUST_EXIST : IGNORE_MISSING);
        if (empty($rs)) {
            $rs = $conditions;
        }
        return static::create($rs);
    }

    /**
     * Get records based on given conditions
     * @param array|null $conditions
     * @return static[]
     */
    public static function get($conditions) {
        global $DB;
        $rs = $DB->get_records(static::table(), $conditions);
        $collection = [];
        foreach ($rs as $key => $record) {
            $collection[$key] = static::create($record);
        }
        return $collection;
    }

    /**
     * Remove object from database
     * @return bool
     */
    public function delete() {
        global $DB;
        return $DB->delete_records(static::table(), array('id' => $this->id));
    }

    /**
     * Save object to database
     * @return bool|int
     */
    public function save() {
        if (!empty($this->id)) {
            return $this->update();
        } else {
            return $this->insert();
        }
    }

    /**
     * Insert object in database
     * @return int primary key
     */
    protected function insert() {
        global $DB;
        $this->timecreated = time();
        $this->timemodified = $this->timecreated;
        return $DB->insert_record(static::table(), $this);
    }

    /**
     * Update object in database
     * @return bool
     */
    protected function update() {
        global $DB;
        $this->timemodified = time();
        return $DB->update_record(static::table(), $this);
    }

}