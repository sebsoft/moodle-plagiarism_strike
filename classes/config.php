<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Config class for plagiarism_strike
 *
 * File         config.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace plagiarism_strike;

defined('MOODLE_INTERNAL') || die();

/**
 * plagiarism_strike\config
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class config {

    /**
     *
     * @var whether or not config has been loaded yet
     */
    private $loaded = false;

    /**
     *
     * @var \stdClass the tool configuration defaults
     */
    private $configdefault = null;

    /**
     *
     * @var \stdClass the tool configuration
     */
    private $config = null;

    /**
     *
     * @var int Course module ID
     */
    private $cmid = null;

    /**
     * Create  new instance of the config for a specific course module
     *
     * @param int $cmid
     */
    public function __construct($cmid) {
        if (empty($cmid)) {
            $cmid = null;
        }
        $this->cmid = $cmid;
        $this->init();
    }

    /**
     * initialize the tool configuration
     */
    private function init() {
        global $DB;
        if (!$this->loaded) {
            $sql = 'SELECT name,value FROM {plagiarism_strike_config} WHERE cm IS NULL';
            $this->configdefault = (object)$DB->get_records_sql_menu($sql);
            if ($this->cmid === null) {
                $this->config = new \stdClass();
            } else {
                $sql = 'SELECT name,value FROM {plagiarism_strike_config} WHERE cm = ?';
                $this->config = (object)$DB->get_records_sql_menu($sql, array($this->cmid));
            }
        }
    }

    /**
     * get the configuration object
     *
     * @return \stdClass
     */
    public function get_config() {
        $this->init();
        return $this->config;
    }

    /**
     * get the configuration object
     *
     * @return \stdClass
     */
    public function get_configdefault() {
        $this->init();
        return $this->configdefault;
    }

    /**
     * get a configuration value
     *
     * @param string $name config name
     * @param bool $nodefaults true if the default value should NOT be returned.
     * @return mixed
     */
    public function get($name, $nodefaults = false) {
        $this->init();
        if (isset($this->config->$name)) {
            return $this->config->$name;
        }
        if ($nodefaults === false && isset($this->configdefault->$name)) {
            return $this->configdefault->$name;
        }
        return null;
    }

    /**
     * set a configuration value
     *
     * @param string $name config name
     * @param mixed $value config value
     * @param bool $force force insert if this value does not exist
     * @return mixed
     */
    public function set($name, $value, $force = true) {
        global $DB;
        $this->init();
        if (isset($this->config->$name) || $force) {
            $record = $DB->get_record('plagiarism_strike_config', array('name' => $name, 'cm' => $this->cmid));
            if (empty($record)) {
                $record = (object) array(
                    'cm' => $this->cmid,
                    'name' => $name,
                    'value' => $value,
                    'config_hash' => "{$this->cmid}_{$name}"
                );
                $DB->insert_record('plagiarism_strike_config', $record);
            } else {
                $record->value = $value;
                $DB->update_record('plagiarism_strike_config', $record);
            }
            // Reload.
            $this->invalidate();
        }
        return $this->get($name);
    }

    /**
     * invalidates the cached configuration and reloads from database
     */
    public function invalidate() {
        $this->loaded = false;
        $this->configdefault = null;
        $this->config = null;
        $this->init();
    }

}