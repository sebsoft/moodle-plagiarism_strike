<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Report removal class for plagiarism_strike
 *
 * File         strikereport.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace plagiarism_strike;

defined('MOODLE_INTERNAL') || die();

/**
 * plagiarism_strike\strikereport
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class strikeremoval {

    /**
     * course module id
     * @var int
     */
    protected $cm;
    /**
     * user id
     * @var int
     */
    protected $userid;
    /**
     * related user id
     * @var int
     */
    protected $relateduserid;

    /**
     * @var array "hashes"
     */
    private $hashes;

    /**
     * Create a new instance
     * @param int $cmid
     * @param int $userid
     * @param int $relateduserid
     */
    public function __construct($cmid, $userid, $relateduserid = 0) {
        $this->cm = $cmid;
        $this->userid = $userid;
        $this->relateduserid = $relateduserid;
        $this->hashes = [0 => [], 1 => []];
    }

    /**
     * Add a known hash.
     * @param string $hash
     * @param int $isstoredfile
     */
    public function add_no_delete_hash($hash, $isstoredfile = 0) {
        $this->hashes[$isstoredfile][] = $hash;
    }

    /**
     * Perform cleanup
     */
    public function cleanup() {
        global $DB;
        // Check config for cleanup.
        $config = new config($this->cm);
        if ($config->get('strike_deletereport_method') === PLAGIARISM_STRIKE_DELETE_REPORT_KEEP) {
            // Keep old reports.
            return;
        }
        // Perform cleanup.
        foreach ($this->hashes as $isstoredfile => $hashes) {
            if (empty($hashes)) {
                // Refuse to cleanup when we have no hashes!
                continue;
            }
            $usersql = '';
            $userparams = [];
            if (!empty($this->relateduserid)) {
                $usersql = '((userid = :userid AND relateduserid = :relateduserid) OR
                         (userid = :altuserid AND (relateduserid IS NULL OR relateduserid = 0)))';
                $userparams['userid'] = $this->userid;
                $userparams['relateduserid'] = $this->relateduserid;
                $userparams['altuserid'] = $this->relateduserid;
            } else {
                $usersql = 'userid = :userid';
                $userparams['userid'] = $this->userid;
            }

            list($insql, $params) = $DB->get_in_or_equal($hashes, SQL_PARAMS_NAMED, 'hash', true, 0);
            $params['cm'] = $this->cm;
            $params['isstoredfile'] = $isstoredfile;
            $params += $userparams;
            $select = $usersql . ' AND isstoredfile = :isstoredfile AND cm = :cm AND contenthash '.$insql;
            $recordshave = strikefile::get_select($select, $params);

            list($notinsql, $params) = $DB->get_in_or_equal($hashes, SQL_PARAMS_NAMED, 'hash', false, 0);
            $params['cm'] = $this->cm;
            $params['isstoredfile'] = $isstoredfile;
            $params += $userparams;
            $select = $usersql . ' AND isstoredfile = :isstoredfile AND cm = :cm AND contenthash '.$notinsql;
            $recordsnothave = strikefile::get_select($select, $params);

            foreach ($recordsnothave as $strikefile) {
                $strikefile->delete();
            }
        }

    }

}