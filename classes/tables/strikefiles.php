<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * strikefiles table class for plagiarism_strike
 *
 * File         strikefiles.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace plagiarism_strike\tables;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/tablelib.php');

/**
 * plagiarism_strike\tables\strikefiles
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class strikefiles extends \table_sql {

    /**
     * Course module or course module id
     *
     * @var int|\stdClass
     */
    protected $cm;

    /**
     * Analysis string
     * @var string
     */
    protected $stranalysedocument;

    /**
     * Add-to-index string
     * @var string
     */
    protected $strindexdocument;

    /**
     * Delete strike file
     * @var string
     */
    protected $strdelete;

    /**
     * strike renderer
     * @var \plagiarism_strike_renderer
     */
    protected $renderer;

    /**
     * Create a new instance of the table
     *
     * @param int|\stdClass $cmorid Course module or course module id
     */
    public function __construct($cmorid) {
        global $USER, $PAGE;
        $cmid = (isset($cmorid->id) ? $cmorid->id : $cmorid);
        parent::__construct(__CLASS__ . '-' . $USER->id . '-' . ((int) $cmid));
        $this->cm = $cmorid;
        $this->stranalysedocument = get_string('sendforanalysis', 'plagiarism_strike');
        $this->strindexdocument = get_string('indexdocument', 'plagiarism_strike');
        $this->strdelete = get_string('deletestrikefile', 'plagiarism_strike');
        $this->renderer = $PAGE->get_renderer('plagiarism_strike');
    }

    /**
     * Display the table.
     *
     * @param int $pagesize
     * @param bool $useinitialsbar
     */
    public function render($pagesize, $useinitialsbar = true) {
        $this->define_table_columns(array('user', 'author', 'modname', 'analysisstatus', 'filename',
            'reportstatus', 'status', 'timesubmitted', 'actions'));
        // We won't be able to sort by most columns.
        $this->no_sorting('analysisstatus');
        $this->no_sorting('reportstatus');
        $this->no_sorting('status');
        $this->no_sorting('actions');

        $this->out($pagesize, $useinitialsbar);
    }

    /**
     * Get the complete query to generate the table.
     *
     * @param bool $forcount if true, generates query for counting.
     * @return array array consisting of query and parameters
     */
    protected function get_query($forcount = false) {
        global $DB;
        if ($forcount) {
            $fields = 'count(sf.id)';
            $from = '{' . \plagiarism_strike\strikefile::table() . '} sf';
        } else {
            $fields = 'sf.*, r.id as reportid';
            $fields .= ', ' . $DB->sql_fullname('u1.firstname', 'u1.lastname') . ' as userfullname';
            $fields .= ', ' . $DB->sql_fullname('u2.firstname', 'u2.lastname') . ' as relateduserfullname';
            $fields .= ', m.name as modname, cm.instance as submissionid';

            $from = '{' . \plagiarism_strike\strikefile::table() . '} sf';
            $from .= ' JOIN {course_modules} cm ON sf.cm=cm.id';
            $from .= ' JOIN {modules} m ON cm.module=m.id';
            $from .= ' JOIN {user} u1 ON u1.id=sf.userid';

            $from .= ' LEFT JOIN {user} u2 ON u2.id=sf.relateduserid';
            $from .= ' LEFT JOIN {' . \plagiarism_strike\strikereport::table() . '} r ON (sf.id=r.strikefileid AND sf.guid=r.guid)';
        }

        $where = array('sf.cm = ?');
        $params = array(isset($this->cm->id) ? $this->cm->id : $this->cm);

        $sql = "SELECT {$fields} FROM {$from} WHERE " . implode(' AND ', $where);
        return array($sql, $params);
    }

    /**
     * Query the db. Store results in the table object for use by build_table.
     *
     * @param int $pagesize size of page for paginated displayed table.
     * @param bool $useinitialsbar do you want to use the initials bar. Bar
     * will only be used if there is a fullname column defined for the table.
     */
    public function query_db($pagesize, $useinitialsbar = true) {
        global $DB;

        // Get count / data.
        list($csql, $cparams) = $this->get_query(true);

        if (!$this->is_downloading()) {
            $total = $DB->count_records_sql($csql, $cparams);
            $this->pagesize($pagesize, $total);
        }

        // Fetch the attempts.
        list($sql, $params) = $this->get_query(false);
        $sort = $this->get_sql_sort();
        if ($sort) {
            $sort = " ORDER BY $sort";
        }
        $sql .= $sort;

        if (!$this->is_downloading()) {
            $reportdata = $DB->get_records_sql($sql, $params, $this->get_page_start(), $this->get_page_size());
        } else {
            $reportdata = $DB->get_records_sql($sql, $params);
        }

        // Relate validity.
        foreach ($reportdata as &$record) {
            $record->valid = true;
            $record->referror = false;
            $userid = (!empty($record->relateduserid) ? $record->relateduserid : $record->userid);
            if ($record->isstoredfile) {
                switch ($record->modname) {
                    case 'assign':
                        $params = ['assignment' => $record->submissionid, 'userid' => $userid];
                        $submission = $DB->get_record('assign_submission', $params);
                        $params = ['contenthash' => $record->contenthash, 'userid' => $record->userid, 'itemid' => $submission->id];
                        $record->valid = $DB->record_exists('files', $params);
                        $record->referror = (int) !$record->valid;
                        break;
                    case 'forum':
                        $params = ['discussion' => $record->submissionid, 'userid' => $userid];
                        $submission = $DB->get_record('forum_posts', $params);
                        $params = ['contenthash' => $record->contenthash, 'userid' => $record->userid, 'itemid' => $submission->id];
                        $record->valid = $DB->record_exists('files', $params);
                        $record->referror = (int) !$record->valid;
                        break;
                    case 'workshop':
                        $params = ['workshopid' => $record->submissionid, 'authorid' => $userid];
                        $submission = $DB->get_record('workshop_submissions', $params);
                        $params = ['contenthash' => $record->contenthash, 'userid' => $record->userid, 'itemid' => $submission->id];
                        $record->valid = $DB->record_exists('files', $params);
                        $record->referror = (int) !$record->valid;
                        break;
                    case 'coursework':
                        break;
                }
            } else {
                switch ($record->modname) {
                    case 'assign':
                        $params = ['assignment' => $record->submissionid, 'userid' => $userid];
                        $submission = $DB->get_record('assign_submission', $params);
                        $params = ['submission' => $submission->id, 'assignment' => $submission->assignment];
                        $onlinetext = $DB->get_record('assignsubmission_onlinetext', $params);
                        $hash = sha1(plagiarism_strike_format_temp_content_text($onlinetext->onlinetext, true));
                        $record->valid = ($record->contenthash == $hash);
                        $record->referror = (int) !$record->valid;
                        break;
                    case 'forum':
                        // TODO: what text do we need to check here?
                        $record->valid = true;
                        $record->referror = (int) !$record->valid;
                        break;
                    case 'workshop':
                        // TODO: what text do we need to check here?
                        $record->valid = true;
                        $record->referror = (int) !$record->valid;
                        break;
                    case 'coursework':
                        break;
                }
            }
        }
        unset($record);

        $this->rawdata = $reportdata;
    }

    /**
     * Convenience method to call a number of methods for you to display the table.
     *
     * @param int $pagesize
     * @param bool $useinitialsbar
     * @param mixed $downloadhelpbutton unused
     */
    public function out($pagesize, $useinitialsbar, $downloadhelpbutton = '') {
        $this->setup();
        $this->query_db($pagesize, $useinitialsbar);
        $this->build_table();
        $this->finish_output();
    }

    /**
     * Render visual representation of the 'user' column for use in the table
     *
     * @param \stdClass $row
     * @return string time string
     */
    public function col_user($row) {
        global $CFG;
        $url = new \moodle_url($CFG->wwwroot . '/user/profile.php', array('id' => $row->userid));
        return \html_writer::link($url, $row->userfullname);
    }

    /**
     * Render visual representation of the 'relateduser' column for use in the table
     *
     * @param \stdClass $row
     * @return string time string
     */
    public function col_author($row) {
        global $CFG;
        if (empty($row->relateduserid)) {
            $author = $row->userid;
            $authorname = $row->userfullname;
        } else {
            $author = $row->relateduserid;
            $authorname = $row->relateduserfullname;
        }
        $url = new \moodle_url($CFG->wwwroot . '/user/profile.php', array('id' => $author));
        return \html_writer::link($url, $authorname) . '<br/>(' . $row->author . ')';
    }

    /**
     * Render visual representation of the 'timesubmitted' column for use in the table
     *
     * @param \stdClass $row
     * @return string time string
     */
    public function col_timesubmitted($row) {
        return userdate($row->timesubmitted);
    }

    /**
     * Render visual representation of the 'reportstatus' column for use in the table
     *
     * @param \stdClass $row
     * @return string
     */
    public function col_reportstatus($row) {
        if ((bool) $row->reportready) {
            if (empty($row->reportid)) {
                return get_string('report:ready', 'plagiarism_strike');
            } else {
                $url = \plagiarism_strike\strikeplagiarism::get_report_url($row);
                $img = \plagiarism_strike\strikeplagiarism::get_image('report', 'report');
                return \html_writer::link($url, $img);
            }
        }
        return get_string('report:na', 'plagiarism_strike');
    }

    /**
     * Render visual representation of the 'analysisstatus' column for use in the table
     *
     * @param \stdClass $row
     * @return string
     */
    public function col_analysisstatus($row) {
        if ($row->reportready == 1 || $row->statuscode == 'complete') {
            $ranking = '';
            if (!empty($row->factor1) || !empty($row->factor2)) {
                $ranking1 = new \plagiarism_strike\renderable\ranking(round(100 * $row->factor1, 1));
                $ranking2 = new \plagiarism_strike\renderable\ranking(round(100 * $row->factor2, 1));
                // User is allowed to view only the score.
                $ranking .= get_string('similarity1', 'plagiarism_strike') . ': ';
                $ranking .= $this->renderer->render($ranking1) . '<br/>';
                $ranking .= get_string('similarity2', 'plagiarism_strike') . ': ';
                $ranking .= $this->renderer->render($ranking2);
            }
            return '<span class="strikeplagiarismreport">' . $ranking . '</span>';
        }
        if ((bool) $row->automatedanalysis) {
            return get_string('analysis:auto', 'plagiarism_strike');
        } else if ((bool) $row->doanalysis === false) {
            return get_string('analysis:awaitchoice', 'plagiarism_strike');
        } else {
            return get_string('analysis:manual', 'plagiarism_strike');
        }
    }

    /**
     * Render visual representation of the 'status' column for use in the table
     *
     * @param \stdClass $row
     * @return string
     */
    public function col_status($row) {
        return $row->statuscode;
    }

    /**
     * Get custom row class
     *
     * @param \stdClass $row
     * @return string
     */
    public function get_row_class($row) {
        if ($row->referror) {
            return 'notifyproblem';
        }
        return '';
    }

    /**
     * Render visual representation of the 'action' column for use in the table
     *
     * @param \stdClass $row
     * @return string actions
     */
    public function col_actions($row) {
        $actions = array();
        $analyseallowfrom = ['pending', 'awaitanalysischoice'];
        if ($row->doanalysis == 0 && $row->automatedanalysis == 0 &&
                in_array($row->statuscode, $analyseallowfrom) && !$row->referror) {
            $actions[] = $this->get_action($row, 'analysedocument');
        }

        $indexallowfrom = ['complete', 'new'];
        if ($row->indexed == 0 && in_array($row->statuscode, $indexallowfrom)) {
            $actions[] = $this->get_action($row, 'indexdocument');
        }

        if ($row->referror) {
            $actions[] = $this->get_action($row, 'delete');
        }
        return implode('', $actions);
    }

    /**
     * Return the image tag representing an action image
     *
     * @param string $action
     * @return string HTML image tag
     */
    protected function get_action_image($action) {
        global $OUTPUT, $CFG;
        $icon = '';
        $actionstr = 'str' . $action;
        $attributes = [];
        if ($CFG->branch >= 33) {
            $icon = $OUTPUT->image_url($action, 'plagiarism_strike');
        } else {
            $icon = $OUTPUT->pix_url($action, 'plagiarism_strike');
        }
        return \html_writer::img($icon, $this->{$actionstr}, $attributes);
    }

    /**
     * Return a string containing the link to an action
     *
     * @param \stdClass $row
     * @param string $action
     * @return string link representing the action with an image
     */
    protected function get_action($row, $action) {
        $actionstr = 'str' . $action;
        return '<a href="' . new \moodle_url($this->baseurl, array('action' => $action, 'id' => $row->id)) .
                '" alt="' . $this->{$actionstr} .
                '" title="' . $this->{$actionstr} .
                '">' . $this->get_action_image($action) . '</a>';
    }

    /**
     * Define columns for output table and define the headers through automated
     * lookup of the language strings.
     *
     * @param array $columns list of column names
     */
    protected function define_table_columns($columns) {
        $this->define_columns($columns);
        $headers = array();
        foreach ($columns as $name) {
            $headers[] = get_string('label:' . $name, 'plagiarism_strike');
        }
        $this->define_headers($headers);
    }

}
