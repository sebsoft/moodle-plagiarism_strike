<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Callback script for plagiarism_strike
 *
 * File         strikecallback.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// No login check is expected since this is a callback script.
// @codingStandardsIgnoreLine
require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot . '/plagiarism/strike/3rdp/strikeplagiarism/notifier/report.php');

$guid = optional_param('id', null, PARAM_RAW); // Document GUID.

// If we have an empty GUID, this is a bad request, so send that status.
if (empty($guid)) {
    strikeplagiarism\notifier\report::bad_request();
}

try {
    // Process this request: insert ad hoc task to sync the report.
    $task = new \plagiarism_strike\task\adhoc\syncreport();
    $customdata = (object)array('guid' => $guid);
    $task->set_custom_data($customdata);
    core\task\manager::queue_adhoc_task($task);

    // IF we're configured to automatically add the reference, do it now.
    $strikefile = \plagiarism_strike\strikefile::get_by_guid($guid);
    $config = new config($strikefile->cm);
    if (!(bool)$strikefile->indexed &&
            $config->get('strike_addref_method') === PLAGIARISM_STRIKE_ADDREF_AUTO) {
        \plagiarism_strike\strikeplagiarism::index_document($strikefile);
    }

    // And pass on the response.
    strikeplagiarism\notifier\report::send_success();
} catch (Exception $ex) {
    strikeplagiarism\notifier\report::send_error($ex->getMessage());
}