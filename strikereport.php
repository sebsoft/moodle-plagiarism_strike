<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Script to embed report.php in a Moodle page.
 *
 * File         strikereport.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__.'/../../config.php');
require_once($CFG->libdir.'/resourcelib.php');
require_once($CFG->dirroot.'/plagiarism/strike/lib.php');

require_login();

$fid = required_param('pid', PARAM_INT);
$cmid = required_param('cm', PARAM_INT);
$params = array('pid' => $fid, 'cm' => $cmid);
$pageurl = new moodle_url($CFG->wwwroot . '/plagiarism/strike/strikereport.php', $params);

$sf = plagiarism_strike\strikefile::get_by_id($fid);
list($course, $cm) = get_course_and_cm_from_cmid($sf->cm);
$context = context_course::instance($course->id);

$strreport = get_string('report', 'plagiarism_strike');
$strreportclick = get_string('reportclick', 'plagiarism_strike');

$PAGE->set_pagelayout('popup');
$PAGE->set_url($pageurl);
$PAGE->set_context($context);
$PAGE->set_title($strreport);
$PAGE->set_heading($SITE->fullname);
$PAGE->navbar->add($strreport);

// We can not use our popups here, because the url may be arbitrary, see MDL-9823.
$mimetype = 'text/html';
$reporturl = new moodle_url($CFG->wwwroot . '/plagiarism/strike/report.php', array('pid' => $sf->id, 'cm' => $cm->id));
$clicktoopen = '<a href="'.$reporturl.'" onclick="this.target=\'_blank\'">'.$strreportclick.'</a>';

echo $OUTPUT->header();
echo $OUTPUT->heading($strreport);
echo '<div class="noticebox">';
echo resourcelib_embed_general($reporturl, $strreport, $clicktoopen, $mimetype);
echo '</div>';
echo $OUTPUT->footer();