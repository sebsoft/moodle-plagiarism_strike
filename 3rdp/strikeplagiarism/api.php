<?php
/**
 * API implementation for Strike Plagiarism
 *
 * File         api.php
 * Encoding     UTF-8
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace strikeplagiarism;

use api\exception;

/**
 * strikeplagiarism\api
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class api {

    /**
     * Request type GET
     */
    const REQUEST_TYPE_GET = 'GET';

    /**
     * Request type POST
     */
    const REQUEST_TYPE_POST = 'POST';

    /**
     * Request type DELETE
     */
    const REQUEST_TYPE_DELETE = 'DELETE';

    /**
     * Request type PUT
     */
    const REQUEST_TYPE_PUT = 'PUT';

    /**
     * Main base API url
     * @var string
     */
    protected $apiurl = '';

    /**
     * API token
     * @var string
     */
    protected $apitoken = '';

    /**
     * API secure
     * @var string
     */
    protected $apisecure = false;

    /**
     * API version
     * @var string
     */
    protected $version = 'v2';

    /**
     * Endpoint for this API
     * @var string
     */
    protected $endpoint = '';

    /**
     * API action
     * @var string
     */
    protected $action = '';

    /**
     * API request type
     * @var int
     */
    protected $requesttype = self::REQUEST_TYPE_GET;

    /**
     * generated postdata for requests
     * @var string
     */
    protected $postdata = array();

    protected $http_info = array();

    /**
     * force application/x-www-form-urlencoded POST.
     * Use ONLY when you use a post command but want the data to NOT be sent in form-data format
     * @var string
     */
    protected $forceurlencodedpost = false;

    /**
     * Debug?
     * @var bool
     */
    protected $debug = false;
    protected $lastrawresponse;
    protected $debuggers;

    public function add_debugger(api\debugger\idebugger $debugger) {
        if ($this->debuggers == null) {
            $this->debuggers = array();
        }
        $this->debuggers[] = $debugger;
    }

    public function clear_debuggers() {
        $this->debuggers = array();
    }

    protected function debug_message($message) {
        foreach ($this->debuggers as $debugger) {
            $debugger->debug($message);
        }
    }

    public function get_debug() {
        return $this->debug;
    }

    public function set_debug($debug) {
        $this->debug = $debug;
        return $this;
    }

    public function set_apiurl($apiurl) {
        $this->apiurl = $apiurl;
        return $this;
    }

    public function set_apisecure($apisecure) {
        $this->apisecure = (bool) $apisecure;
        return $this;
    }

    /**
     * Set the API token
     * The API token is used to identify your company.
     *
     * @param string $apitoken
     */
    public function set_apitoken($apitoken) {
        $this->apitoken = $apitoken;
    }

    public function __construct() {
        $this->debuggers = array();
    }

    /**
     * Gather the post data
     * @return array
     */
    protected function gather_postdata() {
        return $this->postdata;
    }

    /**
     * Gather the post data
     * @return array
     */
    protected function clear_postdata() {
        $this->postdata = array();
    }

    /**
     * add post data
     * @return array
     */
    protected function add_postdata($name, $value) {
        $this->postdata[$name] = $value;
    }

    /**
     * Require fields on post data
     * @return array
     */
    protected function require_postfields($fields) {
        $errors = [];
        foreach ($fields as $field) {
            // check if this postfield is set
            if (!array_key_exists($field, $this->postdata)) {
                $errors[] = "Required field {$field} missing in post data";
            }
        }
        if (!empty($errors)) {
            throw new api\exception(implode("\n", $errors), 1);
        }
    }

    /**
     * Process the API result
     * @param \stdClass $data
     * @return array
     */
    protected function process_result($data) {
        return $data;
    }

    /**
     * Build the API url for a request
     *
     * @return string
     * @throws exception
     */
    private function get_api_url() {
        if ($this->version == '') {
            throw new api\exception('version not set', 1);
        }
        if ($this->endpoint == '') {
            throw new api\exception('endpoint not set', 1);
        }
//        if ($this->action == '') {
//            throw new api\exception('action not set', 1);
//        }

        // type/version/module/method/resulttype
        $proto = ($this->apisecure ? 'https://' : 'http://');
        $parts = ['api', $this->version, $this->endpoint];
        if (!empty($this->action)) {
            $parts[] = $this->action;
        }
        return $proto . preg_replace('/^https?:\/\//is', '', $this->apiurl) . '/' . implode('/', $parts);
    }

    /**
     * Return the post data
     *
     * @return array
     */
    public function get_postdata() {
        return $this->gather_postdata();
    }

    public function get_http_info(){
        return $this->http_info;
    }
    /**
     * Perform the API request and return results.
     *
     * @return array processed result
     * @throws \coding_exception
     * @throws exception on failure
     */
    public function do_request() {
        $url = $this->get_api_url();
        $data = $this->get_postdata();

        $apiurl = $url;

        if ($this->debug) {
            $this->debug_message("apiurl: $apiurl\ndata: " . print_r($data, true));
        }

        $debugout = fopen('php://temp', 'rw+');

        $ch = curl_init();
        if ($this->requesttype == self::REQUEST_TYPE_GET) {
            $strdata = http_build_query($data, '', '&');
            $apiurl .= '?' . $strdata;
        } else if ($this->requesttype == self::REQUEST_TYPE_POST && $this->forceurlencodedpost) {
            $strdata = http_build_query($data, '', '&');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $strdata);
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        curl_setopt($ch, CURLOPT_URL, $apiurl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_STDERR, $debugout);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 6);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

//        curl_setopt($ch, CURLOPT_INFILESIZE);
//        curl_setopt($ch, CURLOPT_INFILE);

        $this->lastrawresponse = curl_exec($ch);

        rewind($debugout);
        $verboseoutput = stream_get_contents($debugout);
//        fclose($debugout);

        if ($this->debug) {
            $this->debug_message("CURL VERBOSE OUTPUT: " . $verboseoutput);
        }

        $info = curl_getinfo($ch);

        $this->http_info = $info;

        if ($this->debug) {
            $this->debug_message("HTTP STATUS: " . $info['http_code']);
            $this->debug_message("URL: " . htmlentities(urldecode($info['url'])));
            $this->debug_message("RAW response: " . htmlspecialchars($this->lastrawresponse));
            $this->debug_message("FULL curl info: " . print_r($info,1));
        }

        if ($this->lastrawresponse === false) {
            $error = curl_error($ch);
            $errno = curl_errno($ch);
            curl_close($ch);
            throw new api\exception($error, $errno);
        }

        curl_close($ch);

        switch ($info['http_code']) {
            case 400:
                $response = json_decode($this->lastrawresponse, true);
                throw new api\exception\badrequest($response['message']);
                break;
            case 403:
                $response = json_decode($this->lastrawresponse, true);
                throw new api\exception\apikeyexpired($response['message']);
                break;
            case 500:
                $response = json_decode($this->lastrawresponse, true);
                throw new api\exception\servererror($response['message']);
                break;
            case 200:
                break;
            default:
                // We MUST set error state.
                throw new api\exception\httpstatuserror("Unknown error. Http Status = {$info['http_code']}\nRaw server response:\n{$this->lastrawresponse}", $info['http_code']);
        }

        if (stristr($info['content_type'], 'application/json') !== false) {
            if (!empty($this->lastrawresponse)) {
                $result = json_decode($this->lastrawresponse, true);
                if ($result === null) {
                    throw new api\exception\parseerror('result parse error', htmlspecialchars($this->lastrawresponse));
                }
            } else {
                $result = null;
            }
        } elseif (stristr($info['content_type'], 'text/html') !== false) {
            $result = $this->lastrawresponse;
        } else {
            $result = $this->lastrawresponse;
        }

        if ($this->debug) {
            $this->debug_message("DECODED response: " . var_export($this->lastrawresponse, 1));
        }

        if ($this->validate_result($result)) {
            return $this->process_result($result);
        }
    }

    /**
     * Validate API result
     *
     * @param array $result the API result
     * @return bool true if success, false otherwise
     * @throws apiexception if errors occured
     */
    protected function validate_result($result) {
        if (is_scalar($result) || empty($result)) {
            return true;
        }
        if (isset($result['status']) && $result['status'] !== 'error') {
            return true;
        } else {
            if (isset($result['status']) && $result['status'] == 'error') {
                throw new api\exception($result['message']);
            } else {
                $msg = "Unexpected api result\nRaw response: ";
                if ($this->lastrawresponse === false) {
                    $msg .= "Raw response not available (curl returned FALSE)";
                } else {
                    $msg .= $this->lastrawresponse;
                }
                throw new api\exception($msg);
            }
        }
    }

}
