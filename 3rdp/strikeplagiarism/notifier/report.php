<?php
/**
 * Notifier implementation for Strike Plagiarism
 *
 * File         report.php
 * Encoding     UTF-8
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace strikeplagiarism\notifier;

/**
 * strikeplagiarism\notifier\report
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class report {

    /**
     * Send 'bad request' message
     */
    static public function bad_request($message = 'bad request') {
        $response = (object) array(
            'status' => 'error',
            'message' => $message
        );
        header('Content-Type: application/json;charset=UTF-8');
        http_response_code(400);
        echo json_encode($response);
        exit;
    }

    /**
     * Send 'internal erorr' message
     */
    static public function internal_error($message = 'Something went wrong') {
        $response = (object) array(
            'status' => 'error',
            'message' => $message
        );
        header('Content-Type: application/json;charset=UTF-8');
        http_response_code(500);
        echo json_encode($response);
        exit;
    }

    /**
     * Send 'success' message
     */
    static public function send_success($message = 'Notification received') {
        $response = (object) array(
            'status' => 'new',
            'message' => $message
        );
        header('Content-Type: application/json;charset=UTF-8');
        http_response_code(200);
        echo json_encode($response);
        exit;
    }

    /**
     * Send 'error' message
     */
    static public function send_error($message = 'Notification error') {
        $response = (object) array(
            'status' => 'error',
            'message' => $message
        );
        header('Content-Type: application/json;charset=UTF-8');
        http_response_code(200);
        echo json_encode($response);
        exit;
    }

}