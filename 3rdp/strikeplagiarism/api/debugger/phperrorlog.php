<?php
/**
 * Output debugger for Strike Plagiarism
 *
 * File         output.php
 * Encoding     UTF-8
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace strikeplagiarism\api\debugger;

/**
 * strikeplagiarism\api\debugger\phperrorlog
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class phperrorlog implements idebugger {

    public function debug($message) {
        error_log($message, 0);
    }

}
