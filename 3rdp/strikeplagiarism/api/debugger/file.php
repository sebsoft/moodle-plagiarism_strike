<?php
/**
 * Output debugger for Strike Plagiarism
 *
 * File         output.php
 * Encoding     UTF-8
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace strikeplagiarism\api\debugger;

/**
 * strikeplagiarism\api\debugger\file
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class file implements idebugger {

    protected $filepath;
    protected $buffered = true;
    protected $buffer = '';

    public function __construct() {
        $this->buffer = '';
    }

    public function __destruct() {
        $this->write_buffer();
    }

    public function set_filepath($filepath) {
        $this->filepath = $filepath;
        return $this;
    }

    public function set_buffered($buffered) {
        $this->buffered = $buffered;
        return $this;
    }

    public function debug($message) {
        $this->buffer .= "{$message}\n";
        if (!$this->buffered) {
            $this->write_buffer();
        }
    }

    protected function write_buffer() {
        try {
            $data = $this->buffer;
            $this->buffer = '';
            if (!empty($data)) {
                @file_put_contents($this->filepath, $data, FILE_APPEND);
            }
        } catch (Exception $ex) {
            return;
        }
    }

}
