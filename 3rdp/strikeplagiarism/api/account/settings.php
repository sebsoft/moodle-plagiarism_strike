<?php

/**
 * Document GET implementation for Strike Plagiarism
 *
 * File         get.php
 * Encoding     UTF-8
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace strikeplagiarism\api\account;

/**
 * strikeplagiarism\api\account\settings
 *
 * @copyright   INTERSIEC.com.pl / Strikeplagiarism.com
 * @author      Kamil Łuczak <kluczak@intersiec.com.pl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class settings extends \strikeplagiarism\api {

    /**
     * Endpoint for this API
     * @var string
     */
    protected $endpoint = 'account';
    /**
     * Request method
     * @var string
     */
    protected $requesttype = self::REQUEST_TYPE_GET;
    /**
     * API action
     * @var string
     */
    protected $action = 'settings';

    protected $simFact2Limit;

    public function get_simFact2Limit() {
        return $this->simFact2Limit;
    }

    protected function gather_postdata() {
        $this->clear_postdata();
        $this->add_postdata('APIKEY', $this->apitoken);
        return parent::gather_postdata();
    }

    protected function process_result($data) {
        return parent::process_result($data);
    }

    protected function validate_result($result) {
        /*if (is_scalar($result)) {
            return true;
        }*/
        if (isset($result['simFact2Limit'])) {
            return true;
        } else {
            throw new \strikeplagiarism\api\exception('Unexpected API result.');
        }
    }

}