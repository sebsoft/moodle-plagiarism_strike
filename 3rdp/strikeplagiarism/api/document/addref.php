<?php

/**
 * Document ADDREF implementation for Strike Plagiarism
 *
 * File         addref.php
 * Encoding     UTF-8
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace strikeplagiarism\api\document;

/**
 * strikeplagiarism\api\document\addref
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class addref extends \strikeplagiarism\api {

    /**
     * Endpoint for this API
     * @var string
     */
    protected $endpoint = 'documents';
    /**
     * Request method
     * @var string
     */
    protected $requesttype = self::REQUEST_TYPE_POST;
    /**
     * API action
     * @var string
     */
    protected $action = 'add-to-database';

    /**
     * Document ID as returned by Strike Antiplagiarism.
     *
     * @var string
     */
    protected $documentid;

    protected $forceurlencodedpost = true;

    public function get_documentid() {
        return $this->documentid;
    }

    public function set_documentid($documentid) {
        $this->documentid = $documentid;
        return $this;
    }

    protected function gather_postdata() {
        $this->clear_postdata();

        $this->add_postdata('APIKEY', $this->apitoken);
        if (!empty($this->documentid)) {
            $this->add_postdata('id', $this->documentid);
        }

        return parent::gather_postdata();
    }

    protected function process_result($data) {
        return parent::process_result($data);
    }

    protected function validate_result($result) {
        return parent::validate_result($result);
        // CUSTOM, because the response is expected to be empty
    }

}