<?php

namespace strikeplagiarism\api\document;

/**
 * strikeplagiarism\api\document\remove-from-database
 *
 * @copyright   INTERSIEC.com.pl / Strikeplagiarism.com
 * @author      Kamil Łuczak <kluczak@intersiec.com.pl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class removefromdatabase extends \strikeplagiarism\api {

    /**
     * Endpoint for this API
     * @var string
     */
    protected $endpoint = 'documents';
    /**
     * Request method
     * @var string
     */
    protected $requesttype = self::REQUEST_TYPE_POST;
    /**
     * API action
     * @var string
     */
    protected $action = 'remove-from-database';

    /**
     * Document ID as returned by Strike Antiplagiarism.
     *
     * @var string
     */
    protected $documentid;

    protected $forceurlencodedpost = true;

    public function set_documentid($documentid) {
        $this->documentid = $documentid;
        return $this;
    }

    protected function gather_postdata() {
        $this->clear_postdata();

        $this->add_postdata('APIKEY', $this->apitoken);
        if (!empty($this->documentid)) {
            $this->add_postdata('id', $this->documentid);
        }

        return parent::gather_postdata();
    }

    protected function process_result($data) {
        return parent::process_result($data);
    }

    protected function validate_result($result) {
        if (is_scalar($result)) {
            return true;
        } else {
            if (isset($result['message'])) {
                throw new \strikeplagiarism\api\exception($result['message']);
            }
        }
    }

}