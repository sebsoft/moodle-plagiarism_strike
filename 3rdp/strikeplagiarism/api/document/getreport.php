<?php

/**
 * Report GET implementation for Strike Plagiarism
 *
 * File         getreport.php
 * Encoding     UTF-8
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace strikeplagiarism\api\document;

/**
 * strikeplagiarism\api\document\getreport
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class getreport extends \strikeplagiarism\api {

    /**
     * Endpoint for this API
     * @var string
     */
    protected $endpoint = 'documents';
    /**
     * Request method
     * @var string
     */
    protected $requesttype = self::REQUEST_TYPE_GET;
    /**
     * API action
     * @var string
     */
    protected $action = 'report';

    /**
     * Document ID as returned by Strike Antiplagiarism.
     *
     * @var string
     */
    protected $documentid;
    /**
     * Document ID as returned by Strike Antiplagiarism.
     *
     * @var string
     */
    protected $documentchecksum;

    public function get_documentid() {
        return $this->documentid;
    }

    public function set_documentid($documentid) {
        $this->documentid = $documentid;
        return $this;
    }

    public function get_documentchecksum() {
        return $this->documentchecksum;
    }

    public function set_documentchecksum($documentchecksum) {
        $this->documentchecksum = $documentchecksum;
        return $this;
    }

    protected function gather_postdata() {
        $this->clear_postdata();

        $this->add_postdata('APIKEY', $this->apitoken);
        if (!empty($this->documentid)) {
            $this->add_postdata('id', $this->documentid);
        }
        if (!empty($this->documentchecksum)) {
            $this->add_postdata('md5sum', $this->documentchecksum);
        }

        return parent::gather_postdata();
    }

    protected function process_result($data) {
        return parent::process_result($data);
    }

    protected function validate_result($result) {
        if (is_scalar($result)) {
            return true;
        } else {
            if (isset($result['message'])) {
                throw new \strikeplagiarism\api\exception($result['message']);
            }
        }
    }

}