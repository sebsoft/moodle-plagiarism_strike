<?php

/**
 * Document ADD implementation for Strike Plagiarism
 *
 * File         add.php
 * Encoding     UTF-8
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace strikeplagiarism\api\document;

/**
 * strikeplagiarism\api\document\add
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class add extends \strikeplagiarism\api {

    /**
     * Endpoint for this API
     * @var string
     */
    protected $endpoint = 'documents';
    /**
     * Request method
     * @var string
     */
    protected $requesttype = self::REQUEST_TYPE_POST;
    /**
     * API action
     * @var string
     */
    protected $action = 'add';

    /**
     * Leading language of the document
     *
     * @var string
     */
    protected $documentlanguage;
    /**
     * Name of the faculty
     *
     * @var string
     */
    protected $documentfaculty;
    /**
     * Action to take on the document.
     *
     * - check (default) for antiplagiarism analysis.
     * - index for adding to the reference database only
     *
     * @var string
     */
    protected $documentaction;
    /**
     * Callback url
     *
     * @var string
     */
    protected $documentcallback;
    /**
     * Email address of the user (only ONLY if APIKEY is constrained to user; otherwise mandatory)
     *
     * @var string
     */
    protected $documentuseremail;
    /**
     * Document identifier
     *
     * @var string
     */
    protected $documentid;
    /**
     * Title of the document
     *
     * @var string
     */
    protected $documenttitle;
    /**
     * Author or co-author of the document
     *
     * @var string
     */
    protected $documentauthor;
    /**
     * Name of the promotor
     *
     * @var string
     */
    protected $documentcoordinator;
    /**
     * Name of the reviewer
     *
     * @var string
     */
    protected $documentreviewer;
    /**
     * Binary content of the document
     *
     * @var string
     */
    protected $documentfile;

    public function get_documentlanguage() {
        return $this->documentlanguage;
    }

    public function get_documentfaculty() {
        return $this->documentfaculty;
    }

    public function get_documentaction() {
        return $this->documentaction;
    }

    public function get_documentcallback() {
        return $this->documentcallback;
    }

    public function get_documentuseremail() {
        return $this->documentuseremail;
    }

    public function get_documentid() {
        return $this->documentid;
    }

    public function get_documenttitle() {
        return $this->documenttitle;
    }

    public function get_documentauthor() {
        return $this->documentauthor;
    }

    public function get_documentcoordinator() {
        return $this->documentcoordinator;
    }

    public function get_documentreviewer() {
        return $this->documentreviewer;
    }

    public function get_documentfile() {
        return $this->documentfile;
    }

    public function set_documentlanguage($documentlanguage) {
        $this->documentlanguage = $documentlanguage;
        return $this;
    }

    public function set_documentfaculty($documentfaculty) {
        $this->documentfaculty = $documentfaculty;
        return $this;
    }

    public function set_documentaction($documentaction) {
        $this->documentaction = $documentaction;
        return $this;
    }

    public function set_documentcallback($documentcallback) {
        $this->documentcallback = $documentcallback;
        return $this;
    }

    public function set_documentuseremail($documentuseremail) {
        $this->documentuseremail = $documentuseremail;
        return $this;
    }

    public function set_documentid($documentid) {
        $this->documentid = $documentid;
        return $this;
    }

    public function set_documenttitle($documenttitle) {
        $this->documenttitle = $documenttitle;
        return $this;
    }

    public function set_documentauthor($documentauthor) {
        $this->documentauthor = $documentauthor;
        return $this;
    }

    public function set_documentcoordinator($documentcoordinator) {
        $this->documentcoordinator = $documentcoordinator;
        return $this;
    }

    public function set_documentreviewer($documentreviewer) {
        $this->documentreviewer = $documentreviewer;
        return $this;
    }

    public function set_documentfile($documentfile) {
        $this->documentfile = $documentfile;
        return $this;
    }

    protected function gather_postdata() {
        $this->clear_postdata();

        $this->add_postdata('APIKEY', $this->apitoken);
        if (!empty($this->documentlanguage)) {
            $this->add_postdata('languageCode', $this->documentlanguage);
        }
        if (!empty($this->documentfaculty)) {
            $this->add_postdata('faculty', $this->documentfaculty);
        }
        if (!empty($this->documentaction)) {
            $this->add_postdata('action', $this->documentaction);
        }
        if (!empty($this->documentcallback)) {
            $this->add_postdata('callback', $this->documentcallback);
        }
        if (!empty($this->documentuseremail)) {
            $this->add_postdata('userEmail', $this->documentuseremail);
        }
        if (!empty($this->documentid)) {
            $this->add_postdata('id', $this->documentid);
        }
        if (!empty($this->documenttitle)) {
            $this->add_postdata('title', $this->documenttitle);
        }
        if (!empty($this->documentauthor)) {
            $this->add_postdata('author', $this->documentauthor);
        }
        if (!empty($this->documentcoordinator)) {
            $this->add_postdata('coordinator', $this->documentcoordinator);
        }
        if (!empty($this->documentreviewer)) {
            $this->add_postdata('reviewer', $this->documentreviewer);
        }
        if (!empty($this->documentfile)) {
            $curlfile = new \CURLFile($this->documentfile, 'application/octet-stream', basename($this->documentfile));
            $this->add_postdata('file', $curlfile);
        }

        // Validate mandatory elements.
        $musthave = ['APIKEY', 'languageCode', 'title', 'author', 'coordinator', 'file'];
        $this->require_postfields($musthave);

        return parent::gather_postdata();
    }

    protected function process_result($data) {
        return parent::process_result($data);
    }

    protected function validate_result($result) {
        if (is_scalar($result)) {
            return true;
        }
        if (isset($result['status']) && $result['status'] === 'new') {
            return true;
        } else if (isset($result['status']) && $result['status'] === 'error') {
            throw new \strikeplagiarism\api\exception($result['message']);
        } else {
            throw new \strikeplagiarism\api\exception('Unexpected api result');
        }
    }


}