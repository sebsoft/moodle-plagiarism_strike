<?php

/**
 * Parse error exception for Strike Plagiarism
 *
 * File         httpstatuserror.php
 * Encoding     UTF-8
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace strikeplagiarism\api\exception;

/**
 * strikeplagiarism\api\exception\httpstatuserror
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class httpstatuserror extends \strikeplagiarism\api\exception {

    protected $httpstatuscode;

    public function get_httpstatuscode() {
        return $this->httpstatuscode;
    }

    public function __construct($message, $httpstatuscode, $previous = null) {
        $this->httpstatuscode = $httpstatuscode;
        parent::__construct($message, 999, $previous);
    }

}