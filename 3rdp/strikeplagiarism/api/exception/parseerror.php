<?php

/**
 * Parse error exception for Strike Plagiarism
 *
 * File         parseerror.php
 * Encoding     UTF-8
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace strikeplagiarism\api\exception;

/**
 * strikeplagiarism\api\exception\parseerror
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class parseerror extends \strikeplagiarism\api\exception {
    /**
     * @var string
     */
    public $result;

    public function __construct($message, $result, $previous = null) {
        $this->result = $result;
        parent::__construct($message, 999, $previous);
    }

}