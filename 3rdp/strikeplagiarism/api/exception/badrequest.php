<?php

/**
 * Bad request exception for Strike Plagiarism
 *
 * File         badrequest.php
 * Encoding     UTF-8
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace strikeplagiarism\api\exception;

/**
 * strikeplagiarism\api\exception\badrequest
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class badrequest extends \strikeplagiarism\api\exception {

    public function __construct($message, $previous = null) {
        parent::__construct($message, 400, $previous);
    }

}