<?php

/**
 * API Key expired exception for Strike Plagiarism
 *
 * File         apikeyexpired.php
 * Encoding     UTF-8
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace strikeplagiarism\api\exception;

/**
 * strikeplagiarism\api\exception\apikeyexpired
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class apikeyexpired extends \strikeplagiarism\api\exception {

    public function __construct($message, $previous = null) {
        parent::__construct($message, 403, $previous);
    }

}