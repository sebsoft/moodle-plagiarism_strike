<?php

/**
 * Server error exception for Strike Plagiarism
 *
 * File         servererror.php
 * Encoding     UTF-8
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace strikeplagiarism\api\exception;

/**
 * strikeplagiarism\api\exception\servererror
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class servererror extends \strikeplagiarism\api\exception {

    public function __construct($message, $previous = null) {
        parent::__construct($message, 500, $previous);
    }

}