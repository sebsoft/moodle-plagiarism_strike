Strikeplagiarism.com is a commercial plagiarism detection system which requires a paid subscription to use.
This Plugin integrates with the existing Moodle Assignment module, Forum and workshop activities in Moodle
and allows the user-submitted content to be checked for Plagiarism.

Supported file formats - PDF, DOC, DOCX, RTF, ODT, TXT, HTML.

To use this plugin you must purchase a subscription from www.strikeplagiarism.com