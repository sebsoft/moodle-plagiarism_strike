<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plagiarism API connection TEST.
 *
 * File         test_connection.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   INTERSIEC.com.pl / Strikeplagiarism.com
 * @author      Kamil Łuczak <kluczak@intersiec.com.pl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__.'/../../config.php');
require_once($CFG->dirroot . '/plagiarism/strike/classes/strikeapitest.php');
require_once($CFG->dirroot . '/plagiarism/strike/lib.php');

require_login();

$strreport = 'Connection test';
$pageurl = new moodle_url($CFG->wwwroot . '/plagiarism/strike/test_connection.php');

$striketest = new \plagiarism_strike\strikeapitest();

$key = optional_param('key', null, PARAM_RAW);
$server = optional_param('server', null, PARAM_RAW);
$connection = $striketest->test_connection($key, $server);

header('Content-Type: application/json');
echo json_encode($connection);

