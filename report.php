<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Script to display report to end user.
 *
 * File         report.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__.'/../../config.php');
require_once($CFG->dirroot.'/plagiarism/strike/lib.php');

require_login();

$fid = required_param('pid', PARAM_INT);
$cmid = required_param('cm', PARAM_INT);
$params = array('pid' => $fid, 'cm' => $cmid);
$pageurl = new moodle_url($CFG->wwwroot . '/plagiarism/strike/report.php', $params);

$sf = plagiarism_strike\strikefile::get_by_id($fid);
$sr = \plagiarism_strike\strikereport::get_by_guid($sf->guid);
list($course, $cm) = get_course_and_cm_from_cmid($sf->cm);
$context = context_course::instance($course->id);

$PAGE->set_url($pageurl);
$PAGE->set_context($context);

if ($USER->id !== $sf->userid) {
    require_capability('plagiarism/strike:viewreport', $context);
} else {
    // Check if student can see this report.
    $config = new \plagiarism_strike\config($sf->cm);
    if ($config->get('strike_show_student_report') == PLAGIARISM_STRIKE_SHOW_NEVER) {
        print_error('nopermission');
    }
}

echo $sr->report;
exit;