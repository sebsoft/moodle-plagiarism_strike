<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library functions for plagiarism_strike
 *
 * File         lib.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); // It must be included from a Moodle page.
}

global $CFG;
require_once($CFG->dirroot . '/plagiarism/lib.php');

define('PLAGIARISM_STRIKE_DRAFTSUBMIT_IMMEDIATE', 0);
define('PLAGIARISM_STRIKE_DRAFTSUBMIT_FINAL', 1);

define('PLAGIARISM_STRIKE_ADDREF_AUTO', 'auto');
define('PLAGIARISM_STRIKE_ADDREF_MANUAL', 'manual');

define('PLAGIARISM_STRIKE_DELETE_REPORT_KEEP', 'keep');
define('PLAGIARISM_STRIKE_DELETE_REPORT_REMOVE', 'remove');

define('PLAGIARISM_STRIKE_SEND_CHOOSE', 0);
define('PLAGIARISM_STRIKE_SEND_ALL', 1);

define('PLAGIARISM_STRIKE_SHOW_NEVER', 0);
define('PLAGIARISM_STRIKE_SHOW_ALWAYS', 1);
define('PLAGIARISM_STRIKE_SHOW_CLOSED', 2);

// Used by content type restriction form - inline-text vs file attachments.
define('PLAGIARISM_STRIKE_RESTRICTCONTENTNO', 0);
define('PLAGIARISM_STRIKE_RESTRICTCONTENTFILES', 1);
define('PLAGIARISM_STRIKE_RESTRICTCONTENTTEXT', 2);

define('PLAGIARISM_STRIKE_MAXATTEMPTS', 10);

define('PLAGIARISM_STRIKE_STATUS_OK', 200);
define('PLAGIARISM_STRIKE_STATUS_ERRBADREQUEST', 400);
define('PLAGIARISM_STRIKE_STATUS_ERRAPIKEY', 403);
define('PLAGIARISM_STRIKE_STATUS_ERRSERVER', 500);
define('PLAGIARISM_STRIKE_STATUS_PENDING', 'pending');
define('PLAGIARISM_STRIKE_STATUS_AWAITCHOICE', 'awaitanalysischoice');
define('PLAGIARISM_STRIKE_STATUS_ACCEPTED', 'new');
define('PLAGIARISM_STRIKE_STATUS_ERROR', 'error');
define('PLAGIARISM_STRIKE_STATUS_TOOLARGE', 'toolarge');
define('PLAGIARISM_STRIKE_STATUS_UNSUPPORTED', 'unsupportedfiletype');
define('PLAGIARISM_STRIKE_STATUS_INVALIDRESPONSE', 'invalidresponse');
define('PLAGIARISM_STRIKE_STATUS_COMPLETE', 'complete');
define('PLAGIARISM_STRIKE_STATUS_TIMEOUT', 'timeout');

define('PLAGIARISM_STRIKE_DEFAULT_MIN_WORDCOUNT', 50);

/**
 * dummy function forcing lib to be loaded
 * @param global_navigation $nav
 */
function plagiarism_strike_extend_navigation(\global_navigation $nav) {
    // No-op.
    return;
}


// Redirect user to plagiarism settings.
$systemcontext = context_system::instance();
if (isloggedin() and !isguestuser() and has_capability('moodle/site:config', $systemcontext)) {
    global $DB, $SESSION;
    if ($DB->record_exists('config_plugins', array('name' => 'strike_after_install', 'plugin' => 'plagiarism'))) {
        $afterinstall = $DB->get_record('config_plugins', array('name' => 'strike_after_install', 'plugin' => 'plagiarism'));
        if (!$afterinstall or !isset($afterinstall->value) or $afterinstall->value == 0 ) {
            $afterinstall->value = 1;
            $DB->update_record('config_plugins', $afterinstall);

            $url = new moodle_url('/plagiarism/strike/settings.php');
            $SESSION->wantsurl = qualified_me();
            redirect($url);
        }
    }
}

/**
 * plagiarism_strike main plugin class
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class plagiarism_plugin_strike extends plagiarism_plugin {

    /**
     * hook to allow plagiarism specific information to be displayed beside a submission
     * @param array  $linkarray contains all relevant information for the plugin to generate a link
     * @return string
     */
    public function get_links($linkarray) {
        global $COURSE, $CFG, $PAGE, $DB;

        $cmid = $linkarray['cmid'];
        $userid = $linkarray['userid'];

        $config = new \plagiarism_strike\config($cmid);
        $renderer = $PAGE->get_renderer('plagiarism_strike');

        $wordcount = \plagiarism_strike\configplagiarism::get('strike_wordcount');
        if (empty($wordcount)) {
            // Set a sensible default if we can't find one.
            $wordcount = PLAGIARISM_STRIKE_DEFAULT_MIN_WORDCOUNT;
        }
        $showcontent = true;
        $showfiles = true;
        $restrictcontent = $config->get('strike_restrictcontent');
        if (!empty($restrictcontent)) {
            if ($restrictcontent == PLAGIARISM_STRIKE_RESTRICTCONTENTFILES) {
                $showcontent = false;
            } else if ($restrictcontent == PLAGIARISM_STRIKE_RESTRICTCONTENTTEXT) {
                $showfiles = false;
            }
        }

        $cm = get_coursemodule_from_id('', $cmid);

        $realuserid = $userid;
        if (!empty($linkarray['content']) && $showcontent && str_word_count($linkarray['content']) > $wordcount) {
            $content = $linkarray['content'];
            if ($cm->modname == 'assign' && isset($linkarray['assignment'])) {
                // Get REAL submission text.
                // The 'content' is unreliable with online text submissions in mod_assign.
                $content = $this->get_online_submission_text($userid, $linkarray['assignment']);
                $linkarray['content_real'] = $content;
            }

            $filename = "content-" . $COURSE->id . "-" . $cmid . "-". $userid . ".htm";
            $filepath = $CFG->tempdir."/strike/" . $filename;
            $file = new stdclass();
            $file->type = "tempstrike";
            $file->filename = $filename;
            $file->timestamp = time();
            $file->contenthash = sha1(plagiarism_strike_format_temp_content_text($content, true));
            $file->altcontenthash = sha1(plagiarism_strike_format_temp_content_text($content, false));
            $file->oldcontenthash = sha1($content);
            $file->filepath = $filepath;
            $file->realuserid = $userid;
        } else if (!empty($linkarray['file']) && $showfiles) {
            $file = new stdclass();
            $file->filename = $linkarray['file']->get_filename();
            $file->timestamp = time();
            $file->contenthash = $linkarray['file']->get_contenthash();
            $file->filepath = $linkarray['file']->get_filepath();
            // Check file-user vs user.
            switch ($cm->modname) {
                case 'assign':
                    $submissionitem = $DB->get_record('assign_submission', ['id' => $linkarray['file']->get_itemid()]);
                    $realuserid = $submissionitem->userid;
                    break;
                case 'workshop':
                    $submissionitem = $DB->get_record('workshop_submissions', ['id' => $linkarray['file']->get_itemid()]);
                    $realuserid = $submissionitem->authorid;
                    break;
                case 'forum':
                    $submissionitem = $DB->get_record('forum_posts', ['id' => $linkarray['file']->get_itemid()]);
                    $realuserid = $submissionitem->userid;
                    break;
                case 'coursework':
                    throw new \Exception(__NETHOD__ . ' __not_implemented__ for ' . $cm->modname . '');
                    break;
            }
        } else {
            return '';
        }

        $file->realuserid = $realuserid;
        $results = $this->get_file_results($cmid, $userid, $file);
        if (empty($results)) {
            // Info about this file is not available to this user.
            return '';
        }

        $modulecontext = context_module::instance($cmid);
        $output = '';
        if ($results['statuscode'] == PLAGIARISM_STRIKE_STATUS_AWAITCHOICE &&
                (bool)$results['canenableanalysis'] && (bool)$results['doanalysis'] == false) {
            // Awaiting decision to send for analysis.
            $url = new \moodle_url($CFG->wwwroot . '/plagiarism/strike/enableanalysis.php',
                    array('sesskey' => sesskey(), 'fid' => $results['pid'], 'doanalysis' => 1));
            $output .= $this->wrap_span('<a href="'.$url->out().'">'.
                    plagiarism_strike\strikeplagiarism::get_image('warning', 'sendforanalysis').
                    ' '.get_string('sendforanalysis', 'plagiarism_strike').'</a>');
            return $output;
        }
        if ($results['statuscode'] == PLAGIARISM_STRIKE_STATUS_PENDING) {
            $output .= $this->wrap_span(plagiarism_strike\strikeplagiarism::get_image('processing', 'pending'));
            return $output;
        }

        if ($results['statuscode'] == PLAGIARISM_STRIKE_STATUS_COMPLETE) {
            // Normal situation - STRIKE has successfully analyzed the file.
            $ranking1 = new \plagiarism_strike\renderable\ranking(round(100 * $results['factor1'], 1));
            $ranking2 = new \plagiarism_strike\renderable\ranking(round(100 * $results['factor2'], 1));
            $ranking = '';
            if ($results['viewscore'] && !empty($results['factor1']) || !empty($results['factor2'])) {
                // User is allowed to view only the score.
                $ranking .= get_string('similarity1', 'plagiarism_strike') . ': ';
                $ranking .= $renderer->render($ranking1) . '<br/>';
                $ranking .= get_string('similarity2', 'plagiarism_strike') . ': ';
                $ranking .= $renderer->render($ranking2);
            }
            if ($results['viewreport'] && !empty($results['reporturl'])) {
                // User is allowed to view the report.
                // Score is contained in report, so they can see the score too.
                $ranking .= '<a href="'.$results['reporturl'].'" target="_blank">';
                $ranking .= plagiarism_strike\strikeplagiarism::get_image('report', 'report');
                $ranking .= '</a>';
            }
            if (!empty($results['renamed'])) {
                $ranking .= $results['renamed'];
            }
            $output .= $this->wrap_span($ranking);
        } else if ($results['statuscode'] == PLAGIARISM_STRIKE_STATUS_ACCEPTED) {
            $output .= $this->wrap_span(plagiarism_strike\strikeplagiarism::get_image('processing', 'processing'));
        } else if ($results['statuscode'] == PLAGIARISM_STRIKE_STATUS_UNSUPPORTED) {
            $output .= $this->wrap_span(plagiarism_strike\strikeplagiarism::get_image('warning', 'unsupportedfiletype'));
        } else if ($results['statuscode'] == PLAGIARISM_STRIKE_STATUS_TOOLARGE) {
            $output .= $this->wrap_span(plagiarism_strike\strikeplagiarism::get_image('warning', 'toolarge'));
        } else if ($results['statuscode'] == PLAGIARISM_STRIKE_STATUS_AWAITCHOICE) {
            $output .= $this->wrap_span(plagiarism_strike\strikeplagiarism::get_image('pending', 'awaitanalysischoice'));
        } else {
            $title = get_string('unknownwarning', 'plagiarism_strike');
            $reset = '';
            if (has_capability('plagiarism/strike:resetfile', $modulecontext) &&
                !empty($results['error'])) { // This is a teacher viewing the responses.
                // Strip out some possible known text to tidy it up.
                $erroresponse = format_text($results['error'], FORMAT_PLAIN);
                $title .= ': ' . $erroresponse;
                $url = new moodle_url('/plagiarism/strike/reset.php', array('cmid' => $cmid, 'pf' => $results['pid'],
                                                                            'sesskey' => sesskey()));
                $reset = "<a href='$url'>".get_string('reset')."</a>";
            }
            $reset = plagiarism_strike\strikeplagiarism::get_image('warning', 'unknownwarning', $title) . $reset;
            $output .= $this->wrap_span($reset);
        }
        return $output;
    }

    /**
     * Wrap a report span.
     * @param string $content
     * @return string
     */
    protected function wrap_span($content) {
        return '<span class="strikeplagiarismreport">' . $content . '</span>';
    }

    /**
     * Get the current online submission text for the given user and assignment.
     * @param int $userid
     * @param int $assignmentid
     * @return string
     */
    protected function get_online_submission_text($userid, $assignmentid) {
        global $DB;
        // Get latest text content submitted as we do not have submission id.
        $submissions = $DB->get_records_select('assign_submission', ' userid = ? AND assignment = ? ',
                                        array($userid, $assignmentid), 'id DESC', 'id', 0, 1);
        $submission = end($submissions);
        $moodletextsubmission = $DB->get_record('assignsubmission_onlinetext',
                                            array('submission' => $submission->id), 'onlinetext, onlineformat');
        if (isset($moodletextsubmission->onlinetext)) {
            return $moodletextsubmission->onlinetext;
        } else {
            return '';
        }
    }

    /**
     * hook to save plagiarism specific settings on a module settings page
     * @param \stdClass $data - data from an mform submission.
     */
    public function save_form_elements($data) {
        // Is strike enabled?
        if ((bool) \plagiarism_strike\configplagiarism::get('strike_use') === false) {
            return;
        }

        // Save.
        if (isset($data->strike_use_strike)) {
            if (empty($data->submissiondrafts)) {
                // Make sure draft_submit is not set if submissiondrafts not used.
                $data->strike_draft_submit = 0;
            }
            // First get existing values.
            if (empty($data->coursemodule)) {
                debugging("STRIKE settings failure - no coursemodule set in form data, STRIKE could not be enabled.");
                return;
            }

            // Array of possible plagiarism config options.
            $plagiarismelements = $this->config_options();
            $strikeconfig = new \plagiarism_strike\config($data->coursemodule);

            foreach ($plagiarismelements as $element => $unused) {
                if (isset($data->$element) && is_array($data->$element)) {
                    $value = implode(',', $data->$element);
                } else {
                    $value = (isset($data->$element) ? $data->$element : 0);
                }
                $strikeconfig->set($element, $value, true);
            }
        }
    }

    /**
     * Check if plugin has been configured with Turnitin account details.
     * @return boolean whether the plugin is configured for Turnitin.
     **/
    public function is_plugin_configured() {
        $config = strike_admin_config();
        if (empty($config->accountid) || empty($config->apiurl) || empty($config->secretkey)) {
            return false;
        }

        return true;
    }

    /**
     * Check if plugin has been configured with Turnitin account details.
     * @return boolean whether the plugin is configured for Turnitin.
     **/
    public static function plugin_config() {
        $config = \plagiarism_strike\configplagiarism::get_config();

        return $config;
    }


    /**
     * hook to add plagiarism specific settings to a module settings page
     * @param \moodleform $mform Moodle form
     * @param \context $context current context
     * @param string $modulename module name
     */
    public function get_form_elements_module($mform, $context, $modulename = "") {
        if ((bool) \plagiarism_strike\configplagiarism::get('strike_use') === false) {
            return;
        }
        if (!empty($modulename)) {
            $modname = 'strike_enable_' . $modulename;
            if ((bool) \plagiarism_strike\configplagiarism::get($modname) === false) {
                return;
            }
        }

        $cmid = optional_param('update', 0, PARAM_INT); // Get cm as $this->_cm is not available here.
        $config = new plagiarism_strike\config($cmid);
        $plagiarismelements = $this->config_options();

        // Check if we have the rights to adjust on this level.
        if (!has_capability('plagiarism/strike:enable', $context)) {
            // Use all default settings.
            foreach ($plagiarismelements as $element => $paramtype) {
                $mform->addElement('hidden', $element);
                $mform->setType($element, $paramtype);
            }
        } else {
            // Add elements.
            strike_get_form_elements($mform, $cmid);

            // Set some "disabledIf" configs.
            strike_form_element_disable_rules($mform);
        }

        // Now set defaults.
        foreach ($plagiarismelements as $element => $paramtype) {
            $configvalue = $config->get($element);
            if ($configvalue !== null) {
                $mform->setDefault($element, $configvalue);
            }
        }

        // Now handle content restriction settings.
        if ($modulename == 'mod_assign' && $mform->elementExists("submissionplugins")) {
            // I can't see a way to check if a particular checkbox exists
            // elementExists on the checkbox name doesn't work.
            $mform->disabledIf('strike_restrictcontent', 'assignsubmission_onlinetext_enabled');
            $mform->setAdvanced('strike_restrictcontent');
        } else if ($modulename != 'mod_forum') {
            // Forum doesn't need any changes but all other modules should disable this.
            $mform->setDefault('strike_restrictcontent', 0);
            $mform->hardFreeze('strike_restrictcontent');
            $mform->setAdvanced('strike_restrictcontent');
        }
    }

    /**
     * hook to allow a disclosure to be printed notifying users what will happen with their submission
     * @param int $cmid - course module id
     * @return string
     */
    public function print_disclosure($cmid) {
        global $OUTPUT;
        if (!plagiarism_strike\strikeplagiarism::use_strike($cmid)) {
            return '';
        }
        // Display disclosure.
        $disclosure = get_string('strikestudentdisclosure', 'plagiarism_strike');
        $formatoptions = new stdClass;
        $formatoptions->noclean = true;
        $out = $OUTPUT->box_start('generalbox boxaligncenter', 'intro');
        $out .= format_text($disclosure, FORMAT_MOODLE, $formatoptions);
        $out .= $OUTPUT->box_end();
        return $out;
    }

    /**
     * hook to allow status of submitted files to be updated - called on grading/report pages.
     * This plugin doesn't use it as it would impact negatively on the page loading.
     *
     * @param object $course - full Course object
     * @param object $cm - full cm object
     */
    public function update_status($course, $cm) {
        // Called at top of submissions/grading pages - allows printing of admin style links or updating status.
        return '';
    }

    /**
     * called by admin/cron.php
     * @deprecated since version 3.1
     */
    public function cron() {
        // Do nothing. Deprecated method.
    }

    /**
     * Generic event handler for STRIKE plagiarism.
     * @param array $eventdata
     * @return boolean
     */
    public function event_handler($eventdata) {
        global $DB, $CFG;
        // Remove the event if the course module no longer exists.
        if (!$cm = get_coursemodule_from_id($eventdata['other']['modulename'], $eventdata['contextinstanceid'])) {
            return true;
        }
        // If STRIKE isn't enabled, return.
        if ((bool) \plagiarism_strike\configplagiarism::get('strike_use') === false) {
            return true;
        }
        // Load settings.
        $config = new \plagiarism_strike\config($cm->id);
        // If STRIKE isn't use on this CM, return.
        if ((bool) $config->get('strike_use_strike') === false) {
            return true;
        }

        $modulerecord = $DB->get_record($cm->modname, array('id' => $cm->instance));
        if ($cm->modname != 'assign') {
            $modulerecord->submissiondrafts = 0;
        }

        $draftsubmit = $config->get('strike_draft_submit');

        // If draft submissions are turned on then only send to STRIKE if the draft submit setting is set.
        if ($modulerecord->submissiondrafts && (bool)$draftsubmit &&
                ($eventdata['eventtype'] == 'file_uploaded' || $eventdata['eventtype'] == 'content_uploaded')) {
            return true;
        }

        // Check to see if restrictcontent is in use.
        $showcontent = true;
        $showfiles = true;
        $restrictcontent = $config->get('strike_restrictcontent');
        if (!empty($restrictcontent)) {
            if ($restrictcontent == PLAGIARISM_STRIKE_RESTRICTCONTENTFILES) {
                $showcontent = false;
            } else if ($restrictcontent == PLAGIARISM_STRIKE_RESTRICTCONTENTTEXT) {
                $showfiles = false;
            }
        }

        $wordcount = \plagiarism_strike\configplagiarism::get('strike_wordcount');
        if (empty($wordcount)) {
            // Set a sensible default if we can't find one.
            $wordcount = PLAGIARISM_STRIKE_DEFAULT_MIN_WORDCOUNT;
        }

        $userid = $eventdata['userid'];
        $relateduserid = (!empty($eventdata['relateduserid']) ? $eventdata['relateduserid'] : null);
        $author = empty($relateduserid) ? $userid : $relateduserid;

        // Initialize removal.
        $strikeremoval = new \plagiarism_strike\strikeremoval($cm->id, $userid, $relateduserid);

        // Handle drafts.
        if ($eventdata['eventtype'] == 'assignsubmission_submitted' && empty($eventdata['other']['submission_editable'])) {
            // Assignment-specific functionality:
            // This is a 'finalize' event. No files from this event itself,
            // but need to check if files from previous events need to be submitted for processing.
            $result = true;
            if (!empty($draftsubmit) && $draftsubmit == PLAGIARISM_STRIKE_DRAFTSUBMIT_FINAL) {
                // Any files attached to previous events were not submitted.
                // These files are now finalized, and should be submitted for processing.
                require_once("$CFG->dirroot/mod/assign/locallib.php");
                require_once("$CFG->dirroot/mod/assign/submission/file/locallib.php");

                $modulecontext = context_module::instance($cm->id);

                if ($showfiles) { // If we should be handling files.
                    $fs = get_file_storage();
                    if ($files = $fs->get_area_files($modulecontext->id, 'assignsubmission_file',
                        ASSIGNSUBMISSION_FILE_FILEAREA, $eventdata['objectid'], "id", false)) {
                        foreach ($files as $file) {
                            // Queue file.
                            $queuedfile = \plagiarism_strike\strikeplagiarism::queue_file($cm->id, $userid, $file, $relateduserid);
                            // Set removal.
                            if (!empty($queuedfile)) {
                                $strikeremoval->add_no_delete_hash($queuedfile->contenthash, 1);
                            }
                        }
                    }
                }

                if ($showcontent) { // If we should be handling in-line text.
                    $submission = $DB->get_record('assignsubmission_onlinetext', array('submission' => $eventdata['objectid']));
                    if (!empty($submission) && str_word_count($submission->onlinetext) > $wordcount) {
                        $content = trim(format_text($submission->onlinetext, $submission->onlineformat,
                            array('context' => $modulecontext)));
                        $file = strike_create_temp_file($cm->id, $eventdata['courseid'], $userid, $content);
                        // Queue file.
                        $queuedfile = \plagiarism_strike\strikeplagiarism::queue_file($cm->id, $userid, $file, $relateduserid);
                        // Set removal.
                        if (!empty($queuedfile)) {
                            $strikeremoval->add_no_delete_hash($queuedfile->contenthash, 0);
                        }
                    }
                }
                // Cleanup.
                $strikeremoval->cleanup();
            }
            return $result;
        }

        if ($draftsubmit == PLAGIARISM_STRIKE_DRAFTSUBMIT_FINAL) {
            // Assignment-specific functionality:
            // Files should only be sent for checking once "finalized".
            return true;
        }

        // Text is attached.
        $result = true;
        if (!empty($eventdata['other']['content']) && $showcontent && str_word_count($eventdata['other']['content']) > $wordcount) {
            $file = strike_create_temp_file($cm->id, $eventdata['courseid'], $userid, $eventdata['other']['content']);
            // Queue file.
            $queuedfile = \plagiarism_strike\strikeplagiarism::queue_file($cm->id, $userid, $file, $relateduserid);
            // Set removal.
            if (!empty($queuedfile)) {
                $strikeremoval->add_no_delete_hash($queuedfile->contenthash, 0);
            }
        }

        // Normal situation: 1 or more assessable files attached to event, ready to be checked.
        if (!empty($eventdata['other']['pathnamehashes']) && $showfiles) {
            foreach ($eventdata['other']['pathnamehashes'] as $hash) {
                $fs = get_file_storage();
                $efile = $fs->get_file_by_hash($hash);

                if (empty($efile)) {
                    mtrace("nofilefound!");
                    continue;
                } else if ($efile->get_filename() === '.') {
                    // This 'file' is actually a directory - nothing to submit.
                    continue;
                }

                // Queue file.
                $queuedfile = \plagiarism_strike\strikeplagiarism::queue_file($cm->id, $userid, $efile, $relateduserid);
                // Set removal.
                if (!empty($queuedfile)) {
                    $strikeremoval->add_no_delete_hash($queuedfile->contenthash, 1);
                }
            }
        }

        // Handled events. Now kick removal processing.
        $strikeremoval->cleanup();

        return $result;
    }

    /**
     * Function which returns an array of all the module instance settings.
     *
     * @return array
     *
     */
    public function config_options() {
        return array(
            'strike_use_strike' => PARAM_INT,
            'strike_allowallfile' => PARAM_INT,
            'strike_selectfiletypes' => PARAM_RAW,
            'strike_show_student_status' => PARAM_INT,
            'strike_show_student_score' => PARAM_INT,
            'strike_show_student_report' => PARAM_INT,
            'strike_addref_method' => PARAM_ALPHA,
            'strike_deletereport_method' => PARAM_ALPHA,
            'strike_studentemail' => PARAM_INT,
            'strike_draft_submit' => PARAM_INT,
            'strike_docsendmode' => PARAM_INT,
            'strike_restrictcontent' => PARAM_INT,
            'strike_useadminnotifications' => PARAM_INT,
            'strike_useteachernotifications' => PARAM_INT,
            'strike_notifyteachers' => PARAM_INT,
        );
    }

    /**
     * Get the plagiarism file results
     * @param int $cmid
     * @param int $userid
     * @param \stdClass $file
     * @return boolean|string
     */
    public function get_file_results($cmid, $userid, $file) {
        global $DB, $USER, $CFG;
        if ((bool) \plagiarism_strike\configplagiarism::get('strike_use') === false) {
            // STRIKE is not enabled.
            return false;
        }
        if (!plagiarism_strike\strikeplagiarism::use_strike($cmid)) {
            // STRIKE not enabled for this cm.
            return false;
        }

        // Collect detail about the specified coursemodule.
        $modulesql = 'SELECT m.id, m.name, cm.instance'.
                ' FROM {course_modules} cm' .
                ' INNER JOIN {modules} m on cm.module = m.id ' .
                'WHERE cm.id = ?';
        $moduledetail = $DB->get_record_sql($modulesql, array($cmid));
        if (!empty($moduledetail)) {
            $sql = "SELECT * FROM {{$moduledetail->name}} WHERE id= ?";
            $module = $DB->get_record_sql($sql, array($moduledetail->instance));
        }
        if (empty($module)) {
            // No such cmid.
            return false;
        }

        $modulecontext = context_module::instance($cmid);
        // If the user has permission to see result of all items in this course module.
        $viewstatus = $viewscore = $viewreport = has_capability('plagiarism/strike:viewreport', $modulecontext);

        // Determine if the activity is closed.
        // If report is closed, this can make the report available to more users.
        $assignclosed = false;
        $time = time();
        if (!empty($module->preventlate) && !empty($module->timedue)) {
            $assignclosed = ($module->timeavailable <= $time && $time <= $module->timedue);
        } else if (!empty($module->timeavailable)) {
            $assignclosed = ($module->timeavailable <= $time);
        }

        $config = new \plagiarism_strike\config($cmid);

        // Under certain circumstances, users are allowed to see plagiarism info
        // even if they don't have view report capability.
        if ($USER->id == $userid || // If this is a user viewing their own report, check if settings allow it.
            (!$viewscore)) { // If teamsubmisson is enabled or teacher submitted, the file may be from a different user.
            $selfreport = true;
            $configreport = $config->get('strike_show_student_report', true);
            $configscore = $config->get('strike_show_student_score', true);
            $configstatus = $config->get('strike_show_student_status', true);
            if (($configreport == PLAGIARISM_STRIKE_SHOW_ALWAYS ||
                     $configreport == PLAGIARISM_STRIKE_SHOW_CLOSED && $assignclosed)) {
                $viewreport = true;
            } else if ($configreport == PLAGIARISM_STRIKE_SHOW_NEVER) {
                $viewreport = false;
            }
            if ($configscore == PLAGIARISM_STRIKE_SHOW_ALWAYS ||
                    ($configscore == PLAGIARISM_STRIKE_SHOW_CLOSED && $assignclosed)) {
                $viewscore = true;
            } else if ($configscore == PLAGIARISM_STRIKE_SHOW_NEVER) {
                $viewscore = false;
            }
            if ($configstatus == PLAGIARISM_STRIKE_SHOW_ALWAYS ||
                    ($configstatus == PLAGIARISM_STRIKE_SHOW_CLOSED && $assignclosed)) {
                $viewstatus = true;
            } else if ($configstatus == PLAGIARISM_STRIKE_SHOW_NEVER) {
                $viewstatus = false;
            }
        } else {
            $selfreport = false;
        }
        // End of rights checking.
        if (!$viewscore && !$viewreport && !$viewstatus && !$selfreport) {
            // User is not permitted to see any details.
            return false;
        }
        if ($selfreport && (!$viewscore && !$viewreport && !$viewstatus)) {
            // Cunfigured so user can't see anything.
            return false;
        }

        $filehash = $file->contenthash;
        $extrasql = '';
        $usersql = '';
        if (isset($file->realuserid) && $userid != $file->realuserid) {
            $params = array($cmid, $userid, $file->realuserid, $filehash);
            $usersql = "userid = ? AND relateduserid = ?";
        } else {
            $params = array($cmid, $userid, $userid, $filehash);
            $usersql = "(userid = ? OR relateduserid = ?)";
        }
        if (!empty($file->oldcontenthash)) {
            $extrasql = ' OR contenthash = ?';
            $params[] = $file->oldcontenthash;
        }
        if (!empty($file->altcontenthash)) {
            $extrasql .= ' OR contenthash = ?';
            $params[] = $file->altcontenthash;
        }
        $sql = "SELECT * FROM {plagiarism_strike_files}
                        WHERE cm = ? AND {$usersql} AND " .
                        "(contenthash = ? {$extrasql})";

        $plagiarismfile = $DB->get_record_sql($sql, $params);
        // We already know this is buggy with online text submissions in mod_assign.
        // When this method is called the content is already modified for editor usage (see locallib.php of onlinetext submission).
        if (empty($plagiarismfile)) {
            // No record of that submitted file.
            return false;
        }

        // Returns after this point will include a result set describing information about
        // interactions with strike servers.
        $results = array(
                'statuscode' => $plagiarismfile->statuscode,
                'pid' => $plagiarismfile->id,
                'error' => '',
                'reporturl' => '',
                'score' => '',
                'renamed' => '',
                'analyzed' => 0,
                'indexed' => 0,
                'reportready' => 0,
                'factor1' => 0,
                'factor2' => 0,
                'factor3' => 0,
                'factor4' => 0,
                'factor5' => 0,
                'automatedanalysis' => 0,
                'doanalysis' => 0,
                'canenableanalysis' => 0,
            'viewscore' => $viewscore,
            'viewstatus' => $viewstatus,
            'viewreport' => $viewreport,
                );
        if ($plagiarismfile->statuscode == PLAGIARISM_STRIKE_STATUS_AWAITCHOICE) {
            $results['canenableanalysis'] = has_capability('plagiarism/strike:enable', $modulecontext) ? 1 : 0;
            return $results;
        }

        if ($plagiarismfile->statuscode == PLAGIARISM_STRIKE_STATUS_PENDING) {
            return $results;
        }

        // Now check for differing filename and display info related to it.
        $previouslysubmitted = '';
        if ($file->filename !== $plagiarismfile->filename) {
            $previouslysubmitted = '('.get_string('previouslysubmitted', 'plagiarism_strike').': '.$plagiarismfile->filename.')';
        }

        $results['error'] = $plagiarismfile->errorresponse;
        $results['reportready'] = $plagiarismfile->reportready;
        $results['indexed'] = $plagiarismfile->indexed;
        $results['automatedanalysis'] = $plagiarismfile->automatedanalysis;
        $results['doanalysis'] = $plagiarismfile->doanalysis;

        if ($plagiarismfile->reportready == 1) {
            $results['analyzed'] = 1;
            // File has been successfully analyzed - return all appropriate details.
            if ($viewscore) {
                // If user can see the report, they can see the score on the report
                // so make it directly available.
                $results['factor1'] = $plagiarismfile->factor1;
                $results['factor2'] = $plagiarismfile->factor2;
                $results['factor3'] = $plagiarismfile->factor3;
                $results['factor4'] = $plagiarismfile->factor4;
                $results['factor5'] = $plagiarismfile->factor5;
            }
            if ($viewreport) {
                $results['reporturl'] = \plagiarism_strike\strikeplagiarism::get_report_url($plagiarismfile);
            }
            $results['renamed'] = $previouslysubmitted;
        }
        return $results;
    }

}

/**
 * Adds the list of plagiarism settings to a form.
 *
 * @param \MoodleQuickForm $mform Moodle form object.
 * @param int $cmid course module id.
 * @param bool $isdefaults should be true if we're loading this from the strike-defaults script.
 */
function strike_get_form_elements($mform, $cmid, $isdefaults = false) {
    $ynoptions = array(
        0 => get_string('no'),
        1 => get_string('yes')
    );
    $tiioptions = array(
        PLAGIARISM_STRIKE_SHOW_NEVER => get_string('never'),
        PLAGIARISM_STRIKE_SHOW_ALWAYS => get_string('always'),
        PLAGIARISM_STRIKE_SHOW_CLOSED => get_string('showwhenclosed', 'plagiarism_strike')
    );
    $draftoptions = array(
        PLAGIARISM_STRIKE_DRAFTSUBMIT_IMMEDIATE => get_string('submitondraft', 'plagiarism_strike'),
        PLAGIARISM_STRIKE_DRAFTSUBMIT_FINAL => get_string('submitonfinal', 'plagiarism_strike')
    );
    $sendoptions = array(
        PLAGIARISM_STRIKE_SEND_CHOOSE => get_string('docsendingchoose', 'plagiarism_strike'),
        PLAGIARISM_STRIKE_SEND_ALL => get_string('docsendingall', 'plagiarism_strike')
    );
    $addrefoptions = array(
        PLAGIARISM_STRIKE_ADDREF_AUTO => get_string('addrefauto', 'plagiarism_strike'),
        PLAGIARISM_STRIKE_ADDREF_MANUAL => get_string('addrefmanual', 'plagiarism_strike')
    );
    $deletereportoptions = array(
        PLAGIARISM_STRIKE_DELETE_REPORT_KEEP => get_string('deletereportkeep', 'plagiarism_strike'),
        PLAGIARISM_STRIKE_DELETE_REPORT_REMOVE => get_string('deletereportremove', 'plagiarism_strike')
    );
    $filetypes = plagiarism_strike\strikeplagiarism::default_allowed_file_types(true);
    $supportedfiles = array();
    foreach ($filetypes as $ext => $mime) {
        $supportedfiles[$ext] = $ext;
    }

    // Load teachers for this CM.
    $teachers = [];
    if (!empty($cmid)) {
        $context = \context_module::instance($cmid);
        $coursecontext = $context->get_course_context();
        $capability = 'moodle/grade:edit';
        $xteachers = get_users_by_capability($coursecontext, $capability);
        $teachers = [];
        foreach ($xteachers as $teacher) {
            $teachers[$teacher->id] = fullname($teacher);
        }
    } else {
        // Somewhat nasty, but when we're adding a module to a course, we can check for an optional param.
        $courseid = optional_param('course', 0, PARAM_INT);
        if (!empty($courseid) && $courseid <> SITEID) {
            $coursecontext = \context_course::instance($courseid);
            $capability = 'moodle/grade:edit';
            $xteachers = get_users_by_capability($coursecontext, $capability);
            $teachers = [];
            foreach ($xteachers as $teacher) {
                $teachers[$teacher->id] = fullname($teacher);
            }
        }
    }

    $mform->addElement('header', 'plagiarismdesc', get_string('strike', 'plagiarism_strike'));

    $mform->addElement('select', 'strike_use_strike', get_string('usestrike', 'plagiarism_strike'), $ynoptions);

    // Use notifications?
    $mform->addElement('select', 'strike_useadminnotifications',
            get_string('useadminnotifications', 'plagiarism_strike'), $ynoptions);
    $mform->addHelpButton('strike_useadminnotifications', 'useadminnotifications', 'plagiarism_strike');

    // Notification teachers.
    $mform->addElement('select', 'strike_useteachernotifications',
            get_string('useteachernotifications', 'plagiarism_strike'), $ynoptions);
    $mform->addHelpButton('strike_useteachernotifications', 'useteachernotifications', 'plagiarism_strike');

    if (!empty($teachers)) {
        $select = $mform->addElement('select', 'strike_notifyteachers',
                get_string('notifyteachers', 'plagiarism_strike'), $teachers);
        $select->setMultiple(true);
    }

    if ($mform->elementExists('submissiondrafts')) {
        $mform->addElement('select', 'strike_draft_submit',
                get_string('strike_draft_submit', 'plagiarism_strike'), $draftoptions);
    }

    $atts = ['onchange' => 'javascript:_sp_check_ft(this)'];
    $mform->addElement('select', 'strike_allowallfile',
            get_string('allowallsupportedfiles', 'plagiarism_strike'), $ynoptions, $atts);
    $mform->addHelpButton('strike_allowallfile', 'allowallsupportedfiles', 'plagiarism_strike');

    // This is really not the cleanest solution.
    // BUT... Moodle has no plagiarism validation callback when editing/creating a mod instance.
    if (!$isdefaults) {
        $msg = '<span id="_strike_allowallfile" class="error" style="display: none">'.
                get_string('warn:strike_selectfiletypes-empty', 'plagiarism_strike').'</span>';
        $msg .= '<script>function _sp_check_ft(el) {var val=el.options[el.selectedIndex].value,'.
                'tgt = document.getElementById(\'_strike_allowallfile\');'.
                'if(parseInt(val)===0){tgt.style.display=\'\';}else{tgt.style.display=\'none\';}}</script>';
        $mform->addElement('static', '_strike_allowallfile', '', $msg);
        $mform->disabledIf('_strike_allowallfile', 'strike_allowallfile', 'eq', 0);
    }

    $mform->addElement('select', 'strike_selectfiletypes',
            get_string('restrictfiles', 'plagiarism_strike'), $supportedfiles, array('multiple' => true));

    $mform->addElement('select', 'strike_docsendmode',
            get_string('docsendmode', 'plagiarism_strike'), $sendoptions);
    $mform->addHelpButton('strike_docsendmode', 'docsendmode', 'plagiarism_strike');

    $mform->addElement('select', 'strike_addref_method',
            get_string("strike_addref_method", 'plagiarism_strike'), $addrefoptions);
    $mform->addHelpButton('strike_addref_method', 'strike_addref_method', 'plagiarism_strike');

    $mform->addElement('select', 'strike_deletereport_method',
            get_string("strike_deletereport_method", 'plagiarism_strike'), $deletereportoptions);
    $mform->addHelpButton('strike_deletereport_method', 'strike_deletereport_method', 'plagiarism_strike');

    $mform->addElement('select', 'strike_show_student_status',
            get_string("strike_show_student_status", 'plagiarism_strike'), $tiioptions);
    $mform->addHelpButton('strike_show_student_status', 'strike_show_student_status', 'plagiarism_strike');

    $mform->addElement('select', 'strike_show_student_score',
            get_string("strike_show_student_score", 'plagiarism_strike'), $tiioptions);
    $mform->addHelpButton('strike_show_student_score', 'strike_show_student_score', 'plagiarism_strike');

    $mform->addElement('select', 'strike_show_student_report',
            get_string('strike_show_student_report', 'plagiarism_strike'), $tiioptions);
    $mform->addHelpButton('strike_show_student_report', 'strike_show_student_report', 'plagiarism_strike');

    $mform->addElement('select', 'strike_studentemail',
            get_string('strike_studentemail', 'plagiarism_strike'), $ynoptions);
    $mform->addHelpButton('strike_studentemail', 'strike_studentemail', 'plagiarism_strike');

    $contentoptions = array(PLAGIARISM_STRIKE_RESTRICTCONTENTNO => get_string('restrictcontentno', 'plagiarism_strike'),
                            PLAGIARISM_STRIKE_RESTRICTCONTENTFILES => get_string('restrictcontentfiles', 'plagiarism_strike'),
                            PLAGIARISM_STRIKE_RESTRICTCONTENTTEXT => get_string('restrictcontenttext', 'plagiarism_strike'));
    $mform->addElement('select', 'strike_restrictcontent', get_string('restrictcontent', 'plagiarism_strike'), $contentoptions);
    $mform->addHelpButton('strike_restrictcontent', 'restrictcontent', 'plagiarism_strike');

}

/**
 * Set the disabilities of config form elements.
 *
 * @param \MoodleQuickForm $mform
 */
function strike_form_element_disable_rules($mform) {
    // If "submissiondrafts" is available (mod_assign); disable strike config.
    if ($mform->elementExists('strike_draft_submit') && $mform->elementExists('submissiondrafts')) {
        $mform->disabledIf('strike_draft_submit', 'submissiondrafts', 'eq', 0);
    }

    // Disable all plagiarism elements if use_plagiarism eg 0.
    $plugin = new plagiarism_plugin_strike();
    $plagiarismelements = $plugin->config_options();
    foreach ($plagiarismelements as $element => $type) {
        if ($element <> 'strike_use_strike') { // Ignore this var.
            $mform->disabledIf($element, 'strike_use_strike', 'eq', 0);
        }
    }
    // Only make file types selectable if "all files" is turned off.
    $mform->disabledIf('strike_selectfiletypes', 'strike_allowallfile', 'eq', 1);

}

/**
 * Create a temp strike file
 * @param int $cmid
 * @param int $courseid
 * @param int $userid
 * @param string $filecontent
 * @return string filepath
 */
function strike_create_temp_file($cmid, $courseid, $userid, $filecontent) {
    global $CFG;
    if (!check_dir_exists($CFG->tempdir . "/strike", true, true)) {
        mkdir($CFG->tempdir . "/strike", 0700);
    }
    $filename = "content-" . $courseid . "-" . $cmid . "-" . $userid . "-" . random_string(8) . ".txt";
    $filepath = $CFG->tempdir . "/strike/" . $filename;
    $fd = fopen($filepath, 'wb');
    // Write html and body tags as it seems that STRIKE doesn't works well without them.
    $content = plagiarism_strike_format_temp_content_text($filecontent, true);

    fwrite($fd, $content);
    fclose($fd);

    return $filepath;
}

/**
 * Format file contents so we can write a proper html file
 * @param string $content
 * @param boool $strippretag
 * @return string
 */
function plagiarism_strike_format_temp_content($content, $strippretag = false) {
    // See MDL-57886.
    if ($strippretag) {
        $content = substr($content, 25, strlen($content) - 31);
    }
    return '<html>' .
            '<head>' .
            '<meta charset="UTF-8">' .
            '</head>' .
            '<body>' .
            $content .
            '</body></html>';
}

/**
 * Format file contents so we can write a proper txt file
 * @param string $content
 * @param bool $writeutfbom
 * @return string
 */
function plagiarism_strike_format_temp_content_text($content, $writeutfbom = true) {
    $rs = '';
    if ($writeutfbom) {
        $rs .= "\xEF\xBB\xBF";
    }
    // Assumed input IS UTF-8, convert without links and unlimited line length.
    $rs .= html_to_text($content, 0, false);
    return $rs;
}

/**
 * Validate if the strike file is based on group submission
 * @param \plagiarism_strike\strikefile $plagiarismfile
 * @return \plagiarism_strike\strikefile
 */
function plagiarism_strike_check_group($plagiarismfile) {
    global $DB, $CFG;

    require_once("$CFG->dirroot/mod/assign/locallib.php");

    $modulecontext = context_module::instance($plagiarismfile->cm);
    $assign = new assign($modulecontext, false, false);

    if (!empty($assign->get_instance()->teamsubmission)) {
        mtrace("STRIKE fileid:".$plagiarismfile->id." Group submission detected.");
        $mygroups = groups_get_user_groups($assign->get_course()->id, $plagiarismfile->userid);
        if (count($mygroups) == 1) {
            $groupid = reset($mygroups)[0];
            // Only users with single groups are supported - otherwise just use the normal userid on this record.
            // Get all users from this group.
            $userids = array();
            $users = groups_get_members($groupid, 'u.id');
            foreach ($users as $u) {
                $userids[] = $u->id;
            }
            if (!empty($userids)) {
                // Find the earliest plagiarism record for this cm with any of these users.
                $sql = 'cm = ? AND userid IN (' . implode(',', $userids) . ')';
                $previousfiles = $DB->get_records_select(\plagiarism_strike\strikefile::table(), $sql,
                    array($plagiarismfile->cm), 'id');
                $sanitycheckusers = 10; // Search through this number of users to find a valid previous submission.
                $i = 0;
                foreach ($previousfiles as $pf) {
                    if ($pf->userid == $plagiarismfile->userid) {
                        return $plagiarismfile;
                    }
                    // Sanity Check to make sure the user isn't in multiple groups.
                    $pfgroups = groups_get_user_groups($assign->get_course()->id, $pf->userid);
                    if (count($pfgroups) == 1) {
                        // This user made the first valid submission so use their id when sending the file.
                        $plagiarismfile->userid = $pf->userid;
                        mtrace("STRIKE: Group submission by newuser, modify to use original userid:".
                               $pf->userid." id:".$plagiarismfile->id);
                        return $plagiarismfile;
                    }
                    if ($i >= $sanitycheckusers) {
                        // Don't cause a massive loop here and break at a sensible limit.
                        return $plagiarismfile;
                    }
                    $i++;
                }
            }
        }
    }
    return $plagiarismfile;
}

/**
 * Get temporary file object for a strikefile
 * @param plagiarism_strike\strikefile $plagiarismfile
 * @return \stdClass
 */
function strike_get_temp_file_object(plagiarism_strike\strikefile $plagiarismfile) {
    global $DB;
    // This is a stored text file in temp dir.
    $file = new stdClass();
    if (file_exists($plagiarismfile->filepath)) {
        $file->type = "tempstrike";
        $file->filename = $plagiarismfile->filename;
        $file->timestamp = time();
        $file->contenthash = sha1(file_get_contents($plagiarismfile->filepath));
        $file->filepath = $plagiarismfile->filepath;

        // Sanity check to see if the Sha1 for this file has already been sent to strike using a different record.
        $conditions = array('contenthash' => $file->contenthash,
                    'cm' => $plagiarismfile->cm,
                    'userid' => $plagiarismfile->userid);
        $record = $DB->get_record(\plagiarism_strike\strikefile::table(), $conditions);
        if (!empty($record)) {
            if ($record->id != $plagiarismfile->id) {
                // This file has already been sent and multiple records for this file were created
                // Delete plagiarism record and file.
                debugging("This file has been duplicated, deleting the duplicate record. ".
                        "Hash:{$file->contenthash}; PATH: {$file->filepath}");
                unlink($plagiarismfile->filepath); // Delete temp file as we don't need it anymore.
                $plagiarismfile->delete();
                return null;
            }
        }
        return $file;
    } else {
        debugging("The local version of this file has been deleted, and this file cannot be sent");
        return null;
    }
}

/**
 * Get assign file object for a strikefile
 * @param plagiarism_strike\strikefile $plagiarismfile
 * @return \stored_file|null
 */
function strike_get_assign_file_object(plagiarism_strike\strikefile $plagiarismfile) {
    global $CFG, $DB;
    require_once($CFG->dirroot . '/mod/assign/locallib.php');
    $modulecontext = context_module::instance($plagiarismfile->cm);
    $fs = get_file_storage();

    $userid = $plagiarismfile->userid;
    if (!empty($plagiarismfile->relateduserid)) {
        $userid = $plagiarismfile->relateduserid;
    }
    $assign = new assign($modulecontext, null, null);

    if ($assign->get_instance()->teamsubmission) {
        $submission = $assign->get_group_submission($userid, 0, false);
    } else {
        $submission = $assign->get_user_submission($userid, false);
    }
    $submissionplugins = $assign->get_submission_plugins();

    foreach ($submissionplugins as $submissionplugin) {
        $component = $submissionplugin->get_subtype() . '_' . $submissionplugin->get_type();
        $fileareas = $submissionplugin->get_file_areas();
        foreach ($fileareas as $filearea => $name) {
            if (debugging()) {
                mtrace("STRIKE fileid:" . $plagiarismfile->id . " Check component:" . $component . " Filearea:" .
                        $filearea . " Submission" . $submission->id);
            }
            $files = $fs->get_area_files(
                    $assign->get_context()->id, $component, $filearea, $submission->id, "timemodified", false
            );

            foreach ($files as $file) {
                if (debugging()) {
                    mtrace("STRIKE fileid:" . $plagiarismfile->id . " check fileid:" . $file->get_id());
                }
                if ($file->get_contenthash() == $plagiarismfile->contenthash) {
                    if (debugging()) {
                        mtrace("STRIKE fileid:" . $plagiarismfile->id . " found fileid:" . $file->get_id());
                    }
                    return $file;
                }
            }
        }
    }
    return null;
}

/**
 * Get workshop file object for a strikefile
 * @param plagiarism_strike\strikefile $plagiarismfile
 * @return \stored_file|null
 */
function strike_get_workshop_file_object(plagiarism_strike\strikefile $plagiarismfile) {
    global $CFG, $DB;
    require_once($CFG->dirroot . '/mod/workshop/locallib.php');

    $userid = $plagiarismfile->userid;
    if (!empty($plagiarismfile->relateduserid)) {
        $userid = $plagiarismfile->relateduserid;
    }

    $cm = get_coursemodule_from_id('workshop', $plagiarismfile->cm, 0, false, MUST_EXIST);
    $fs = get_file_storage();
    $workshop = $DB->get_record('workshop', array('id' => $cm->instance), '*', MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $workshop = new workshop($workshop, $cm, $course);
    $submissions = $workshop->get_submissions($userid);
    foreach ($submissions as $submission) {
        $files = $fs->get_area_files($workshop->context->id, 'mod_workshop', 'submission_attachment', $submission->id);
        foreach ($files as $file) {
            if (debugging()) {
                mtrace("STRIKE fileid {$plagiarismfile->id} check fileid " . $file->get_id());
            }
            if ($file->get_contenthash() == $plagiarismfile->contenthash) {
                if (debugging()) {
                    mtrace("STRIKE fileid {$plagiarismfile->id} found fileid " . $file->get_id());
                }
                return $file;
            }
        }
    }
    return null;
}

/**
 * Get forum file object for a strikefile
 * @param plagiarism_strike\strikefile $plagiarismfile
 * @return \stored_file|null
 */
function strike_get_forum_file_object(plagiarism_strike\strikefile $plagiarismfile) {
    global $CFG;
    require_once($CFG->dirroot . '/mod/forum/lib.php');

    $userid = $plagiarismfile->userid;
    if (!empty($plagiarismfile->relateduserid)) {
        $userid = $plagiarismfile->relateduserid;
    }

    $cm = get_coursemodule_from_id('forum', $plagiarismfile->cm, 0, false, MUST_EXIST);
    $modulecontext = context_module::instance($plagiarismfile->cm);
    $fs = get_file_storage();
    $posts = forum_get_user_posts($cm->instance, $userid);
    foreach ($posts as $post) {
        $files = $fs->get_area_files($modulecontext->id, 'mod_forum', 'attachment', $post->id, "timemodified", false);
        foreach ($files as $file) {
            if (debugging()) {
                mtrace("STRIKE fileid {$plagiarismfile->id} check fileid " . $file->get_id());
            }
            if ($file->get_contenthash() == $plagiarismfile->contenthash) {
                if (debugging()) {
                    mtrace("STRIKE fileid {$plagiarismfile->id} found fileid " . $file->get_id());
                }
                return $file;
            }
        }
    }
    return null;
}

/**
 * Extends the course navigation.
 *
 * @param \navigation_node $parentnode
 * @param \stdClass $course
 * @param \context_course $context
 */
function plagiarism_strike_extend_navigation_course(navigation_node $parentnode, stdClass $course, context_course $context) {
    global $CFG;
    // Only allow teachers.
    if (!has_capability('moodle/grade:viewall', $context)) {
        return;
    }
    // Find out what modules use STRIKE.
    $cms = get_fast_modinfo($course);
    $support = [];
    foreach ($cms->cms as $cm) {
        if (plugin_supports('mod', $cm->modname, FEATURE_PLAGIARISM)) {
            $config = new plagiarism_strike\config($cm->id);
            if ((bool)$config->get('strike_use_strike')) {
                $support[] = $cm;
            }
        }
    }
    if (empty($support)) {
        return;
    }
    // Ok this is a filthy work-around. These navigation items SHOULD be in the main "current course" navigation.
    $nodeproperties = array(
        'text'          => get_string('pluginname', 'plagiarism_strike'),
        'shorttext'     => get_string('pluginname', 'plagiarism_strike'),
        'type'          => navigation_node::TYPE_CONTAINER,
        'key'           => 'caplagiarismstrike'
    );
    $canode = new navigation_node($nodeproperties);
    if (!$parentnode->find('users', \navigation_node::TYPE_CONTAINER)) {
        return;
    }
    $parentnode->add_node($canode, 'users');

    foreach ($support as $cm) {
        $nodeproperties = array(
            'text'          => $cm->name,
            'shorttext'     => $cm->name,
            'type'          => navigation_node::TYPE_CUSTOM,
            'key'           => 'caplagiarismstrike' . $cm->id,
            'action'        => new moodle_url($CFG->wwwroot . '/plagiarism/strike/cmoverview.php', array('cm' => $cm->id))
        );
        $cmnode = new navigation_node($nodeproperties);
        $canode->add_node($cmnode);
    }
}