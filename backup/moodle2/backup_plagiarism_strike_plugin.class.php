<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Backup class for plagiarism_strike
 *
 * File         backup_plagiarism_strike_plugin.class.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') or die();

/**
 * Backup class for plagiarism_strike
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class backup_plagiarism_strike_plugin extends backup_plagiarism_plugin {

    /**
     * Define plugin structure
     * @return \backup_plugin_element
     */
    protected function define_module_plugin_structure() {
        $plugin = $this->get_plugin_element();

        $pluginelement = new backup_nested_element($this->get_recommended_name());
        $plugin->add_child($pluginelement);

        // Add module config elements.
        $strikeconfigs = new backup_nested_element('strike_configs');
        $strikeconfig = new backup_nested_element('strike_config', array('id'), array('name', 'value'));
        $pluginelement->add_child($strikeconfigs);
        $strikeconfigs->add_child($strikeconfig);
        $strikeconfig->set_source_table('plagiarism_strike_config', array('cm' => backup::VAR_PARENTID));

        // Add file elements if required.
        if ($this->get_setting_value('userinfo')) {
            $strikefiles = new backup_nested_element('strike_files');
            $strikefile = new backup_nested_element('strike_file', array('id'),
                                array('userid',
                                    'isstoredfile', 'relateduserid', 'automatedanalysis', 'doanalysis',
                                    'title', 'author', 'coordinator', 'reportready', 'indexed',
                                    'factor1', 'factor2', 'factor3', 'factor4', 'factor5', 'foreignalphabetalert',
                                    'md5sum', 'guid', 'contenthash', 'filename', 'pathname',
                                    'attempt', 'statuscode', 'errorresponse',
                                    'timesubmitted', 'timecreated', 'timemodified'
                                    ));
            $pluginelement->add_child($strikefiles);
            $strikefiles->add_child($strikefile);

            $strikefile->set_source_table('plagiarism_strike_files', array('cm' => backup::VAR_PARENTID));
        }

        return $plugin;
    }

    /**
     * Define plugin structure for course
     * @return \backup_plugin_element
     */
    protected function define_course_plugin_structure() {
        $plugin = $this->get_plugin_element();

        $pluginwrapper = new backup_nested_element($this->get_recommended_name());
        $plugin->add_child($pluginwrapper);

        // Save id from strike course.
        $strikeconfigs = new backup_nested_element('strike_configs');
        $strikeconfig = new backup_nested_element('strike_config', array('id'), array('plugin', 'name', 'value'));
        $pluginwrapper->add_child($strikeconfigs);
        $strikeconfigs->add_child($strikeconfig);
        $strikeconfig->set_source_table('config_plugins',
            array('name' => backup::VAR_PARENTID, 'plugin' => backup_helper::is_sqlparam('plagiarism_strike_course')));
        return $plugin;
    }
}