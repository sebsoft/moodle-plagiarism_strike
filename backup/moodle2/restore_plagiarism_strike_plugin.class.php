<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Restore class for plagiarism_strike
 *
 * File         restore_plagiarism_strike_plugin.class.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') or die();

/**
 * Restore class for plagiarism_strike
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class restore_plagiarism_strike_plugin extends restore_plagiarism_plugin {

    /**
     * Return the paths of the course data along with the function used for restoring that data.
     */
    protected function define_course_plugin_structure() {
        $paths = array();

        // Add own format stuff.
        $elename = 'strikeconfig';
        $elepath = $this->get_pathfor('strike_configs/strike_config'); // We used get_recommended_name() so this works.
        $paths[] = new restore_path_element($elename, $elepath);

        return $paths;
    }

    /**
     * Return the paths of the module data along with the function used for restoring that data.
     */
    protected function define_module_plugin_structure() {
        $paths = array();
        // Add own format stuff.
        $elename = 'strikeconfigmod';
        $elepath = $this->get_pathfor('strike_configs/strike_config'); // We used get_recommended_name() so this works.
        $paths[] = new restore_path_element($elename, $elepath);

        $elename = 'strikefiles';
        $elepath = $this->get_pathfor('/strike_files/strike_file'); // We used get_recommended_name() so this works.
        $paths[] = new restore_path_element($elename, $elepath);

        $elename = 'strikereports';
        $elepath = $this->get_pathfor('/strike_reports/strike_report'); // We used get_recommended_name() so this works.
        $paths[] = new restore_path_element($elename, $elepath);

        return $paths;
    }

    /**
     * Process strike config.
     * @param array $data data
     */
    public function process_strikeconfig($data) {
        global $DB;
        $data = (object)$data;
        set_config($this->task->get_courseid(), $data->value, $data->plugin);
    }

    /**
     * Process strike module config
     * @param array $data data
     */
    public function process_strikeconfigmod($data) {
        global $DB;

        $data = (object)$data;
        $data->cm = $this->task->get_moduleid();
        $data->config_hash = $data->cm . '_' . $data->name;

        $DB->insert_record('plagiarism_strike_config', $data);
    }

    /**
     * Restore the links to STRIKE files.
     * @param array $data data
     */
    public function process_strikefiles($data) {
        global $DB;

        $data = (object)$data;
        $data->cm = $this->task->get_moduleid();
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->relateduserid = $this->get_mappingid('user', $data->relateduserid);

        $DB->insert_record('plagiarism_strike_files', $data);
    }

    /**
     * Restore the link to STRIKE reports.
     * @param array $data data
     */
    public function process_strikereports($data) {
        global $DB;

        $data = (object)$data;
        $data->strikefileid = $this->get_mappingid('plagiarism_strike_files', $data->strikefileid);

        $DB->insert_record('plagiarism_strike_reports', $data);
    }

}