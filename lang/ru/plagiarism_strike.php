<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language file for plagiarism_strike, EN
 *
 * File         plagiarism_strike.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// General.
$string['pluginname'] = 'StrikePlagiarism.com';
$string['strike'] = 'StrikePlagiarism.com plugin';
$string['strikeplagiarism'] = 'Strike plagiarism';
$string['strikeexplain'] = 'Для получения дополнительной информации об этой системе и о том, как она работает, см.: <a href="http://www.strikeplagiarism.com">www.StrikePlagiarism.com</a>';

$string['task:syncreport'] = 'Синхронизировать отчет StrikePlagiarism.com';
$string['task:getreports'] = 'Синхронизация отчетов StrikePlagiarism.com';
$string['task:sendfiles'] = 'Отправить файлы в StrikePlagiarism.com API';

$string['usestrike'] = 'Использовать плагин';
$string['useadminnotifications'] = 'Уведомлять администратора об ошибках?';
$string['useadminnotifications_help'] = 'Сообщает администратору об ошибках при их возникновении (например, при общении с API StrikePlagiarism.com)';
$string['useteachernotifications'] = 'Уведомлять преподавателя(ей) об ошибках?';
$string['useteachernotifications_help'] = 'Уведомляет преподавателя(ей) об ошибках при их возникновении (например, при общении с API StrikePlagiarism.com)';
$string['strikekey'] = 'Strike API Key';
$string['strikekey_help'] = 'API-ключ для использования со strike<br/> Вам нужно получить ключ StrikePlagiarism.com ID, чтобы использовать этот плагин. Для получения дополнительной информации свяжитесь с нами через контактную форму на сайте <a href="http://www.strikeplagiarism.com">www.StrikePlagiarism.com</a>';
$string['strikeserver'] = 'Strike API Host';
$string['strikeserver_help'] = 'Strike server hostname или ip адрес. Введите адрес IP StrikePlagiarism.com с http или https';
$string['studentdisclosuredefault'] = 'Я согласен, что все файлы, которые я загрузил, могут быть отправлены в антиплагиатную систему Strikeplagiarism.com с целью проверки оригинальности текста.';
$string['strikestudentdisclosure'] = 'Я согласен, что все файлы, которые я загрузил, могут быть отправлены в антиплагиатную систему Strikeplagiarism.com с целью проверки оригинальности текста.';
$string['studentdisclosure'] = 'Заявление студента';
$string['studentdisclosure_help'] = 'Этот текст будет отображаться всем учащимся на странице загрузки файлов. Он должен быть принят для продолжения проверки.';
$string['strike_enableplugin'] = 'Включить StrikePlagiarism.com для {$a}';
$string['notifyteachers'] = 'Уведомить преподавателя (ей)';
$string['enableapidebugging'] = 'Включить отладку API';
$string['enableapidebugging_help'] = 'Включите эту опцию, если вы хотите расширенную отладку, предоставляемую API StrikePlagiarism.com.<br/>
Доступны несколько методов отладки. Обратите внимание, что отладка будет <i> только </ i> состоять из информации о связи / трафике на API StrikePlagiarism.com.
';
$string['apidebuggingmtrace'] = 'Включить отладку mtrace?';
$string['apidebuggingmtrace_help'] = 'Включите эту опцию, если вы хотите видеть вывод отладки API как вывод mtrace (). <br/>
Если вы включите это, вывод трассировки будет виден на выходе фоновой задачи (CRON).
';
$string['apidebuggingerrorlog'] = 'Включить отладку error_log?';
$string['apidebuggingerrorlog_help'] = 'Включите эту опцию, если вы хотите видеть вывод отладки API как вывод ошибки. <br/>
Если вы включите это, вывод трассировки будет виден в webserver / php error_log. <br/>
ПРИМЕЧАНИЕ. Это не всегда будет отображаться в зависимости от того, как настроен ваш веб-сервер или PHP.
';
$string['apidebuggingemail'] = 'Включить отладку электронной почты?';
$string['apidebuggingemail_help'] = 'Включите эту опцию, если вы хотите получить вывод отладки API в своем письме. <br/>
Если вы включите это, вывод трассировки будет отправлен на указанный адрес электронной почты. <br/>
ПРИМЕЧАНИЕ. Это может привести к отправке большого количества писем. <br/>
ПРИМЕЧАНИЕ 2. Этот метод использует функцию mail () по умолчанию и не будет всегда доступен. Он работает вне функций почты Moodle.
';
$string['apidebuggingemailaddress'] = 'Получатель отладки электронной почты.';

$string['str:settings'] = 'Strike настройки';
$string['str:defaults'] = 'Strike значения по умолчанию';
$string['str:dump'] = 'Strike Dump';
$string['str:debug'] = 'Strike отладка';
$string['allowallsupportedfiles'] = 'Разрешить все поддерживаемые типы файлов';
$string['allowallsupportedfiles_help'] = 'Это позволяет преподавателю ограничивать, какие типы файлов будут отправлены на StrikePlagiarism.com для обработки. Это не мешает студентам загружать разные типы файлов в задание.';
$string['restrictfiles'] = 'Типы файлов для отправки';
$string['form:desc:defaults'] = 'Ниже вы можете ввести все параметры по умолчанию, которые будут использоваться при создании и настройке нового модуля активности. <br/>
Значения, введенные здесь, будут служить в качестве значений по умолчанию для конфигурации StrikePlagiarism для этого модуля активности.';
$string['docsendmode'] = 'Отправка документа';
$string['docsendmode_help'] = 'Выберите метод отправки документа для анализа. Это может быть либо по выбору (что означает, что учитель выбирает документы для отправки для анализа), либо все (что означает, что все документы отправляются для анализа по умолчанию)';
$string['docsendingchoose'] = 'Выберите документы для отправки для анализа';
$string['docsendingall'] = 'Отправка всех документов для анализа';
$string['strike_show_student_status'] = 'Показать статус StrikePlagiarism.com студенту';
$string['strike_show_student_status_help'] = 'Статус позволяет студентам видеть статус отправки на StrikePlagiarism.com.';
$string['strike_show_student_report'] = 'Показать отчет подобия студенту';
$string['strike_show_student_report_help'] = 'Отчет подобия дает информацию о том, какие части материала были опредлены как заимстования, и об источнике, в которых StrikePlagiarism.com впервые обнаружил этот контент';
$string['strike_show_student_score'] = 'Показать оценку подобия студенту';
$string['strike_show_student_score_help'] = 'Оценка подобия - это процентная доля представленного материала, которая совпала с другим контентом.';
$string['strike_addref_method'] = 'Добавить ссылочный метод.';
$string['strike_addref_method_help'] = 'Это определяет, как документы, проверенные StrikePlagiarism.com, добавляются в справочную базу данных. <br/>
Настоятельно рекомендуется установить это на AUTOMATIC, тогда проверенные документы автоматически добавляются в справочную базу данных.';
$string['strike_deletereport_method_help'] = 'Это определяет то, как отчеты подобия удаляются всякий раз, когда удаляется или обновляется материал. <br/>
Возможные методы:
<UL>
<li> сохранить: сохраняет старые отчеты подобия </ li>
<li> remove: удаляет старые отчеты подобия </ li>
</ UL>';
$string['strike_studentemail'] = 'Отправить студенту сообщение через электронную почту';
$string['strike_studentemail_help'] = 'Это отправит студенту сообщение по электронной почте, когда файл будет обработан, чтобы сообщить им, что отчет доступен, в электронное письмо также входит ссылка.';
$string['strike_draft_submit'] = 'Когда должен быть отправлен файл';
$string['strike_defaultlang'] = 'Язык документа по умолчанию';
$string['strike_defaultlang_help'] = 'Это язык по умолчанию, используемый для отправки документов на StrikePlagiarism.com. Это обязательный параметр для их API, однако Moodle не знает этот тип информации';
$string['wordcount'] = 'Минимальное количество слов';
$string['wordcount_help'] = 'Это устанавливает минимальный предел количества слов, которые требуются для текстового текста (сообщений в форумах и онлайн-текстах), прежде чем контент будет отправлен на StrikePlagiarism.com.';

$string['addrefauto'] = 'Автоматически добавлять ссылки на документы после проверки';
$string['addrefmanual'] = 'Вручную добавьте ссылки на документы после проверки';

$string['deletereportkeep'] = 'Сохранять старые отчеты подобия удаленных / обновленных материалов';
$string['deletereportremove'] = 'Удаление старых отчетов подобия удаленных / обновленных материалов';

$string['submitondraft'] = 'Отправить файл при первой загрузке';
$string['submitonfinal'] = 'Отправить файл, когда отправитель отправляет для маркировки';
$string['showwhenclosed'] = 'Когда активность закрыта';

$string['pending'] = 'Файл ожидает отправки на StrikePlagiarism.com';
$string['processing'] = 'Файл был отправлен на StrikePlagiarism.com и ожидает завершения анализа';
$string['toolarge'] = 'Файл слишком велик для отправки на StrikePlagiarism.com';
$string['unsupported'] = 'Тип файла не поддерживается StrikePlagiarism.com';
$string['unsupportedfiletype'] = 'Тип файла не поддерживается StrikePlagiarism.com';
$string['unknownwarning'] = 'Произошла ошибка при отправке этого файла на StrikePlagiarism.com';
$string['awaitanalysischoice'] = 'В ожидании выбора анализа StrikePlagiarism.com';
$string['previouslysubmitted'] = 'Ранее представленный как';
$string['sendforanalysis'] = 'Добавить материал на StrikePlagiarism.com для анализа';
$string['indexdocument'] = 'Добавить материал в базу данных StrikePlagiarism.com';
$string['deletestrikefile'] = 'Удалить локальную ссылку на StrikePlagiarism.com';

$string['similarity1'] = 'Коэффициент подобия 1';
$string['similarity2'] = 'Коэффициент подобия 2';
$string['similarity3'] = 'Коэффициент подобия 3';
$string['similarity4'] = 'Коэффициент подобия 4';
$string['similarity5'] = 'Коэффициент подобия 5';

$string['filereset'] = 'Файл был обнулен для повторной отправки на StrikePlagiarism.com';
$string['fileresetfailed'] = 'Ошибка: файл не был обнулен для повторной отправки на StrikePlagiarism.com';

$string['report'] = 'StrikePlagiarism отчет';
$string['reportclick'] = 'Ссылка на отчет StrikePlagiarism';

$string['restrictcontent'] = 'Отправить прикрепленный файлы и онлайн текст';
$string['restrictcontent_help'] = 'StrikePlagiarism.com может обрабатывать загруженные файлы, но также может обрабатывать текст из сообщений форума и онлайн текст. Вы можете решить, какие компоненты отправлять на StrikePlagiarism.com. ';
$string['restrictcontentfiles'] = 'ОТправить только прикрепленные файлы';
$string['restrictcontentno'] = 'Отправить все';
$string['restrictcontenttext'] = 'Отправить онлайн текст';

$string['config:plagiarism'] = 'Значения конфигурации глобального плагиата';
$string['config:defaults'] = 'Настройки по умолчанию модуля StrikePlagiarism.com';

$string['studentemailsubject'] = 'Обработка заявки StrikePlagiarism.com Plagiarism';
$string['studentemailcontent'] = '<p>Уважаемый {$a->name} {$a->surname},</p>
Файл, который вы выслали {$a->modulename} в {$a->coursename} был обработан StrikePlagiarism.com.<br/>
{$a->modulelink}<br/>
c уважением,
{$a->admin}
';

$string['label:user'] = 'Пользователь';
$string['label:author'] = 'Автор';
$string['label:timesubmitted'] = 'Выслан';
$string['label:analysisstatus'] = 'Статус анализа';
$string['label:reportstatus'] = 'Статус отчета';
$string['label:status'] = 'Статус';
$string['label:filename'] = 'Название';
$string['label:modname'] = 'Модуль';
$string['label:actions'] = 'Дайствия';

$string['report:na'] = 'Недоступен';
$string['report:ready'] = 'Готов';
$string['report:available'] = 'Доступен';
$string['analysis:auto'] = 'автоматический';
$string['analysis:manual'] = 'мануальный';
$string['analysis:awaitchoice'] = 'ожидание выбора';
$string['cmoverview'] = 'Статус модуля курса в Strike';
$string['notification:subject'] = 'StrikePlagiarism.com plagiarism сообщение [{$a}]';
$string['notification:errormessage'] = '<p>Уважаемый {$a->fullname}<p>
<p>Произошла ошибка в плагине StrikePlagiarism.com в \'{$a->instancename}\' в курсе \'{$a->coursefullname}\'.<br/>
Возникла ошибка: {$a->errormessage}.</p>
<p>Ниже приведены данные файла:<br/>
Автор: {$a->strikefileauthor}<br/>
Координатор: {$a->strikefilecoordinator}<br/>
Название: {$a->strikefiletitle}<br/>
Название файла: {$a->strikefilename}<br/>
GUID: {$a->strikefileguid}<br/>
Время создания: {$a->strikefiletimecreated}
</p>
<p>Пожалуйста, перейдите к <a href="{$a->contexturl}">{$a->contexturlname}</a> подробнее</p>
<p>с уважением,<br/>{$a->signoff}</p>';
$string['messageprovider:strikenotification'] = 'StrikePlagiarism.com уведомления об ошибках';

$string['err:strike_selectfiletypes-empty'] = 'Пожалуйста, выберите виды документов для отправки.<br/>
Если вид документа не будет выбран,  документы не будут высылаться для проверки на  плагиат.';
$string['warn:strike_selectfiletypes-empty'] = 'Пожалуйста, выберите виды документов для отправки.<br/>
Если вид документа не будет выбран,  документы не будут высылаться для проверки на  плагиат.';