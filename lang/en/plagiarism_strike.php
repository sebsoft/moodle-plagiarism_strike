<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language file for plagiarism_strike, EN
 *
 * File         plagiarism_strike.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// General.
$string['pluginname'] = 'StrikePlagiarism.com';
$string['strike'] = 'StrikePlagiarism.com plugin';
$string['strikeplagiarism'] = 'Strike plagiarism';
$string['strikeexplain'] = 'For more information on this antiplagiarism system and how it works see: <a href="http://www.strikeplagiarism.com">www.StrikePlagiarism.com</a>';

$string['task:syncreport'] = 'Synchronize StrikePlagiarism.com report for plagiarism document';
$string['task:getreports'] = 'Synchronize StrikePlagiarism.com plagiarism reports';
$string['task:sendfiles'] = 'Send files to StrikePlagiarism.com plagiarism API';

$string['usestrike'] = 'Use strike plagiarism plugin';
$string['useadminnotifications'] = 'Notify admin about errors?';
$string['useadminnotifications_help'] = 'Notifies admin about errors when various errors occur (e.g. when communicating with the StrikePlagiarism.com API)';
$string['useteachernotifications'] = 'Notify teacher(s) about errors?';
$string['useteachernotifications_help'] = 'Notifies teacher(s) about errors when various errors occur (e.g. when communicating with the StrikePlagiarism.com API)';
$string['strikekey'] = 'Strike API Key';
$string['strikekey_help'] = 'API Key to use with strike<br/>You need to obtain an StrikePlagiarism.com ID key to use this plugin. For more information please contact us via website contact form on <a href="http://www.strikeplagiarism.com">www.StrikePlagiarism.com</a>';
$string['strikeserver'] = 'Strike API Host';
$string['strikeserver_help'] = 'Strike server hostname or ip address. Enter StrikePlagiarism.com IP address with http or https';
$string['studentdisclosuredefault'] = 'I agree that all files which I uploaded might be submitted to the Internet Antiplagiarism System StrikePlagiarism.com in order to check oryginality of the text';
$string['strikestudentdisclosure'] = 'I agree that all files which I uploaded might be submitted to the Internet Antiplagiarism System StrikePlagiarism.com in order to check oryginality of the text';
$string['studentdisclosure'] = 'Student Disclosure';
$string['studentdisclosure_help'] = 'This text will be displayed to all students on the file upload page. It has to be agreed on in order to proceed with submission.';
$string['strike_enableplugin'] = 'Enable StrikePlagiarism.com for {$a}';
$string['notifyteachers'] = 'Notify teacher(s)';
$string['enableapidebugging'] = 'Enable API debugging';
$string['enableapidebugging_help'] = 'Turn this option on if you want extended debugging provided by the StrikePlagiarism.com API.<br/>
Multiple debugging methods are available. Do note the debugging will <i>only</i> consist of communication/traffic information on the StrikePlagiarism.com API.
';
$string['apidebuggingmtrace'] = 'Enable mtrace debugging?';
$string['apidebuggingmtrace_help'] = 'Enable this option if you want to see the API debugging output as mtrace() output.<br/>
If you enable this, the trace output will be visible in your background task output (CRON).
';
$string['apidebuggingerrorlog'] = 'Enable error_log debugging?';
$string['apidebuggingerrorlog_help'] = 'Enable this option if you want to see the API debugging output as error_log output.<br/>
If you enable this, the trace output will be visible in webserver / php error_log.<br/>
NOTE: This will not always be visible depending on how your webserver or PHP is configured.
';
$string['apidebuggingemail'] = 'Enable email debugging?';
$string['apidebuggingemail_help'] = 'Enable this option if you want to receive the API debugging output in your email.<br/>
If you enable this, the trace output will be send to the specified email address.<br/>
NOTE: This can cause a lot of emails to be sent.<br/>
NOTE 2: This method makes use of the default mail() function, and will not alway be available. It works outside of the Moodle mail functions.
';
$string['apidebuggingemailaddress'] = 'Email debugging recipient.';

$string['str:settings'] = 'Strike Settings';
$string['str:defaults'] = 'Strike Defaults';
$string['str:dump'] = 'Strike Dump';
$string['str:debug'] = 'Strike Debug';
$string['allowallsupportedfiles'] = 'Allow all supported file types';
$string['allowallsupportedfiles_help'] = 'This allows the teacher to restrict which file types will be sent to StrikePlagiarism.com for processing. It does not prevent students from uploading different file types to the assignment.';
$string['restrictfiles'] = 'File types to submit';
$string['form:desc:defaults'] = 'Below you can enter all default settings that will be used when a new activity module is created and has to be configured.<br/>
The values entered here will serve as default values for the StrikePlagiarism configuration for that activity module.';
$string['docsendmode'] = 'Document sending';
$string['docsendmode_help'] = 'Choose the method to send document for analysis. This can be either by choice (meaning the teacher selects documents to send for analysis) or all (meaning all documents are sent for analysis by default)';
$string['docsendingchoose'] = 'Choose documents to send for analysis';
$string['docsendingall'] = 'Send all documents for analysis';
$string['strike_show_student_status'] = 'Show StrikePlagiarism.com status to student';
$string['strike_show_student_status_help'] = 'The status allows students to see the status of submissions to StrikePlagiarism.com.';
$string['strike_show_student_report'] = 'Show similarity report to student';
$string['strike_show_student_report_help'] = 'The similarity report gives a breakdown on what parts of the submission were plagiarised and the location that StrikePlagiarism.com first saw this content';
$string['strike_show_student_score'] = 'Show similarity score to student';
$string['strike_show_student_score_help'] = 'The similarity score is the percentage of the submission that has been matched with other content.';
$string['strike_addref_method'] = 'Add reference method.';
$string['strike_addref_method_help'] = 'This defines the way documents that are checked by StrikePlagiarism.com are added to the reference database.<br/>
It is highly adviced to set this to AUTOMATIC, so checked documents are automatically added to the reference database.';
$string['strike_deletereport_method'] = 'Similarity report deletion.';
$string['strike_deletereport_method_help'] = 'This defines the way similarity reports are deleted whenever a submission is deleted or updated.<br/>
Possible methods are:
<ul>
<li>keep: Keeps old similarity reports</li>
<li>remove: Removes old similarity reports</li>
</ul>';
$string['strike_studentemail'] = 'Send student email';
$string['strike_studentemail_help'] = 'This will send an e-mail to the student when a file has been processed to let them know that a report is available, the e-mail also includes the opt-out link.';
$string['strike_draft_submit'] = 'When should the file be submitted';
$string['strike_defaultlang'] = 'Default document language';
$string['strike_defaultlang_help'] = 'This is the default language used to send documents to StrikePlagiarism.com. It is a mandatory parameter to their API, however Moodle does not know this type of information';
$string['wordcount'] = 'Minimum word count';
$string['wordcount_help'] = 'This sets a minimum limit on the number of words that are required for in-line text (forum posts and online assignment type) before the content will be sent to StrikePlagiarism.com.';

$string['addrefauto'] = 'Automatically add document references after checking';
$string['addrefmanual'] = 'Manually add document references after checking';

$string['deletereportkeep'] = 'Keep old similarity reports for deleted/updated submissions';
$string['deletereportremove'] = 'Remove old similarity reports for deleted/updated submissions';

$string['submitondraft'] = 'Submit file when first uploaded';
$string['submitonfinal'] = 'Submit file when student sends for marking';
$string['showwhenclosed'] = 'When Activity closed';

$string['pending'] = 'File is pending submission to StrikePlagiarism.com';
$string['processing'] = 'File has been submitted to StrikePlagiarism.com and is awaiting analysis to complete';
$string['toolarge'] = 'File is too big to be submitted to StrikePlagiarism.com';
$string['unsupported'] = 'File type is not supported by StrikePlagiarism.com';
$string['unsupportedfiletype'] = 'File type is not supported by StrikePlagiarism.com';
$string['unknownwarning'] = 'There was an error submitting this file to StrikePlagiarism.com';
$string['awaitanalysischoice'] = 'Awaiting choice to submit to StrikePlagiarism.com for analysis';
$string['previouslysubmitted'] = 'Previously submitted as';
$string['sendforanalysis'] = 'Add submission to StrikePlagiarism.com for analysis';
$string['indexdocument'] = 'Include submission to StrikePlagiarism.com database';
$string['deletestrikefile'] = 'Delete local StrikePlagiarism.com file reference';

$string['similarity1'] = 'Similarity Coefficient 1';
$string['similarity2'] = 'Similarity Coefficient 2';
$string['similarity3'] = 'Similarity Coefficient 3';
$string['similarity4'] = 'Similarity Coefficient 4';
$string['similarity5'] = 'Similarity Coefficient 5';

$string['filereset'] = 'A file has been reset for re-submission to StrikePlagiarism.com';
$string['fileresetfailed'] = 'Error: file has not been reset for re-submission to StrikePlagiarism.com';

$string['report'] = 'StrikePlagiarism report';
$string['reportclick'] = 'Link to StrikePlagiarism report';

$string['restrictcontent'] = 'Submit attached files and in-line text';
$string['restrictcontent_help'] = 'StrikePlagiarism.com can process uploaded files but can also process in-line text from forum posts and text from the online text assignment submission type. You can decide which components to send to StrikePlagiarism.com.';
$string['restrictcontentfiles'] = 'Only submit attached files';
$string['restrictcontentno'] = 'Submit everything';
$string['restrictcontenttext'] = 'Only submit in-line text';

$string['config:plagiarism'] = 'Global plagiarism configuration values';
$string['config:defaults'] = 'StrikePlagiarism.com plagiarism module configuration defaults';

$string['studentemailsubject'] = 'Submission processed by StrikePlagiarism.com Plagiarism';
$string['studentemailcontent'] = '<p>Dear {$a->firstname} {$a->lastname},</p>
The file you submitted to {$a->modulename} in {$a->coursename} has now been processed by StrikePlagiarism.com.<br/>
{$a->modulelink}<br/>
Regards,
{$a->admin}
';

$string['label:user'] = 'User';
$string['label:author'] = 'Author';
$string['label:timesubmitted'] = 'Submitted on';
$string['label:analysisstatus'] = 'Analysis status';
$string['label:reportstatus'] = 'Report status';
$string['label:status'] = 'Status';
$string['label:filename'] = 'Filename';
$string['label:modname'] = 'Module';
$string['label:actions'] = 'Action(s)';

$string['report:na'] = 'N/A';
$string['report:ready'] = 'Ready';
$string['report:available'] = 'Available';
$string['analysis:auto'] = 'automated';
$string['analysis:manual'] = 'manual';
$string['analysis:awaitchoice'] = 'awaiting choice';
$string['cmoverview'] = 'Strike course module submission status';
$string['notification:subject'] = 'StrikePlagiarism.com plagiarism message [{$a}]';
$string['notification:errormessage'] = '<p>Dear {$a->fullname}<p>
<p>An error occured in the StrikePlagiarism.com plagiarism plugin in \'{$a->instancename}\' in course \'{$a->coursefullname}\'.<br/>
The error raised was: {$a->errormessage}.</p>
<p>Below are the file details:<br/>
Author: {$a->strikefileauthor}<br/>
Coordinator: {$a->strikefilecoordinator}<br/>
Title: {$a->strikefiletitle}<br/>
Filename: {$a->strikefilename}<br/>
GUID: {$a->strikefileguid}<br/>
Time created: {$a->strikefiletimecreated}
</p>
<p>Please go to <a href="{$a->contexturl}">{$a->contexturlname}</a> for more details</p>
<p>Regards,<br/>{$a->signoff}</p>';
$string['messageprovider:strikenotification'] = 'StrikePlagiarism.com error notifications';

$string['err:strike_selectfiletypes-empty'] = 'Please make a selection of document types to submit.<br/>
Since no document types are chosen now, no documents would ever be submitted to the plagiarism API.';
$string['warn:strike_selectfiletypes-empty'] = 'Please make a selection of document types to submit below.<br/>
If no document types are chosen, no documents would ever be submitted to the plagiarism API.';

// Added by INTERSIEC Kamil Łuczak @ 01.08.2018.
$string['test_api_connection'] = 'Test API connection';
$string['test_api_connection_status'] = 'API connection status';
$string['test_api_connection_help'] = 'You can check that current entered host API and API Key are working correctly.';
$string['api_status_1'] = 'Connection ok';
$string['api_status_2'] = 'Key is not valid';
$string['api_status_3'] = 'Connection timeout';
$string['api_status_4'] = 'Connection ok, server not responding';
$string['api_status_5'] = 'Connection error code: {$a->error}';

$string['privacy:metadata:userid'] = 'Information about user that is submitting file for plagiarism check.';
$string['privacy:metadata:relateduserid'] = 'Information about related that is submitting file for plagiarism check.';
$string['privacy:metadata:plagiarism_strike_files'] = 'Table that stores informations about files submitted for plagiarism check.';
$string['privacy:metadata:title'] = 'Title of document created by user.';
$string['privacy:metadata:author'] = 'Submitted file author or co-author name.';
$string['privacy:metadata:coordinator'] = 'Submitted file promoter.';
$string['privacy:metadata:md5sum'] = 'MD5 sum of the binary document sent for analysis.';
$string['privacy:metadata:ftimesubmitted'] = 'StrikePlagiarism.com file submission time.';
$string['privacy:metadata:ftimecreated'] = 'StrikePlagiarism.com file record creation time.';
$string['privacy:metadata:ftimemodified'] = 'StrikePlagiarism.com submission modification time.';

$string['privacy:metadata:reportready'] = 'Boolean true if the report is ready for download, false otherwise.';
$string['privacy:metadata:indexed'] = 'Boolean true if the document has been indexed.';
$string['privacy:metadata:factor1'] = 'Number The first factor / coefficient.';
$string['privacy:metadata:factor2'] = 'Number The second factor / coefficient.';
$string['privacy:metadata:factor3'] = 'Number The third factor / coefficient.';
$string['privacy:metadata:factor4'] = 'Number The fourth factor / coefficient.';
$string['privacy:metadata:factor5'] = 'Number The fifth factor / coefficient.';

$string['privacy:metadata:strikefileid'] = 'StrikePlagiarism.com submission fileid.';
$string['privacy:metadata:report'] = 'StrikePlagiarism.com submission report data.';
$string['privacy:metadata:timemodified'] = 'StrikePlagiarism.com submission report modification time.';
$string['privacy:metadata:timecreated'] = 'StrikePlagiarism.com submission report creation time.';
$string['privacy:metadata:plagiarism_strike_reports'] = 'StrikePlagiarism.com creates report for each file submission. That record is stored in Moodle database. Each report is related to user file submission.';

$string['privacy:metadata:filename'] = 'Filename of file submitted by user.';
$string['privacy:metadata:plagiarism_strike:data'] = 'Personal data passed through from the plagiarism subsystem.';
$string['privacy:metadata:plagiarism_strike:externalpurpose'] = 'This plugin sends data externally using the StrikePlagiarism.com API.';

$string['privacy:metadata:guid'] = 'StrikePlagiarism.com creates guid which is file relation to submission to StrikePlagiarism.com API.';
$string['privacy:metadata:core_files'] = 'StrikePlagiarism.com stores files that have been uploaded to Moodle to form a StrikePlagiarism.com submission.';