<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language file for plagiarism_strike, EN
 *
 * File         plagiarism_strike.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// General.
$string['pluginname'] = 'StrikePlagiarism.com Sistem Antiplagiat prin Internet';
$string['strike'] = 'plugin StrikePlagiarism.com';
$string['strikeplagiarism'] = 'StrikePlagiarism.com';
$string['strikeexplain'] = 'Pentru mai multe informații cu privire la acest sistem antiplagiat și modalitatea în care funcționează, vizitați: <a href="http://www.sistemantiplagiat.ro">www.Sistemantiplagiat.ro</a>';

$string['task:syncreport'] = 'Sincronizați raportul StrikePlagiarism.com pentru document';
$string['task:getreports'] = 'Sincronizați rapoartele StrikePlagiarism.com';
$string['task:sendfiles'] = 'Trimiteți fișierele către API-ul StrikePlagiarism.com';

$string['usestrike'] = 'Folosiți pluginul StrikePlagiarism.com';
$string['useadminnotifications'] = 'Notificați administratorul despre erori?';
$string['useadminnotifications_help'] = 'Notifică administratorul cu privire la erori atunci când apar diverse erori (e.g. în comunicarea cu API-ul StrikePlagiarism.com)';
$string['useteachernotifications'] = 'Notifică profesorul/profesorii cu privire la erori?';
$string['useteachernotifications_help'] = 'Notifică profesorul/profesorii cu privire la erori atunci când apar diverse erori (e.g. în comunicarea cu API-ul StrikePlagiarism.com)';
$string['strikekey'] = 'Strike API Key';
$string['strikekey_help'] = 'Cheie API folosită cu strike<br/>Trebuie să obțineți o cheie ID StrikePlagiarism.com pentru a folosi acest plugin. Pentru mai multe informații vă rugăm să ne contactați la website contact form on <a href="http://www.strikeplagiarism.com">www.StrikePlagiarism.com</a>';
$string['strikeserver'] = 'Strike API Host';
$string['strikeserver_help'] = 'Hostname-ul serverului Strike sau adresa ip. Introduceți adresa ip a StrikePlagiarism.com cu http sau https';
$string['studentdisclosuredefault'] = 'Accept faptul că toate fișierele pe care le încarc ar putea fi transmise către Sistemul Antiplagiat StrikePlagiarism.com pentru a verifica originalitatea textului.';
$string['strikestudentdisclosure'] = 'Accept faptul că toate fișierele pe care le încarc ar putea fi transmise către Sistemul Antiplagiat StrikePlagiarism.com pentru a verifica originalitatea textului.';
$string['studentdisclosure'] = 'Informare Student';
$string['studentdisclosure_help'] = 'Acest text va fi afișat studenților în pagina de încărcare fișiere. Trebuie să fie acceptat pentru a continua încărcarea.';
$string['strike_enableplugin'] = 'Activare StrikePlagiarism.com pentru {$a}';
$string['notifyteachers'] = 'Notificare profesor/profesori';
$string['enableapidebugging'] = 'Activare debugging API';
$string['enableapidebugging_help'] = 'Activați această opțiune dacă doriți informații amănunțite pentru debugging oferit the API-ul StrikePlagiarism.com.<br/>
Sunt disponibile mai multe metode pentru debugging. Rețineți faptul că informațiile furnizate pentru debugging sunt formate din informații despre comunicații/trafic cu API-ul StrikePlagiarism.com.
';
$string['apidebuggingmtrace'] = 'Activare debugging mtrace?';
$string['apidebuggingmtrace_help'] = 'Activați această opțiune dacă doriți să vedeți informațiile de debugging ale API-ului ca informații tip mtrace().<br/>
Dacă activați această opțiune, informațiile trace vor fi vizibile în rezultatele procesului de background (CRON).
';
$string['apidebuggingerrorlog'] = 'Activați debugging pentru error_log?';
$string['apidebuggingerrorlog_help'] = 'Activați această opțiune dacă doriți să vedeți informațiile de debugging ale API-ului ca informații tip error_log.<br/>
Dacă activați această opțiune, informațiile trace vor fi vizibile în error_log-ul serverului web / serviciului php.<br/>
NOTĂ: Acest lucru nu va fi întotdeauna vizibil, depinzând de modul în care serverul web sau serviciul PHP este configurat PHP.
';
$string['apidebuggingemail'] = 'Activați debugging prin e-mail?';
$string['apidebuggingemail_help'] = 'Activați această opțiune dacă doriți să primiți informațiile de debugging ale API-ului prin e-mail.<br/>
Dacă activați această opțiune, informațiile trace vor fi transmise la adresa de e-mail specificată.<br/>
NOTĂ: Acest lucru poate cauza trimiterea unui număr mare de e-mailuri.<br/>
NOTĂ 2: Această metodă folosește funcția de e-mail() implicită și nu va fi disponibilă mereu. Funcționează în afara funcției de Moodle mail.
';
$string['apidebuggingemailaddress'] = 'Destinatar e-mail debugging.';

$string['str:settings'] = 'Setări Strike';
$string['str:defaults'] = 'Predefinite Strike';
$string['str:dump'] = 'Dump Strike';
$string['str:debug'] = 'Debug Strike';
$string['allowallsupportedfiles'] = 'Acceptă toate tipurile de fișiere permise';
$string['allowallsupportedfiles_help'] = 'Permite profesorului să restricționeze tipurile de fișiere care vor fi transmise către StrikePlagiarism.com pentru procesare. Nu îi împiedică pe studenți să încarce alte tipuri de fișiere în Teme.';
$string['restrictfiles'] = 'Tipuri de fișiere de încărcat';
$string['form:desc:defaults'] = 'Mai jos puteți introduce toate setările predefinite care vor fi folosite atunci când se creează un nou modul de activitate și trebuie să fie configurat.<br/>
Valorile introduse aici vor folosi ca valori predefinite la configurarea Strikeplagiarism.com pentru acel modul de activitate.';
$string['docsendmode'] = 'Trimitere document';
$string['docsendmode_help'] = 'Alegeți metoda de trimitere a documentului pentru analiză. Trimiterea se face fie selectiv (însemnând că profesorul selectează documentele care vor fi transmise) sau toate (însemnând că toate documentele sunt transmise implicit către analiză)';
$string['docsendingchoose'] = 'Alegeți documente pentru analiză';
$string['docsendingall'] = 'Trimiteți toate documentele pentru analiză';
$string['strike_show_student_status'] = 'Arătați statusul StrikePlagiarism.com studentului';
$string['strike_show_student_status_help'] = 'Statusul îi permite studentului să vadă statusul documentului transmis către StrikePlagiarism.com.';
$string['strike_show_student_report'] = 'Arătați Raportul de Similitudine studentului';
$string['strike_show_student_report_help'] = 'Raportul de Similitudine oferă detalii cu privire la părțile din document care au fost plagiate și locația în care a fost găsit conținutul de StrikePlagiarism.com';
$string['strike_show_student_score'] = 'Arătați coeficientii de similitudine studentului';
$string['strike_show_student_score_help'] = 'Coeficientii de similitudine reprezinta procentul din documentul analizat care a fost detectat ca fiind similar cu alt conținut.';
$string['strike_addref_method'] = 'Adaugă metodă de referință.';
$string['strike_addref_method_help'] = 'Definește modul în care sunt adăugate în baza de date documentele verificate cu StrikePlagiarism.com.<br/>
Este recomandat să selectați AUTOMATIC, astfel încât documentele să fie adăugate automat în baza de date.';
$string['strike_deletereport_method'] = 'Ștergere Raport Similitudine.';
$string['strike_deletereport_method_help'] = 'Definește modul în care sunt șterse rapoartele de similitudine atunci când un document este șters sau actualizat.<br/>
Metodele disponibile sunt:
<ul>
<li>keep: Păstrează rapoarte de similitudine vechi</li>
<li>remove: Șterge rapoarte de similitudine vechi</li>
</ul>';
$string['strike_studentemail'] = 'Trimite e-mail studentului';
$string['strike_studentemail_help'] = 'Se trimite un e-mail studentului atunci când un document este procesat, pentru a-l informa că rapoartul este disponibil, e-mailul include și opțiunea de a opri notificări.';
$string['strike_draft_submit'] = 'Când trebuie să fie încărcat fișierul';
$string['strike_defaultlang'] = 'Limbă document predefinită';
$string['strike_defaultlang_help'] = 'Este limba presetată de transmitere a documentelor către StrikePlagiarism.com. Este un parametru obligatoriu pentru API-ul lor, însă Moodle nu recunoaște această information';
$string['wordcount'] = 'Număr minim cuvinte';
$string['wordcount_help'] = 'Setează o limită minimă de cuvinte necesare pentru textul inserat online (postări în forum și teme online) înainte ca textul să fie trimis către StrikePlagiarism.com.';

$string['addrefauto'] = 'Adăugarea automată în baza de date a documentelor verificate';
$string['addrefmanual'] = 'Adăugarea manuală în baza de date a documentelor verificate';

$string['deletereportkeep'] = 'Păstrează rapoarte de similitudine vechi pentru fișiere șterse/actualizate';
$string['deletereportremove'] = 'Șterge rapoarte de similitudine vechi pentru fișiere șterse/actualizate';

$string['submitondraft'] = 'Trimite fișier când este încărcat';
$string['submitonfinal'] = 'Trimite fișier când studentul îl trimite la evaluare';
$string['showwhenclosed'] = 'Când Activitatea este închisă';

$string['pending'] = 'Fișierul așteaptă trimitere către StrikePlagiarism.com';
$string['processing'] = 'Fișierul a fost trimis către StrikePlagiarism.com and și așteaptă finalizarea analizei';
$string['toolarge'] = 'Fișier prea mare pentru a fi transmis către StrikePlagiarism.com';
$string['unsupported'] = 'Formatul fișierului nu este acceptat de StrikePlagiarism.com';
$string['unsupportedfiletype'] = 'Formatul fișierului nu este acceptat de StrikePlagiarism.com';
$string['unknownwarning'] = 'A apărut o eroare în trimiterea acestui fișier către StrikePlagiarism.com';
$string['awaitanalysischoice'] = 'Așteaptă trimitere către StrikePlagiarism.com pentru analiză';
$string['previouslysubmitted'] = 'Încărcat anterior ca';
$string['sendforanalysis'] = 'Adaugă fișier în StrikePlagiarism.com pentru analiză';
$string['indexdocument'] = 'Adaugă fișierul în baza de date a StrikePlagiarism.com';
$string['deletestrikefile'] = 'Șterge din baza de date a StrikePlagiarism.com';

$string['similarity1'] = 'Coeficient Similitudine 1';
$string['similarity2'] = 'Coeficient Similitudine 2';
$string['similarity3'] = 'Coeficient Similitudine 3';
$string['similarity4'] = 'Coeficient Similitudine 4';
$string['similarity5'] = 'Coeficient Similitudine 5';

$string['filereset'] = 'Un fișier a fost resetat pentru retransmitere către StrikePlagiarism.com';
$string['fileresetfailed'] = 'Eroare: fișierul nu a fost resetat pentru retransmitere către StrikePlagiarism.com';

$string['report'] = 'Raportul StrikePlagiarism ';
$string['reportclick'] = 'Link către raportul StrikePlagiarism';

$string['restrictcontent'] = 'Transmiteți fișierele atașate și textul inserat online';
$string['restrictcontent_help'] = 'StrikePlagiarism.com poate procesa fișierele încarcate și textul inserat online în postările în forum și cel din temele online. Puteți decide ce componente să transmiteți către StrikePlagiarism.com.';
$string['restrictcontentfiles'] = 'Trimiteți numai fișierele atașate';
$string['restrictcontentno'] = 'Trimiteți totul';
$string['restrictcontenttext'] = 'Trimiteți numai textul inserat online';

$string['config:plagiarism'] = 'Valorile globalee de configurarea a pluginului antiplagiat';
$string['config:defaults'] = 'Configurarea presetată a modulului antiplagiat StrikePlagiarism.com';

$string['studentemailsubject'] = ' Fișierul a fost procesat de StrikePlagiarism.com';
$string['studentemailcontent'] = '<p>Dear {$a->firstname} {$a->lastname},</p>
Fișierul transmis către {$a->modulename} în {$a->coursename} a fost procesat de StrikePlagiarism.com.<br/>
{$a->modulelink}<br/>
Cu stimă,
{$a->admin}
';

$string['label:user'] = 'Utilizator';
$string['label:author'] = 'Autor';
$string['label:timesubmitted'] = 'Încărcat la';
$string['label:analysisstatus'] = 'Status analiză';
$string['label:reportstatus'] = 'Status raport';
$string['label:status'] = 'Status';
$string['label:filename'] = 'Nume fișier';
$string['label:modname'] = 'Modul';
$string['label:actions'] = 'Acțiune(i)';

$string['report:na'] = 'N/A';
$string['report:ready'] = 'Gata';
$string['report:available'] = 'Disponibil';
$string['analysis:auto'] = 'automat';
$string['analysis:manual'] = 'manual';
$string['analysis:awaitchoice'] = 'așteaptă alegere';
$string['cmoverview'] = 'Status încărcare modul curs Strike';
$string['notification:subject'] = 'Mesaj StrikePlagiarism.com [{$a}]';
$string['notification:errormessage'] = '<p>Stimat(ă) {$a->fullname}<p>
<p>A apărut o eroare la pluginul antiplagiat StrikePlagiarism.com în \'{$a->instancename}\' în cursul \'{$a->coursefullname}\'.<br/>
Eroarea apărută: {$a->errormessage}.</p>
<p>Mai jos sunt detaliile fișierului:<br/>
Autor: {$a->strikefileauthor}<br/>
Coordonator: {$a->strikefilecoordinator}<br/>
Titlu: {$a->strikefiletitle}<br/>
Nume fișier: {$a->strikefilename}<br/>
GUID: {$a->strikefileguid}<br/>
Creat la: {$a->strikefiletimecreated}
</p>
<p>Mergeți la <a href="{$a->contexturl}">{$a->contexturlname}</a> pentru mai multe detalii</p>
<p>Cu stimă,<br/>{$a->signoff}</p>';
$string['messageprovider:strikenotification'] = 'Eroare Notificare StrikePlagiarism.com';

$string['err:strike_selectfiletypes-empty'] = 'Vă rugăm să selectați formatele de fișiere care pot fi încărcate.<br/>
Dacă nu sunt selectate formatele acceptate, nu se vor putea trimite documente către API-ul antiplagiat.';
$string['warn:strike_selectfiletypes-empty'] = 'Vă rugăm să selectați formatele de fișiere care pot fi încărcate.<br/>
Dacă nu sunt selectate formatele acceptate, nu se vor putea trimite documente către API-ul antiplagiat.';
