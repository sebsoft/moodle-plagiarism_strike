<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language file for plagiarism_strike, EN
 *
 * File         plagiarism_strike.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// General.
$string['pluginname'] = 'Plagiat.pl';
$string['strike'] = 'Plagiat.pl wtyczka';
$string['strikeplagiarism'] = 'Antyplagiat';
$string['strikeexplain'] = 'Aby uzyskać więcej informacji na temat systemu Antyplagiatowego oraz jego działnia odzwiedź: <a href="http://www.plagiat.pl">www.plagiat.pl</a>';

$string['task:syncreport'] = 'Synchronizuj raport Plagiat.pl do dokumentu';
$string['task:getreports'] = 'Synchronizuj raporty Plagiat.pl ';
$string['task:sendfiles'] = 'Wyslij plik Plagiat.pl do API';

$string['usestrike'] = 'Użyj wtyczki Plagiat.pl';
$string['useadminnotifications'] = 'Powiadomić Administratora o błędach?';
$string['useadminnotifications_help'] = 'Powiadamia administratora w przypadku wystąpienia różnych błędów (na przykład podczas komunikacji z interfejsem API Plagiat.pl)';
$string['useteachernotifications'] = 'Powiadomić wykładowcę o blędach?';
$string['useteachernotifications_help'] = 'Powiadom wykładowcę w przypadku wystąpienia róznych bledów (na przykład podczas komunikacji z API  Plagiat.pl)';
$string['strikekey'] = 'Plagiat klucz API';
$string['strikekey_help'] = 'Klucz API Plagiat.pl <br/> Aby użyć tej wtyczki musisz uzyskać klucz indentyfikacyjny ID aby uzyskać więcej informacji proszę skontaktować się za pomocą formularza kontaktowego na stronie <a href="http://www.plagiat.pl">www.plagiat.pl</a>';
$string['strikeserver'] = 'Plagiat.pl host API';
$string['strikeserver_help'] = 'Nazwa serwera Plagiat.pl lub adres IP. Wprowadź adres IP Plagiat.pl z http lub https';
$string['studentdisclosuredefault'] = 'Wyrażam zgodę, aby wszystkie przesłane przeze mnie pliki zostały przekazane do internetowego systemu antyplagiatowego Plagiat.pl w celu sprawdzenia oryginalności tekstu.';
$string['strikestudentdisclosure'] = 'Wyrażam zgodę, aby wszystkie przesłane przeze mnie pliki zostały przekazane do internetowego systemu antyplagiatowego Plagiat.pl w celu sprawdzenia oryginalności tekstu.';
$string['studentdisclosure'] = 'Informacje o studencie';
$string['studentdisclosure_help'] = 'Ten tekst będzie wyświetlany wszystkim studetom na stronie przesyłania plików.';
$string['strike_enableplugin'] = 'Włącz plagiat.pl do {$a}';
$string['notifyteachers'] = 'Powiadom Wykładowcę';
$string['enableapidebugging'] = 'Włącz debugowanie API';
$string['enableapidebugging_help'] = 'Włącz tę opcję jeśli chcesz uzyskać dostęp do rozszerzone debugowania API.<br/>
. Dostępnych jest wiele metod debugowania <i>tylko</i> skaładających się z komunikacji/ruchu na API Plagiat.pl.
';
$string['apidebuggingmtrace'] = 'Włączyć debugowanie mtrace?';
$string['apidebuggingmtrace_help'] = 'Włącz tę opcję, jeśli chcesz zoabaczyć wyjście degugowania API jako mtrace() output.<br/>
Jeśli właczysz tę opcję, opuszczenie  będzie widoczne w tle zadania (CRON).
';
$string['apidebuggingerrorlog'] = 'Włączyć error_log debugowania?';
$string['apidebuggingerrorlog_help'] = '\Włącz tę opcję aby wyświetlić wyjscie debugowania protokołu API jako error_log output.<br/>
Jesli włączysz tę opcję, wyjście śledzenia będzie widoczne w webserwerze / php error_log.<br/>
Uwaga: Opcja będzie widoczna w zależności od twojej konfiguracji webservera lub PHP.
';
$string['apidebuggingemail'] = 'Włącz debugowanie poczty e-mail?';
$string['apidebuggingemail_help'] = 'Włącz tę opcję, jeżeli chcesz otrzymywać wynik debugowania API w wiadomośći e-mail.<br/>
jeśli opcja zostanie włączona, wyjście śledzenia zostanie wysłane na wskazany adres e-mail.<br/>
Uwaga: Może spowodować wysłanie wielu e-maili.<br/>
Uwaga 2: Ta metoda wykorzystuje domyślną funkcję skrzynki mailowej() , i nie bedzie zawsze dostępna. Działa poza fukcjami poczty Moodle.
';
$string['apidebuggingemailaddress'] = 'Adresat debugowania adresu e-mail.';

$string['str:settings'] = 'Ustawienia Plagiat.pl';
$string['str:defaults'] = 'domyślne Plagiat.pl';
$string['str:dump'] = 'zrzut zawartośći';
$string['str:debug'] = 'Debugowanie';
$string['allowallsupportedfiles'] = 'Zezwalaj na wszystkie obsługiwane typy plików';
$string['allowallsupportedfiles_help'] = 'Pozwala wykładowcy na ograniczenie, które typy plików będą wysyłane do serwisu Plagiat.pl w celu przetworzenia. Nie zapobiega przesyłaniu przez studentów różnych typów plików do Plagiat.pl';
$string['restrictfiles'] = 'Typy plików do wysłania';
$string['form:desc:defaults'] = 'Poniżej możesz wprowadzić wszystkie ustawienia domyślne, które będą używane przy tworzeniu nowego modułu aktywności który należy skonfigurować.<br/>
Podane  wartośći słuzą jako domyślne wartośći konfiguracji Plagiat.pl dla tego modułu aktywnośći.';
$string['docsendmode'] = 'Wysyłanie dokumentów';
$string['docsendmode_help'] = 'Wybierz metodę wysyłania dokumentów do analizy. Możliwe wysłanie konkretnych dokumentów do analizy lub wszytskie - wybór domyślny';
$string['docsendingchoose'] = 'Wybierz pliki które mają zostać wysłane do analizy';
$string['docsendingall'] = 'wyslij wszystkie pliki do analizy ';
$string['strike_show_student_status'] = 'Pokaż status analizy studentowi ';
$string['strike_show_student_status_help'] = 'Status umożliwianie sprawdzanie studentom stanu zgłoszeń.';
$string['strike_show_student_report'] = 'Pokaż raport podobieństwa studentowi';
$string['strike_show_student_report_help'] = 'Raport podobieństwa wskazuje sprecyzowane fragmenty tekstu które zostały zapożyczone z danego zródła poprzez analizę systemu Plagiat.pl';
$string['strike_show_student_score'] = 'Pokaż wynik raportu podobieństwa studentowi';
$string['strike_show_student_score_help'] = 'Raport podobieństwa wskazuje procentowy udział zapożyczeń w kazdym z 5 współczynników podobieństwa.';
$string['strike_addref_method'] = 'Dodaj referencyjną metodę.';
$string['strike_addref_method_help'] = 'Określa w jaki sposób dokumenty sprawdzane przez Plagiat.pl są dodawane do bazy referencyjnych .<br/>
Zalecamy usatwić tryb automatyczny, pliki bedą automatycznie dodawane do bazy.';
$string['strike_deletereport_method'] = 'Usunięcie Raportu Podobieństwa.';
$string['strike_deletereport_method_help'] = 'Określa sposób w jaki raporty podobieństwa zostają usuwane przy kazdej zmianie lub aktualizacji zgłoszenia.<br/>
Możliwe metody to:
<ul>
<li>keep: Zachowanie poprzednich raportów podobieństwa</li>
<li>remove: Usunięcie poprzednich raportów podobieństwa</li>
</ul>';
$string['strike_studentemail'] = 'Wyslij email studenta';
$string['strike_studentemail_help'] = 'Zostanie wysłana wiadomość e-mail do studenta, informująca o wygenerowaniu raportu.';
$string['strike_draft_submit'] = 'Termin w którym należy złożyć plik';
$string['strike_defaultlang'] = 'Domyślny język dokumentu';
$string['strike_defaultlang_help'] = 'Jest to domyślny język dokumentów używany do wysyłania dokumentów do serwisu Plagiat.pl Jest to obowiązkowy parametr API, jednakże Moodle nie ma dostępu do tego typu danych';
$string['wordcount'] = 'Minimalna liczba znaków';
$string['wordcount_help'] = 'Okresla minimalny limit znaków wymagany w tekstach, przed wysłaniem zawartości do serwisu Plagiat.pl';

$string['addrefauto'] = 'Po sprawdzeniu automatycznie dodawaj dane dokumentu ';
$string['addrefmanual'] = 'Po sprawdzeniu dodawaj ręcznie dane dokumentu ';

$string['deletereportkeep'] = 'Zachowaj stare raporty podobieństwa dla poprzednich zgłoszeń';
$string['deletereportremove'] = 'Usuń stare raporty podobieństwa dla poprzednich sprawdzeń';

$string['submitondraft'] = 'Zatwierdź plik przed wysłaniem';
$string['submitonfinal'] = 'Zatwierdź plik gdy student przesyła go do sprawdzenia';
$string['showwhenclosed'] = 'Gdy aktywność została zamknięta';

$string['pending'] = 'Plik oczekuje na dodanie do Plagiat.pl';
$string['processing'] = 'Plik został przesłany do Plagiat.pl oczekuje na zakończenie analizy';
$string['toolarge'] = 'Plik jest zbyt duży aby przesłać go do Plagiat.pl';
$string['unsupported'] = 'Typ pliku nie jest wspierany przez Plagiat.pl';
$string['unsupportedfiletype'] = 'Ten rodzaj pliku nie jest obsługiwany przez Plagiat.pl';
$string['unknownwarning'] = 'Wystąpił błąd podczas przesyłnia pliku do Plagiat.pl';
$string['awaitanalysischoice'] = 'Oczekuje działania, aby poddać analizie Plagiat.pl';
$string['previouslysubmitted'] = 'Poprzednio zgłoszono jako';
$string['sendforanalysis'] = 'Dodaj plik do Plagiat.pl w celu jego analizy';
$string['indexdocument'] = 'Obejmij plik wysyłką do bazy danych Plagiat.pl';
$string['deletestrikefile'] = 'Usuń lokalny odnośnik pliku w Plagiat.pl';

$string['similarity1'] = 'Współczynnik Podobieństwa 1';
$string['similarity2'] = 'Współczynnik Podobieństwa 2';
$string['similarity3'] = 'Współczynnik Podobieństwa 3';
$string['similarity4'] = 'Współczynnik Podobieństwa 4';
$string['similarity5'] = 'Współczynnik Podobieństwa 5';

$string['filereset'] = 'Plik został zresetowany w celu ponownego przesłania do serwisu Plagiat.pl';
$string['fileresetfailed'] = 'Błąd: plik nie został prawidłowo zresetowany';

$string['report'] = 'raport Plagiat.pl';
$string['reportclick'] = 'przejdź do raportu Plagiat.pl';

$string['restrictcontent'] = 'prześlij załączony plik i tekst';
$string['restrictcontent_help'] = 'System Plagiat.pl może przetwarzać przesłane pliki, teksty online a także wiersze z postów na forum, Możesz zdecydowąć które składniki bedą wysłane';
$string['restrictcontentfiles'] = 'prześlij tylko załączony plik';
$string['restrictcontentno'] = 'prześlij wszystko';
$string['restrictcontenttext'] = 'Only submit in-line text';

$string['config:plagiarism'] = 'Łączne wartości konfiguracji Plagiat.pl';
$string['config:defaults'] = 'domyślne ustawienia modułu Plagiat.pl';

$string['studentemailsubject'] = 'Wniosek przetworzony przez Plagiat.pl';
$string['studentemailcontent'] = '<p>Szanowny/a {$a->Imię} {$a->Nazwisko},</p>
Przesłany plik {$a->nazwa modułu} in {$a->nazwa przedmiotu} został przetworzony przez Plagiat.pl<br/>
{$a->modulelink}<br/>
Z poważaniem,
{$a->admin}
';

$string['label:user'] = 'Użytkownik';
$string['label:author'] = 'Autor';
$string['label:timesubmitted'] = 'przedłożone w dniu';
$string['label:analysisstatus'] = 'status analizy';
$string['label:reportstatus'] = 'status raportu';
$string['label:status'] = 'Status';
$string['label:filename'] = 'nazwa pliku';
$string['label:modname'] = 'Moduł';
$string['label:actions'] = 'Akcja';

$string['report:na'] = 'N/A';
$string['report:ready'] = 'Gotowy';
$string['report:available'] = 'Dostępny';
$string['analysis:auto'] = 'Analiza automatyczna';
$string['analysis:manual'] = 'Analiza ręczna';
$string['analysis:awaitchoice'] = 'czeka na wybór';
$string['cmoverview'] = 'status modułu kursu Plagiat.pl';
$string['notification:subject'] = 'Wiadomość Plagiat.pl [{$a}]';
$string['notification:errormessage'] = '<p>Drogi/a {$a->Nazwisko}<p>
<p>An error occured in the StrikePlagiarism.com plagiarism plugin in \'{$a->instancename}\' in course \'{$a->coursefullname}\'.<br/>
The error raised was: {$a->errormessage}.</p>
<p>Below are the file details:<br/>
Author: {$a->strikefileauthor}<br/>
Coordinator: {$a->strikefilecoordinator}<br/>
Title: {$a->strikefiletitle}<br/>
Filename: {$a->strikefilename}<br/>
GUID: {$a->strikefileguid}<br/>
Time created: {$a->strikefiletimecreated}
</p>
<p>Please go to <a href="{$a->contexturl}">{$a->contexturlname}</a> for more details</p>
<p>Regards,<br/>{$a->signoff}</p>';
$string['messageprovider:strikenotification'] = 'Plagiat.pl powiadomienie o błędzie';

$string['err:strike_selectfiletypes-empty'] = 'Wybierz rodzaje dokumentów, które będą poddawanych analizie antyplagiatowej.<br/>
Jeśli nie zostanie wybrany żaden rodzaj dokumentów, dokumenty nie będą wysyłane do sprawdzenia w systemie antyplagiatowym.';
$string['warn:strike_selectfiletypes-empty'] = 'Wybierz rodzaje dokumentów, które będą poddawanych analizie antyplagiatowej.<br/>
Jeśli nie zostanie wybrany żaden rodzaj dokumentów, dokumenty nie będą wysyłane do sprawdzenia w systemie antyplagiatowym.';

$string['test_api_connection'] = 'Testuj połączenie z API';
$string['test_api_connection_status'] = 'Status połączenia z API';
$string['test_api_connection_help'] = 'Możesz sprawdzić czy wprowadzone host i klucz API są poprawne i czy można połączyć się z systemem antyplagiatowym.';