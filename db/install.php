<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Install information for plagiarism_strike
 *
 * File         install.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

/**
 * Install script.
 */
function xmldb_plagiarism_strike_install() {
    global $DB, $CFG;
    // Turning on Plagiarism in Moodle.
    if (!$DB->record_exists('config', array('name' => 'enableplagiarism'))) {
        $enabledata = new stdClass();
        $enabledata->name = 'enableplagiarism';
        $enabledata->value = 1;
        $DB->insert_record('config', $enabledata);
    } else if ($enabled = $DB->get_record('config', array('name' => 'enableplagiarism'))) {
        if ($enabled->value == 0) {
            $enabled->value = 1;
            $DB->update_record('config', $enabled);
        }
    }

    // Enabling default settings.
    $insertdata = array(
        "strike_use" => 1,
        "strike_server" => "https://lmsapi.plagiat.pl",
        "strike_key" => "",
        "strike_enableapidebugging" => 0,
        "strike_debuggingmtrace" => 0,
        "strike_debuggingerrorlog" => 0,
        "strike_debuggingemail" => 0,
        "strike_defaultlang" => "en-US",
        "strike_wordcount" => 50,
        "strike_useadminnotifications" => 1,
        "strike_enable_mod_assign" => 1,
        "strike_enable_mod_forum" => 1,
        "strike_enable_mod_workshop" => 1,
        "strike_after_install" => 0
    );

    foreach ($insertdata as $name => $value) {
        if (!$DB->record_exists('config_plugins', ['name' => $name, 'plugin' => 'plagiarism'])) {
            $insertdata = new stdClass();
            $insertdata->plugin = 'plagiarism';
            $insertdata->name = $name;
            $insertdata->value = $value;
            $DB->insert_record('config_plugins', $insertdata);
        }
    }

    $insertdata = array(
        ['cm' => null, 'name' => 'strike_use_strike',
            'value' => 1, 'config_hash' => '_strike_use_strike'],
        ['cm' => null, 'name' => 'strike_useadminnotifications',
            'value' => 0, 'config_hash' => '_strike_useadminnotifications'],
        ['cm' => null, 'name' => 'strike_useteachernotifications',
            'value' => 0, 'config_hash' => '_strike_useteachernotifications'],
        ['cm' => null, 'name' => 'strike_allowallfile',
            'value' => 1, 'config_hash' => '_strike_allowallfile'],
        ['cm' => null, 'name' => 'strike_docsendmode',
            'value' => 0, 'config_hash' => '_strike_docsendmode'],
        ['cm' => null, 'name' => 'strike_addref_method',
            'value' => 'auto', 'config_hash' => '_strike_addref_method'],
        ['cm' => null, 'name' => 'strike_deletereport_method',
            'value' => 'keep', 'config_hash' => '_strike_deletereport_method'],
        ['cm' => null, 'name' => 'strike_show_student_status',
            'value' => 0, 'config_hash' => '_strike_show_student_status'],
        ['cm' => null, 'name' => 'strike_show_student_score',
            'value' => 0, 'config_hash' => '_strike_show_student_score'],
        ['cm' => null, 'name' => 'strike_show_student_report',
            'value' => 0, 'config_hash' => '_strike_show_student_report'],
        ['cm' => null, 'name' => 'strike_studentemail',
            'value' => 0, 'config_hash' => '_strike_studentemail'],
        ['cm' => null, 'name' => 'strike_restrictcontent',
            'value' => 0, 'config_hash' => '_strike_restrictcontent']
    );

    foreach ($insertdata as $array) {
        $object = (object) $array;
        if (!$DB->record_exists('plagiarism_strike_config', ['name' => $object->name, 'config_hash' => $object->config_hash])) {
            $DB->insert_record('plagiarism_strike_config', $object);
        }
    }
}
