<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Upgrade information for plagiarism_strike
 *
 * File         upgrade.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

/**
 * Upgrade script.
 *
 * @param int $oldversion
 * @return boolean
 */
function xmldb_plagiarism_strike_upgrade($oldversion = 0) {
    global $DB;
    $dbman = $DB->get_manager();

    // Perform upgrade tasks.
    if ($oldversion < 2017103002) {
        // Włączam plagiarism.
        if (!$DB->record_exists('config', array('name' => 'enableplagiarism'))) {
            $enabledata = new stdClass();
            $enabledata->name = 'enableplagiarism';
            $enabledata->value = 1;
            $DB->insert_record('config', $enabledata);
        } else if ($enabled = $DB->get_record('config', array('name' => 'enableplagiarism'))) {
            if ($enabled->value == 0) {
                $enabled->value = 1;
                $DB->update_record('config', $enabled);
            }
        }
    }

    return true;
}
