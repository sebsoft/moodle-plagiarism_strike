<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Uninstall information for plagiarism_strike
 *
 * File         uninstall.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

/**
 * Uninstall script.
 */
function xmldb_plagiarism_strike_uninstall() {
    global $DB;

    $deletenames = array(
        "strike_use",
        "strike_server",
        "strike_key",
        "strike_enableapidebugging",
        "strike_debuggingmtrace",
        "strike_debuggingerrorlog",
        "strike_debuggingemail",
        "strike_defaultlang",
        "strike_wordcount",
        "strike_useadminnotifications",
        "strike_enable_mod_assign",
        "strike_enable_mod_forum",
        "strike_enable_mod_workshop",
        "strike_after_install",
    );

    foreach ($deletenames as $name) {
        $DB->delete_records('config_plugins', array('name' => $name, 'plugin' => 'plagiarism'));
    }

    // Perform uninstall tasks.
}
