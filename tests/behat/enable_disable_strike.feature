@plugin @plagiarism_Strike
Feature: Enable Strike
  In order to enable/disable plagiarism features
  As an Admin
  I need to be able to enable/disable the Strike plugin

  Background:
    Given I log in as "admin"
    And I navigate to "Advanced features" node in "Site administration"
    And I set the field "Enable plagiarism plugins" to "1"
    And I press "Save changes"

  @javascript
  Scenario: Enable Strike
    Given I navigate to "STRIKE plagiarism plugin" node in "Site administration>Plugins>Plagiarism"
    When I set the field "Enable STRIKE" to "1"
    And I set the field "Username" to "1"
    And I set the field "Password" to "1"
    And I set the field "Enable STRIKE for assign" to "1"
    And I set the field "Enable STRIKE for forum" to "1"
    And I set the field "Enable STRIKE for workshop" to "1"
    And I press "Save changes"
    Then the field "Enable STRIKE" matches value "1"
    And the field "Enable STRIKE for assign" matches value "1"
    And the field "Enable STRIKE for forum" matches value "1"
    And the field "Enable STRIKE for workshop" matches value "1"

  @javascript
  Scenario: Disable STRIKE
    Given I navigate to "STRIKE plagiarism plugin" node in "Site administration>Plugins>Plagiarism"
    When I set the field "Enable STRIKE" to "0"
    And I set the field "Enable STRIKE for assign" to "0"
    And I set the field "Enable STRIKE for forum" to "0"
    And I set the field "Enable STRIKE for workshop" to "0"
    And I press "Save changes"
    Then the field "Enable STRIKE" matches value "0"
    And the field "Enable STRIKE for assign" matches value "0"
    And the field "Enable STRIKE for forum" matches value "0"
    And the field "Enable STRIKE for workshop" matches value "0"