<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course module analysis overview.
 *
 * File         cmoverview.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__.'/../../config.php');
require_once($CFG->dirroot . '/plagiarism/strike/lib.php');

$cmid = required_param('cm', PARAM_INT);
$action = optional_param('action', null, PARAM_ALPHANUMEXT);

list($course, $cm) = get_course_and_cm_from_cmid($cmid);
require_login($course, true, $cm);

$strreport = get_string('cmoverview', 'plagiarism_strike');
$pageurl = new moodle_url($CFG->wwwroot . '/plagiarism/strike/cmoverview.php', array('cm' => $cmid));

$PAGE->set_pagelayout('report');
$PAGE->set_url($pageurl);
$PAGE->set_title($strreport);
$PAGE->set_heading($SITE->fullname);
$PAGE->navbar->add($strreport);

switch ($action) {
    case 'delete':
        $id = required_param('id', PARAM_INT);
        $sf = \plagiarism_strike\strikefile::get_by_id($id);
        $sf->delete();
        redirect($pageurl);
        break;

    case 'indexdocument':
        $id = required_param('id', PARAM_INT);
        $sf = \plagiarism_strike\strikefile::get_by_id($id);
        \plagiarism_strike\strikeplagiarism::index_document($sf);
        redirect($pageurl);
        break;

    case 'analysedocument':
        $id = required_param('id', PARAM_INT);
        $sf = \plagiarism_strike\strikefile::get_by_id($id);
        $sf->doanalysis = 1;
        $sf->statuscode = PLAGIARISM_STRIKE_STATUS_PENDING;
        $sf->save();
        redirect($pageurl);
        break;

    case 'resetdocument':
        $id = required_param('id', PARAM_INT);
        $sf = \plagiarism_strike\strikefile::get_by_id($id);
        \plagiarism_strike\strikeplagiarism::reset_file($sf);
        redirect($pageurl);
        break;

    default:
        $table = new \plagiarism_strike\tables\strikefiles($cm);
        $table->baseurl = $PAGE->url;

        echo $OUTPUT->header();
        echo $OUTPUT->heading($strreport);
        echo $table->render(25);
        echo $OUTPUT->footer();
}