<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Script to configure default settings for plagiarism_strike
 *
 * File         strikedefaults.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__.'/../../config.php');

require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/plagiarismlib.php');
require_once($CFG->dirroot.'/plagiarism/strike/lib.php');

require_login();
$pageurl = new moodle_url($CFG->wwwroot . '/plagiarism/strike/strike_defaults.php');
admin_externalpage_setup('plagiarismstrike', '', null, $pageurl);

$context = context_system::instance();
require_capability('moodle/site:config', $context, $USER->id, true, 'nopermissions');

$mform = new \plagiarism_strike\forms\defaults($PAGE->url);
$plagiarismplugin = new plagiarism_plugin_strike();
$renderer = $PAGE->get_renderer('plagiarism_strike');
$config = new plagiarism_strike\config(null);

if ($mform->is_cancelled()) {
    redirect($pageurl);
} else if ($data = $mform->get_data()) {
    // Insert or update.
    foreach ($data as $name => $value) {
        if (stripos($name, 'strike_') !== 0) {
            continue;
        }
        if (is_array($value)) {
            $value = implode(',', $value);
        }
        $config->set($name, $value, true);
    }
    redirect($pageurl);
}

echo $OUTPUT->header();
echo $renderer->get_admin_tabs('strikedefaults');
$mform->set_data($config->get_configdefault());
$mform->display();
echo $OUTPUT->footer();