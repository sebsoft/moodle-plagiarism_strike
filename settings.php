<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Settings information for plagiarism_strike
 *
 * File         settings.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__.'/../../config.php');

require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/plagiarismlib.php');
require_once($CFG->dirroot.'/plagiarism/strike/lib.php');

require_login();
admin_externalpage_setup('plagiarismstrike');

$context = context_system::instance();
require_capability('moodle/site:config', $context, $USER->id, true, "nopermissions");

$mform = new \plagiarism_strike\forms\setup();
$plagiarismplugin = new plagiarism_plugin_strike();

if ($mform->is_cancelled()) {
    redirect('/');
} else if ($data = $mform->get_data()) {
    if (!isset($data->strike_use)) {
        $data->strike_use = 0;
    }
    if (!isset($data->strike_enable_mod_assign)) {
        $data->strike_enable_mod_assign = 0;
    }
    if (!isset($data->strike_enable_mod_forum)) {
        $data->strike_enable_mod_forum = 0;
    }
    if (!isset($data->strike_enable_mod_workshop)) {
        $data->strike_enable_mod_workshop = 0;
    }

    foreach ($data as $field => $value) {
        if (strpos($field, 'strike') === 0) {
            $value = trim($value);
            if ($field == 'strike_server') { // Strip trailing slash from api.
                $value = rtrim($value, '/');
            }
            if ($configfield = $DB->get_record('config_plugins', array('name' => $field, 'plugin' => 'plagiarism'))) {
                $configfield->value = $value;
                if (! $DB->update_record('config_plugins', $configfield)) {
                    error("errorupdating");
                }
            } else {
                $configfield = new stdClass();
                $configfield->value = $value;
                $configfield->plugin = 'plagiarism';
                $configfield->name = $field;
                if (! $DB->insert_record('config_plugins', $configfield)) {
                    error("errorinserting");
                }
            }
        }
    }

    // We MUST invalidate cache here or the configplagiarism class will use OLD data.
    cache_helper::invalidate_by_definition('core', 'config', array(), 'plagiarism');

    redirect($PAGE->url);
}

$renderer = $PAGE->get_renderer('plagiarism_strike');
$mform->set_data(\plagiarism_strike\configplagiarism::as_object());

echo $OUTPUT->header();
echo $renderer->get_admin_tabs('strikesettings');
echo $OUTPUT->box_start('generalbox boxaligncenter', 'intro');
$mform->display();
echo $OUTPUT->box_end();
echo $OUTPUT->footer();