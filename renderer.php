<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer for plagiarism_strike
 *
 * File         renderer.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * plagiarism_strike_renderer
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class plagiarism_strike_renderer extends plugin_renderer_base {

    /**
     * Create a tab object with a nice image view, instead of just a regular tabobject
     *
     * @param string $id unique id of the tab in this tree, it is used to find selected and/or inactive tabs
     * @param string $pix image name
     * @param string $pixcomponent component where the image will be looked for
     * @param string|moodle_url $link
     * @param string $text text on the tab
     * @param string $title title under the link, by defaul equals to text
     * @param bool $linkedwhenselected whether to display a link under the tab name when it's selected
     * @return \tabobject
     */
    protected function create_pictab($id, $pix = null, $pixcomponent = null,
            $link = null, $text = '', $title = '', $linkedwhenselected = false) {
        global $CFG;
        $img = '';
        if ($pix !== null) {
            if ($CFG->branch >= 33) {
                $img = $this->image_url($pix, $pixcomponent);
            } else {
                $img = $this->pix_url($pix, $pixcomponent);
            }
            $img = \html_writer::img($img, empty($title) ? $text : $title);
        }
        return new \tabobject($id, $link, $img . $text, empty($title) ? $text : $title, $linkedwhenselected);
    }

    /**
     * Render administration tabs.
     * @param string $selected selected tab
     * @param array $params
     * @return string
     */
    public function get_admin_tabs($selected, $params = array()) {
        unset($params['action']);
        $tabs = array();
        $settings = $this->create_pictab('strikesettings', 'i/settings', '',
                new \moodle_url('/plagiarism/strike/settings.php', $params),
                get_string('str:settings', 'plagiarism_strike'));
        $tabs[] = $settings;
        $defaults = $this->create_pictab('strikedefaults', null, 'plagiarism_strike',
                new \moodle_url('/plagiarism/strike/strike_defaults.php', $params),
                get_string('str:defaults', 'plagiarism_strike'));
        $tabs[] = $defaults;
        $debug = $this->create_pictab('strikedebug', null, 'plagiarism_strike',
                new \moodle_url('/plagiarism/strike/strike_debug.php', $params),
                get_string('str:debug', 'plagiarism_strike'));
        $tabs[] = $debug;

        return $this->tabtree($tabs, $selected);
    }

    /**
     * render a ranking
     * @param \plagiarism_strike\renderable\ranking $ranking
     * @return string
     */
    public function render_ranking(\plagiarism_strike\renderable\ranking $ranking) {
        $rank = "none";
        if ($ranking->score > 90) {
            $rank = "1";
        } else if ($ranking->score > 80) {
            $rank = "2";
        } else if ($ranking->score > 70) {
            $rank = "3";
        } else if ($ranking->score > 60) {
            $rank = "4";
        } else if ($ranking->score > 50) {
            $rank = "5";
        } else if ($ranking->score > 40) {
            $rank = "6";
        } else if ($ranking->score > 30) {
            $rank = "7";
        } else if ($ranking->score > 20) {
            $rank = "8";
        } else if ($ranking->score > 10) {
            $rank = "9";
        } else if ($ranking->score >= 0) {
            $rank = "10";
        }

        return '<span class="rank' . $rank . '">' . $ranking->score . '%</span>';
    }

}
