// Standard license block omitted.
/*
 * @package    plagiarism_strike
 * @copyright  2018 INTERSIEC Kamil Łuczak
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
define(['jquery'], function($) {
    function test_api_status(server){
        $('.strike_api_status').html('<img src="' + server + '/pix/i/loading_small.gif" />');
        var response = '';
        var key = $('#id_strike_key').val();
        var host = $('#id_strike_server').val();
        var request = $.ajax({
            url: server + "/plagiarism/strike/test_connection.php",
            method: "GET",
            data: { key: key, server: host },
            dataType: "json"
        });
        request.done(function( msg ) {
            response = msg.statuscode + ": " + msg.errorresponse;
            if(msg.statuscode === 200){
                response = '<b style="color: green;">' + response + '</b>';
            }
            else if(msg.statuscode === 403){
                response = '<b style="color: orangered;">' + response + '</b>';
            }
            else if( (msg.statuscode !== 403) && (msg.statuscode > 200) ){
                response = '<b style="color: red;">' + response + '</b>';
            } else {
                response = '<b style="color: red;">' + response + '</b>';
            }

            $('.strike_api_status').html(response);

        });
        request.fail(function( jqXHR, textStatus ) {
            $('.strike_api_status').html(textStatus);
        });
    }

    return {
        init: function(server) {
            test_api_status(server);
            $( "#id_strike_test_api" ).click(function() {
                test_api_status(server);
            });
        }
    };
});