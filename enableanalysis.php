<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Script to enable sending a file to STRIKE if not yet sent (modify "doanalysis").
 *
 * File         enableanalysis.php
 * Encoding     UTF-8
 *
 * @package     plagiarism_strike
 *
 * @copyright   Sebsoft.nl / Strikeplagiarism.com
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__.'/../../config.php');
require_once($CFG->dirroot . '/plagiarism/strike/lib.php');

require_login();
$pageurl = new moodle_url($CFG->dirroot . '/plagiarism/strike/enableanalysis.php');

require_sesskey();
$fid = required_param('fid', PARAM_INT);
$doanalysis = required_param('doanalysis', PARAM_INT);
$redirect = optional_param('redirect', get_local_referer(false), PARAM_URL);

$sf = plagiarism_strike\strikefile::get_by_id($fid);
list($course, $cm) = get_course_and_cm_from_cmid($sf->cm);
$context = context_course::instance($course->id);
require_capability('plagiarism/strike:enable', $context, $USER->id, true, 'nopermissions');

switch ($doanalysis) {
    case 0:
        $sf->doanalysis = 0;
        $sf->statuscode = PLAGIARISM_STRIKE_STATUS_AWAITCHOICE;
        $sf->save();
        break;
    case 1:
        $sf->doanalysis = 1;
        $sf->statuscode = PLAGIARISM_STRIKE_STATUS_PENDING;
        $sf->save();
        break;
    default:
        break;
}

redirect($redirect);